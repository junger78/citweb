﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LayerTo1C._1C
{
    class LinqTo1C
    {
        private readonly string pathToWorkDir = LinqTo1C.CheckDirLayer1C();

        private static string CheckDirLayer1C()
        {
            if (!Directory.Exists("C:\\www\\layer1C\\"))
            {
                Directory.CreateDirectory("C:\\www\\layer1C\\");
            }

            return "C:\\www\\layer1C\\";
        }

        private static dynamic conn1C;

        public static dynamic Conn1C { get { return conn1C = _1CWorker.start1C(conn1C); } }


        dynamic Запрос;
        dynamic Выборка;

        public static Type v82ComConnector = Type.GetTypeFromProgID("V83.COMConnector");

        public void CreateQuery1C()
        {

            Object[] ar = { "Запрос" };
            Запрос = v82ComConnector.InvokeMember("NewObject", BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.Static, null, Conn1C, ar);

        }

        public void SetParam(string name, object value)
        {
            Запрос.Параметры.Вставить(name, value);
        }

        public dynamic GetResultValue(string queryText)
        {

            //СД.ЭтоГруппа И
            Запрос.Текст = queryText;
            Выборка = Запрос.Выполнить.Выбрать();
            return Выборка;

            //Object[] ar = { };
            //dynamic _Docs = v82ComConnector.InvokeMember("Документы", BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.Static, null, result, ar);
            //dynamic b = _Docs.Т_АктОказанияУслугПоОтправкеПриемуКонтейнеров.НайтиПоНомеру(akt, date);
            //b = b.ПолучитьОбъект();
            //b.СохранитьРасшифровку(DecodeFileName);
        }

        public dynamic GetFileByUIDDoc(string GUID)
        {
            return null;
        }
        public string Sendkod1CFromContractors(string KodeService)
        {
            string Code = "0";
            //string Query = @"ВЫБРАТЬ  
            //        ЦеныНоменклатурыСрезПоследних.Цена как ЦенаУслуги                 
            //        ИЗ    Справочник.Номенклатура КАК Номенклатура1  
            //        ЛЕВОЕ СОЕДИНЕНИЕ 
            //        РегистрСведений.ЦеныНоменклатуры.СрезПоследних(&Период) КАК ЦеныНоменклатурыСрезПоследних  ПО ЦеныНоменклатурыСрезПоследних.ТипЦен.Код = ""00000001 "" 
            //        И ЦеныНоменклатурыСрезПоследних.Номенклатура = Номенклатура1.Ссылка  
            //        ГДЕ  
            //        (Номенклатура1.Код = &КОД)";
            string Query = @"ВЫБРАТЬ
                               Контрагенты.Ссылка КАК Ссылка,
                               Контрагенты.Код КАК Код,
                               Контрагенты.Наименование КАК Наименование,
                               Контрагенты.ИНН КАК ИНН,
                               Контрагенты.КПП КАК КПП
                               ИЗ
                               Справочник.Контрагенты КАК Контрагенты
                              ";

            // Query = @"ВЫБРАТЬ СД.Ссылка КАК Ссылка, СД.Наименование КАК Наименование ИЗ Справочник.Д_СогласованиеДокументов КАК СД ГДЕ  СД.Родитель.Ссылка ЕСТЬ NULL ";
            this.CreateQuery1C();
            this.SetParam("КОД", KodeService);
            this.SetParam("Период", DateTime.Now.Date);

            var Выборка = this.GetResultValue(Query);

            while (Выборка.Следующий())
            {
                Code = Выборка.ЦенаУслуги;
            }
            return Code;
        }
        //public decimal GetPriceServiceFrom1C(string KodeService)
        public string GetIsThereKontragentFrom1C(string InnService, string KppService)
        {
            //decimal Price = 0;
            string IsThereOne = null;
            string IsThereOneINN = null;

;
            // Query = @"ВЫБРАТЬ СД.Ссылка КАК Ссылка, СД.Наименование КАК Наименование ИЗ Справочник.Д_СогласованиеДокументов КАК СД ГДЕ  СД.Родитель.Ссылка ЕСТЬ NULL ";
            this.CreateQuery1C();
            this.SetParam("ИНН", InnService);
            this.SetParam("КПП", KppService);
            //this.SetParam("Период", DateTime.Now.Date);
            string Query = @"ВЫБРАТЬ
                               Контрагенты.Ссылка КАК Ссылка,
                               Контрагенты.Код КАК Код,
                               Контрагенты.Наименование КАК Наименование,
                               Контрагенты.ИНН КАК ИНН,
                               Контрагенты.КПП КАК КПП
                               ИЗ
                               Справочник.Контрагенты КАК Контрагенты
                               ГДЕ
                               Контрагенты.ИНН = &ИНН
                               И Контрагенты.КПП = &КПП";
            var Выборка = this.GetResultValue(Query);

            while (Выборка.Следующий())
            {

                IsThereOne = Выборка.Наименование;
                //IsThereOneINN = Выборка.ИНН;
                //IsThereOne = Выборка.Код;
                // Price = Выборка.ЦенаУслуги;
            }
            return IsThereOne;
        }
        public static dynamic ValidateContractors(string inn, string kpp)
        {
            dynamic result = LinqTo1C.Conn1C;
            Object[] ar = { };

            dynamic _Docs = v82ComConnector.InvokeMember("оссСайт", BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.Static, null, result, ar);
            dynamic b = _Docs.ПроверкаКонтрагента(inn, kpp);
            return b;
        }




    }
}
