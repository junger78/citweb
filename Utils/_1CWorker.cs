﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LayerTo1C._1C
{
    public class _1CWorker
    {
        public static dynamic start1C(dynamic Conn1C)
        {
            dynamic result = Conn1C;

            if (result == null || !check_link(result))
            {
                var v82ComConnector = Type.GetTypeFromProgID("V83.COMConnector");
                dynamic V82 = Activator.CreateInstance(v82ComConnector);

                //берем строку подключения не из базы, т.к сервер sql утром перезагружается
                //string t = context.global_variables.First(x => x.name == "1cConnectionString").value;
               string t = "Srvr=\"server-1c83.oss.local\"; Ref=\"work2018_test\"; Usr=\"data_importer\"; Pwd=\"data_importer\";";
               //string t = "Srvr=\"ns1c83x64.oss.local\"; Ref=\"work2018_tenzor\"; Usr=\"data_importer\"; Pwd=\"data_importer\";";
                
                Object[] arguments = { t };
                result = v82ComConnector.InvokeMember("Connect", BindingFlags.Public | BindingFlags.InvokeMethod | BindingFlags.Static, null, V82, arguments);

                Conn1C = result;
            }
            return Conn1C;
        }
        protected static bool check_link(dynamic result)
        {

            try
            {
                Type v82ComConnector = Type.GetTypeFromProgID("V83.COMConnector");

                Object[] ar = { };
                dynamic Справочники = v82ComConnector.InvokeMember("Справочники", BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.Static, null, result, ar);

                string inn = "7710293280";

                //ar = new Object[] { "111111111" };
                //dynamic resXML = v82ComConnector.InvokeMember("XMLстрока", BindingFlags.Public | BindingFlags.InvokeMethod | BindingFlags.Static, null, result, ar);
                if (Справочники.Контрагенты.НайтиПоРеквизиту("ИНН", inn).ИНН == inn)
                {

                }
            }
            catch (Exception ex)
            {                
                return false;
            }
            return true;


        }

    }
}
