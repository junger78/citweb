﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Prilozhenie1
    {
        public int Prilozhenie1Id { get; set; }
        public string Prilozhenie1HeadId { get; set; }
        public string Prilozhenie1Text { get; set; }
        public string Prilozhenie1Head { get; set; }
    }
}
