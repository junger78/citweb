﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class DocParagraph
    {
        public int DocParagraphsId { get; set; }
        public string DocParagraphsHeadId { get; set; }
        public string DocParagraphsText { get; set; }
        public string DocParagraphsHeadText { get; set; }
    }
}
