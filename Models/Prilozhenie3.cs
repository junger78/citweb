﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Prilozhenie3
    {
        public int Prilozhenie3Id { get; set; }
        public string Prilozhenie3HeadId { get; set; }
        public string Prilozhenie3Text { get; set; }
        public string Prilozhenie3Head { get; set; }
    }
}
