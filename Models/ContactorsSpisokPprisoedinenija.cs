﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class ContactorsSpisokPprisoedinenija
    {
        public int Id { get; set; }
        public int? OrganizId { get; set; }
        public string Prilozhenie1 { get; set; }
        public string Prilozhenie2 { get; set; }
        public string Prilozhenie3 { get; set; }
        public string Prilozhenie4 { get; set; }
        public string Prilozhenie5 { get; set; }
        public string Prilozhenie6 { get; set; }
        public string Prilozhenie7 { get; set; }
        public string Prilozhenie8 { get; set; }
        public string Prilozhenie9 { get; set; }
    }
}
