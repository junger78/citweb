﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Prilozhenie2
    {
        public int Prilozhenie2Id { get; set; }
        public string Prilozhenie2HeadId { get; set; }
        public string Prilozhenie2Text { get; set; }
        public string Prilozhenie2Head { get; set; }
    }
}
