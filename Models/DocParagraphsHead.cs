﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class DocParagraphsHead
    {
        public int DocParagraphsHeadId { get; set; }
        public string DocParagraphsHeadText { get; set; }
    }
}
