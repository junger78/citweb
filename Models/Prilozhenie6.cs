﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Prilozhenie6
    {
        public int Prilozhenie6Id { get; set; }
        public string Prilozhenie6HeadId { get; set; }
        public string Prilozhenie6Text { get; set; }
        public string Prilozhenie6Head { get; set; }
    }
}
