﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Organization
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string ShortName { get; set; }
        public string Inn { get; set; }
        public string Okpo { get; set; }
        public int? MaxDebt { get; set; }
        public int? Write { get; set; }
        public int? PayFreightCharge { get; set; }
        public int? FreightCharge { get; set; }
    }
}
