﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Prilozhenie7
    {
        public int Prilozhenie7Id { get; set; }
        public string Prilozhenie7HeadId { get; set; }
        public string Prilozhenie7Text { get; set; }
        public string Prilozhenie7Head { get; set; }
    }
}
