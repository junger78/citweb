﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Price1
    {
        public int Id { get; set; }
        public int Prilozhenie10Punkt { get; set; }
        public string Prilozhenie10Head { get; set; }
        public string Prilozhenie10Text { get; set; }
        public string Prilozhenie10EdenicaIzmerenija { get; set; }
        public string Prilozhenie10Stoimost { get; set; }
        public string Prilozhenie10Foot { get; set; }
        public int? Prilozhenie10Prim { get; set; }
        public int Prilozhenie10HeadId { get; set; }
    }
}
