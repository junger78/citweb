﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class containers3Context : DbContext
    {
        public containers3Context()
        {
        }

        public containers3Context(DbContextOptions<containers3Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Contractor> Contractors { get; set; }
        public virtual DbSet<NewContractor> NewContractors { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=hv-sql; Database=containers3; user=sa; password=nthgtybtbnhel}; ");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Cyrillic_General_CI_AS");

            modelBuilder.Entity<Contractor>(entity =>
            {
                entity.ToTable("contractors");

                entity.HasIndex(e => e.AutoDelivery, "idx_auto_delivery");

                entity.HasIndex(e => e.Group, "idx_group");

                entity.HasIndex(e => e.Inn, "idx_inn");

                entity.HasIndex(e => e.Name, "idx_name");

                entity.HasIndex(e => e.Okpo, "idx_okpo");

                entity.HasIndex(e => e.OperationsDenial, "idx_operations_denial");

                entity.HasIndex(e => e.SbEmptyInbound, "idx_sb_empty_inbound");

                entity.HasIndex(e => e.SbEmptyOutput, "idx_sb_empty_output");

                entity.HasIndex(e => e.SbFullInbound, "idx_sb_full_inbound");

                entity.HasIndex(e => e.SbFullOutput, "idx_sb_full_output");

                entity.HasIndex(e => e.Write, "idx_write");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Abbreviated)
                    .HasMaxLength(10)
                    .HasColumnName("abbreviated");

                entity.Property(e => e.ActualContract).HasColumnName("actual_contract");

                entity.Property(e => e.Address)
                    .HasMaxLength(500)
                    .HasColumnName("address");

                entity.Property(e => e.AutoDelivery).HasColumnName("auto_delivery");

                entity.Property(e => e.BadPayer).HasColumnName("bad_payer");

                entity.Property(e => e.CarriageService1c)
                    .HasMaxLength(50)
                    .HasColumnName("carriage_service_1c");

                entity.Property(e => e.CheckTtnMail).HasColumnName("Check_TTN_mail");

                entity.Property(e => e.Color).HasColumnName("color");

                entity.Property(e => e.ContainerService1c)
                    .HasMaxLength(50)
                    .HasColumnName("container_service_1c");

                entity.Property(e => e.Country).HasColumnName("country");

                entity.Property(e => e.Debet)
                    .HasMaxLength(50)
                    .HasColumnName("debet");

                entity.Property(e => e.Diadoc).HasColumnName("diadoc");

                entity.Property(e => e.Emails)
                    .HasMaxLength(500)
                    .HasColumnName("emails");

                entity.Property(e => e.FnsParticipantId)
                    .HasMaxLength(500)
                    .HasColumnName("fnsParticipantId");

                entity.Property(e => e.ForAlterInter).HasColumnName("for_alter_inter");

                entity.Property(e => e.FreightCharge).HasColumnName("freight_charge");

                entity.Property(e => e.FreightChargeService)
                    .HasMaxLength(50)
                    .HasColumnName("freight_charge_service");

                entity.Property(e => e.FullName)
                    .HasMaxLength(500)
                    .HasColumnName("full_name");

                entity.Property(e => e.Group)
                    .HasMaxLength(50)
                    .HasColumnName("group");

                entity.Property(e => e.Inn)
                    .HasMaxLength(50)
                    .HasColumnName("inn");

                entity.Property(e => e.IsTrusted).HasColumnName("is_trusted");

                entity.Property(e => e.IsUnreliable).HasColumnName("is_unreliable");

                entity.Property(e => e.Kod1c).HasColumnName("kod1c");

                entity.Property(e => e.Kpp)
                    .HasMaxLength(50)
                    .HasColumnName("kpp");

                entity.Property(e => e.KtPriceVersion)
                    .HasMaxLength(100)
                    .HasColumnName("kt_price_version");

                entity.Property(e => e.LoadedGateInConstraint).HasColumnName("loaded_gate_in_constraint");

                entity.Property(e => e.MaxDebt).HasColumnName("max_debt");

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .HasColumnName("name");

                entity.Property(e => e.NightGateOut).HasColumnName("night_gate_out");

                entity.Property(e => e.NightGateOutReserve).HasColumnName("night_gate_out_reserve");

                entity.Property(e => e.NoNds).HasColumnName("no_nds");

                entity.Property(e => e.NotifyContainersEmails)
                    .HasMaxLength(500)
                    .HasColumnName("notify_containers_emails");

                entity.Property(e => e.NotifyEmails)
                    .HasMaxLength(500)
                    .HasColumnName("notify_emails");

                entity.Property(e => e.Okpo)
                    .HasMaxLength(50)
                    .HasColumnName("okpo");

                entity.Property(e => e.OperationsDenial).HasColumnName("operations_denial");

                entity.Property(e => e.PasteTogetherNumber).HasColumnName("paste_together_number");

                entity.Property(e => e.PayFreightCharge).HasColumnName("pay_freight_charge");

                entity.Property(e => e.Phones)
                    .HasMaxLength(500)
                    .HasColumnName("phones");

                entity.Property(e => e.PriceVersion)
                    .HasMaxLength(100)
                    .HasColumnName("price_version");

                entity.Property(e => e.RepairChargeType).HasColumnName("repair_charge_type");

                entity.Property(e => e.RepairContract)
                    .HasMaxLength(50)
                    .HasColumnName("repair_contract");

                entity.Property(e => e.SbEmptyInbound).HasColumnName("sb_empty_inbound");

                entity.Property(e => e.SbEmptyOutput).HasColumnName("sb_empty_output");

                entity.Property(e => e.SbFullInbound).HasColumnName("sb_full_inbound");

                entity.Property(e => e.SbFullOutput).HasColumnName("sb_full_output");

                entity.Property(e => e.SeparateAtBids).HasColumnName("separate_at_bids");

                entity.Property(e => e.SeparateAtBidsReceivers).HasColumnName("separate_at_bids_receivers");

                entity.Property(e => e.Transporter).HasColumnName("transporter");

                entity.Property(e => e.UseAddressFom1c).HasColumnName("use_address_fom_1c");

                entity.Property(e => e.Write).HasColumnName("write");
            });
            
            modelBuilder.Entity<NewContractor>(entity =>
            {
                entity.ToTable("NewContractors");

                entity.HasIndex(e => e.AutoDelivery, "idx_auto_delivery");

                entity.HasIndex(e => e.Group, "idx_group");

                entity.HasIndex(e => e.Inn, "idx_inn");

                entity.HasIndex(e => e.Name, "idx_name");

                entity.HasIndex(e => e.Okpo, "idx_okpo");

                entity.HasIndex(e => e.OperationsDenial, "idx_operations_denial");

                entity.HasIndex(e => e.SbEmptyInbound, "idx_sb_empty_inbound");

                entity.HasIndex(e => e.SbEmptyOutput, "idx_sb_empty_output");

                entity.HasIndex(e => e.SbFullInbound, "idx_sb_full_inbound");

                entity.HasIndex(e => e.SbFullOutput, "idx_sb_full_output");

                entity.HasIndex(e => e.Write, "idx_write");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Abbreviated)
                    .HasMaxLength(10)
                    .HasColumnName("abbreviated");

                entity.Property(e => e.ActualContract).HasColumnName("actual_contract");

                entity.Property(e => e.Address)
                    .HasMaxLength(500)
                    .HasColumnName("address");

                entity.Property(e => e.AutoDelivery).HasColumnName("auto_delivery");

                entity.Property(e => e.BadPayer).HasColumnName("bad_payer");

                entity.Property(e => e.CarriageService1c)
                    .HasMaxLength(50)
                    .HasColumnName("carriage_service_1c");

                entity.Property(e => e.CheckTtnMail).HasColumnName("Check_TTN_mail");

                entity.Property(e => e.Color).HasColumnName("color");

                entity.Property(e => e.ContainerService1c)
                    .HasMaxLength(50)
                    .HasColumnName("container_service_1c");

                entity.Property(e => e.Country).HasColumnName("country");

                entity.Property(e => e.Debet)
                    .HasMaxLength(50)
                    .HasColumnName("debet");

                entity.Property(e => e.Diadoc).HasColumnName("diadoc");

                entity.Property(e => e.Emails)
                    .HasMaxLength(500)
                    .HasColumnName("emails");

                entity.Property(e => e.FnsParticipantId)
                    .HasMaxLength(500)
                    .HasColumnName("fnsParticipantId");

                entity.Property(e => e.ForAlterInter).HasColumnName("for_alter_inter");

                entity.Property(e => e.FreightCharge).HasColumnName("freight_charge");

                entity.Property(e => e.FreightChargeService)
                    .HasMaxLength(50)
                    .HasColumnName("freight_charge_service");

                entity.Property(e => e.FullName)
                    .HasMaxLength(500)
                    .HasColumnName("full_name");

                entity.Property(e => e.Group)
                    .HasMaxLength(50)
                    .HasColumnName("group");

                entity.Property(e => e.Inn)
                    .HasMaxLength(50)
                    .HasColumnName("inn");

                entity.Property(e => e.IsTrusted).HasColumnName("is_trusted");

                entity.Property(e => e.IsUnreliable).HasColumnName("is_unreliable");

                entity.Property(e => e.Kod1c).HasColumnName("kod1c");

                entity.Property(e => e.Kpp)
                    .HasMaxLength(50)
                    .HasColumnName("kpp");

                entity.Property(e => e.KtPriceVersion)
                    .HasMaxLength(100)
                    .HasColumnName("kt_price_version");

                entity.Property(e => e.LoadedGateInConstraint).HasColumnName("loaded_gate_in_constraint");

                entity.Property(e => e.MaxDebt).HasColumnName("max_debt");

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .HasColumnName("name");

                entity.Property(e => e.NightGateOut).HasColumnName("night_gate_out");

                entity.Property(e => e.NightGateOutReserve).HasColumnName("night_gate_out_reserve");

                entity.Property(e => e.NoNds).HasColumnName("no_nds");

                entity.Property(e => e.NotifyContainersEmails)
                    .HasMaxLength(500)
                    .HasColumnName("notify_containers_emails");

                entity.Property(e => e.NotifyEmails)
                    .HasMaxLength(500)
                    .HasColumnName("notify_emails");

                entity.Property(e => e.Okpo)
                    .HasMaxLength(50)
                    .HasColumnName("okpo");

                entity.Property(e => e.OperationsDenial).HasColumnName("operations_denial");

                entity.Property(e => e.PasteTogetherNumber).HasColumnName("paste_together_number");

                entity.Property(e => e.PayFreightCharge).HasColumnName("pay_freight_charge");

                entity.Property(e => e.Phones)
                    .HasMaxLength(500)
                    .HasColumnName("phones");

                entity.Property(e => e.PriceVersion)
                    .HasMaxLength(100)
                    .HasColumnName("price_version");

                entity.Property(e => e.RepairChargeType).HasColumnName("repair_charge_type");

                entity.Property(e => e.RepairContract)
                    .HasMaxLength(50)
                    .HasColumnName("repair_contract");

                entity.Property(e => e.SbEmptyInbound).HasColumnName("sb_empty_inbound");

                entity.Property(e => e.SbEmptyOutput).HasColumnName("sb_empty_output");

                entity.Property(e => e.SbFullInbound).HasColumnName("sb_full_inbound");

                entity.Property(e => e.SbFullOutput).HasColumnName("sb_full_output");

                entity.Property(e => e.SeparateAtBids).HasColumnName("separate_at_bids");

                entity.Property(e => e.SeparateAtBidsReceivers).HasColumnName("separate_at_bids_receivers");

                entity.Property(e => e.Transporter).HasColumnName("transporter");

                entity.Property(e => e.UseAddressFom1c).HasColumnName("use_address_fom_1c");

                entity.Property(e => e.Write).HasColumnName("write");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
