﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Prilozhenie5
    {
        public int Prilozhenie5Id { get; set; }
        public string Prilozhenie5HeadId { get; set; }
        public string Prilozhenie5Text { get; set; }
        public string Prilozhenie5Head { get; set; }
    }
}
