﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Price10
    {
        public int Id { get; set; }
        public int Prilozhenie19Punkt { get; set; }
        public string Prilozhenie19Head { get; set; }
        public string Prilozhenie19Text { get; set; }
        public string Prilozhenie19EdenicaIzmerenija { get; set; }
        public string Prilozhenie19Stoimost { get; set; }
        public string Prilozhenie19Foot { get; set; }
        public int? Prilozhenie19Prim { get; set; }
        public int Prilozhenie19HeadId { get; set; }
    }
}
