﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Price7
    {
        public int Id { get; set; }
        public int Prilozhenie16Punkt { get; set; }
        public string Prilozhenie16Head { get; set; }
        public string Prilozhenie16Text { get; set; }
        public string Prilozhenie16EdenicaIzmerenija { get; set; }
        public string Prilozhenie16Stoimost { get; set; }
        public string Prilozhenie16Foot { get; set; }
        public int? Prilozhenie16Prim { get; set; }
        public int Prilozhenie16HeadId { get; set; }
    }
}
