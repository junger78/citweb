﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class DocUnit
    {
        public int DocUnitId { get; set; }
        public string DocUnitText { get; set; }
        public byte[] DocUnitFile { get; set; }
        public string DocUnitLink { get; set; }
        public string DocUnitHttpslink { get; set; }
    }
}
