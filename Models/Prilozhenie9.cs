﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Prilozhenie9
    {
        public int Prilozhenie9Id { get; set; }
        public string Prilozhenie9HeadId { get; set; }
        public string Prilozhenie9Text { get; set; }
        public string Prilozhenie9Head { get; set; }
    }
}
