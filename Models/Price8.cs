﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Price8
    {
        public int Id { get; set; }
        public int Prilozhenie17Punkt { get; set; }
        public string Prilozhenie17Head { get; set; }
        public string Prilozhenie17Text { get; set; }
        public string Prilozhenie17EdenicaIzmerenija { get; set; }
        public string Prilozhenie17Stoimost { get; set; }
        public string Prilozhenie17Foot { get; set; }
        public int? Prilozhenie17Prim { get; set; }
        public int Prilozhenie17HeadId { get; set; }
    }
}
