﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Companies
    {
        public int CompaniesId { get; set; }
        public string CompaniesShortName { get; set; }
        public string CompaniesTelephone { get; set; }
        public string CompaniesEmail { get; set; }
        public string CompaniesINN { get; set; }
        public string CompaniesKPP { get; set; }
        public string CompaniesAdress { get; set; }
    }
}

