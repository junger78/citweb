﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Contractor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Abbreviated { get; set; }
        public string Inn { get; set; }
        public string Okpo { get; set; }
        public int? MaxDebt { get; set; }
        public int? Write { get; set; }
        public string FullName { get; set; }
        public int? PayFreightCharge { get; set; }
        public int? FreightCharge { get; set; }
        public int? Color { get; set; }
        public string CarriageService1c { get; set; }
        public string ContainerService1c { get; set; }
        public string FreightChargeService { get; set; }
        public int? RepairChargeType { get; set; }
        public string RepairContract { get; set; }
        public int? PasteTogetherNumber { get; set; }
        public string Group { get; set; }
        public int? OperationsDenial { get; set; }
        public int? SbEmptyInbound { get; set; }
        public int? SbFullInbound { get; set; }
        public int? SbEmptyOutput { get; set; }
        public int? SbFullOutput { get; set; }
        public int? CheckTtnMail { get; set; }
        public int? Postpay { get; set; }
        public int? NoNds { get; set; }
        public int? AutoDelivery { get; set; }
        public int? OwnerDisposerPlatform { get; set; }
        public string Address { get; set; }
        public string Phones { get; set; }
        public string Emails { get; set; }
        public string NotifyEmails { get; set; }
        public string PriceVersion { get; set; }
        public int? SeparateAtBids { get; set; }
        public int? SeparateAtBidsReceivers { get; set; }
        public string NotifyContainersEmails { get; set; }
        public int? Transporter { get; set; }
        public string KtPriceVersion { get; set; }
        public int? Diadoc { get; set; }
        public int? BadPayer { get; set; }
        public string Kpp { get; set; }
        public string Kod1c { get; set; }
        public int? ForAlterInter { get; set; }
        public bool? IsTrusted { get; set; }
        public string Debet { get; set; }
        public int? NightGateOut { get; set; }
        public int? Country { get; set; }
        public bool? UseAddressFom1c { get; set; }
        public string FnsParticipantId { get; set; }
        public int? NightGateOutReserve { get; set; }
        public int? LoadedGateInConstraint { get; set; }
        public int? IsUnreliable { get; set; }
        public int? ActualContract { get; set; }
    }
}
