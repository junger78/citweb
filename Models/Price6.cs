﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Price6
    {
        public int Id { get; set; }
        public int Prilozhenie15Punkt { get; set; }
        public string Prilozhenie15Head { get; set; }
        public string Prilozhenie15Text { get; set; }
        public string Prilozhenie15EdenicaIzmerenija { get; set; }
        public string Prilozhenie15Stoimost { get; set; }
        public string Prilozhenie15Foot { get; set; }
        public int? Prilozhenie15Prim { get; set; }
        public int Prilozhenie15HeadId { get; set; }
    }
}
