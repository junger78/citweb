﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class logistContext : DbContext
    {
        public logistContext()
        {
        }

        public logistContext(DbContextOptions<logistContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ContactorsSpisokPprisoedinenija> ContactorsSpisokPprisoedinenijas { get; set; }
        public virtual DbSet<DocParagraph> DocParagraphs { get; set; }
        public virtual DbSet<DocParagraphsHead> DocParagraphsHeads { get; set; }
        public virtual DbSet<DocUnit> DocUnits { get; set; }
        public virtual DbSet<Membership> Memberships { get; set; }
        public virtual DbSet<Organization> Organizations { get; set; }
        public virtual DbSet<Price1> Price1s { get; set; }
        public virtual DbSet<Price10> Price10s { get; set; }
        public virtual DbSet<Price2> Price2s { get; set; }
        public virtual DbSet<Price3> Price3s { get; set; }
        public virtual DbSet<Price5> Price5s { get; set; }
        public virtual DbSet<Price6> Price6s { get; set; }
        public virtual DbSet<Price7> Price7s { get; set; }
        public virtual DbSet<Price8> Price8s { get; set; }
        public virtual DbSet<Prilozhenie1> Prilozhenie1s { get; set; }
        public virtual DbSet<Prilozhenie2> Prilozhenie2s { get; set; }
        public virtual DbSet<Prilozhenie3> Prilozhenie3s { get; set; }
        public virtual DbSet<Prilozhenie4> Prilozhenie4s { get; set; }
        public virtual DbSet<Prilozhenie5> Prilozhenie5s { get; set; }
        public virtual DbSet<Prilozhenie6> Prilozhenie6s { get; set; }
        public virtual DbSet<Prilozhenie7> Prilozhenie7s { get; set; }
        public virtual DbSet<Prilozhenie8> Prilozhenie8s { get; set; }
        public virtual DbSet<Prilozhenie9> Prilozhenie9s { get; set; }
        public virtual DbSet<Profile> Profiles { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=Web;Database=logist; user=LLIYKA; password=09051990; ");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Cyrillic_General_CI_AS");

            modelBuilder.Entity<ContactorsSpisokPprisoedinenija>(entity =>
            {
                entity.ToTable("ContactorsSpisokPprisoedinenija");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.OrganizId).HasColumnName("Organiz_id");

                entity.Property(e => e.Prilozhenie1)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie_1");

                entity.Property(e => e.Prilozhenie2)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie_2");

                entity.Property(e => e.Prilozhenie3)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie_3");

                entity.Property(e => e.Prilozhenie4)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie_4");

                entity.Property(e => e.Prilozhenie5)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie_5");

                entity.Property(e => e.Prilozhenie6)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie_6");

                entity.Property(e => e.Prilozhenie7)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie_7");

                entity.Property(e => e.Prilozhenie8)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie_8");

                entity.Property(e => e.Prilozhenie9)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie_9");
            });

            modelBuilder.Entity<DocParagraph>(entity =>
            {
                entity.HasKey(e => e.DocParagraphsId)
                    .HasName("PK_doc_paragraphs");

                entity.ToTable("docParagraphs");

                entity.Property(e => e.DocParagraphsId).HasColumnName("Doc_paragraphs_id");

                entity.Property(e => e.DocParagraphsHeadId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Doc_paragraphs_head_id");

                entity.Property(e => e.DocParagraphsHeadText)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Doc_Paragraphs_head_text");

                entity.Property(e => e.DocParagraphsText)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Doc_paragraphs_text");
            });

            modelBuilder.Entity<DocParagraphsHead>(entity =>
            {
                entity.ToTable("docParagraphsHead");

                entity.Property(e => e.DocParagraphsHeadId).HasColumnName("Doc_paragraphs_head_id");

                entity.Property(e => e.DocParagraphsHeadText)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Doc_paragraphs_head_text");
            });

            modelBuilder.Entity<DocUnit>(entity =>
            {
                entity.ToTable("docUnit");

                entity.Property(e => e.DocUnitId)
                    .ValueGeneratedNever()
                    .HasColumnName("doc_unit_id");

                entity.Property(e => e.DocUnitFile)
                    .IsRequired()
                    .HasColumnName("doc_unit_file");

                entity.Property(e => e.DocUnitHttpslink)
                    .IsRequired()
                    .HasColumnName("doc_unit_httpslink");

                entity.Property(e => e.DocUnitLink)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("doc_unit_link");

                entity.Property(e => e.DocUnitText)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("doc_unit_text");
            });

            modelBuilder.Entity<Membership>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Comment).HasMaxLength(256);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.FailedPasswordAnswerAttemptWindowsStart).HasColumnType("datetime");

                entity.Property(e => e.FailedPasswordAttemptWindowStart).HasColumnType("datetime");

                entity.Property(e => e.LastLockoutDate).HasColumnType("datetime");

                entity.Property(e => e.LastLoginDate).HasColumnType("datetime");

                entity.Property(e => e.LastPasswordChangedDate).HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.PasswordAnswer).HasMaxLength(128);

                entity.Property(e => e.PasswordQuestion).HasMaxLength(256);

                entity.Property(e => e.PasswordSalt)
                    .IsRequired()
                    .HasMaxLength(128);
            });

            modelBuilder.Entity<Organization>(entity =>
            {
                entity.ToTable("Organization");

                entity.Property(e => e.FreightCharge).HasColumnName("freight_charge");

                entity.Property(e => e.FullName)
                    .HasColumnType("ntext")
                    .HasColumnName("Full_name");

                entity.Property(e => e.Inn)
                    .HasMaxLength(50)
                    .HasColumnName("inn");

                entity.Property(e => e.MaxDebt).HasColumnName("max_debt");

                entity.Property(e => e.Okpo)
                    .HasMaxLength(50)
                    .HasColumnName("okpo");

                entity.Property(e => e.PayFreightCharge).HasColumnName("pay_freight_charge");

                entity.Property(e => e.ShortName)
                    .HasMaxLength(50)
                    .HasColumnName("Short_name");

                entity.Property(e => e.Write).HasColumnName("write");
            });

            modelBuilder.Entity<Price1>(entity =>
            {
                entity.ToTable("Price-1");

                entity.Property(e => e.Prilozhenie10EdenicaIzmerenija)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie10EdenicaIzmerenija");

                entity.Property(e => e.Prilozhenie10Foot)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie10Foot");

                entity.Property(e => e.Prilozhenie10Head)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie10Head");

                entity.Property(e => e.Prilozhenie10HeadId).HasColumnName("prilozhenie10HeadId");

                entity.Property(e => e.Prilozhenie10Prim).HasColumnName("prilozhenie10Prim");

                entity.Property(e => e.Prilozhenie10Punkt).HasColumnName("prilozhenie10Punkt");

                entity.Property(e => e.Prilozhenie10Stoimost)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie10Stoimost");

                entity.Property(e => e.Prilozhenie10Text)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie10Text");
            });

            modelBuilder.Entity<Price10>(entity =>
            {
                entity.ToTable("Price-10");

                entity.Property(e => e.Prilozhenie19EdenicaIzmerenija)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie19EdenicaIzmerenija");

                entity.Property(e => e.Prilozhenie19Foot)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie19Foot");

                entity.Property(e => e.Prilozhenie19Head)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie19Head");

                entity.Property(e => e.Prilozhenie19HeadId).HasColumnName("prilozhenie19HeadId");

                entity.Property(e => e.Prilozhenie19Prim).HasColumnName("prilozhenie19Prim");

                entity.Property(e => e.Prilozhenie19Punkt).HasColumnName("prilozhenie19Punkt");

                entity.Property(e => e.Prilozhenie19Stoimost)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie19Stoimost");

                entity.Property(e => e.Prilozhenie19Text)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie19Text");
            });

            modelBuilder.Entity<Price2>(entity =>
            {
                entity.ToTable("Price-2");

                entity.Property(e => e.Prilozhenie11EdenicaIzmerenija)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie11EdenicaIzmerenija");

                entity.Property(e => e.Prilozhenie11Foot)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie11Foot");

                entity.Property(e => e.Prilozhenie11Head)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie11Head");

                entity.Property(e => e.Prilozhenie11HeadId).HasColumnName("prilozhenie11HeadId");

                entity.Property(e => e.Prilozhenie11Prim).HasColumnName("prilozhenie11Prim");

                entity.Property(e => e.Prilozhenie11Punkt).HasColumnName("prilozhenie11Punkt");

                entity.Property(e => e.Prilozhenie11Stoimost)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie11Stoimost");

                entity.Property(e => e.Prilozhenie11Text)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie11Text");
            });

            modelBuilder.Entity<Price3>(entity =>
            {
                entity.ToTable("Price-3");

                entity.Property(e => e.Prilozhenie12EdenicaIzmerenija)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie12EdenicaIzmerenija");

                entity.Property(e => e.Prilozhenie12Foot)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie12Foot");

                entity.Property(e => e.Prilozhenie12Head)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie12Head");

                entity.Property(e => e.Prilozhenie12HeadId).HasColumnName("prilozhenie12HeadId");

                entity.Property(e => e.Prilozhenie12Prim).HasColumnName("prilozhenie12Prim");

                entity.Property(e => e.Prilozhenie12Punkt).HasColumnName("prilozhenie12Punkt");

                entity.Property(e => e.Prilozhenie12Stoimost)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie12Stoimost");

                entity.Property(e => e.Prilozhenie12Text)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie12Text");
            });

            modelBuilder.Entity<Price5>(entity =>
            {
                entity.ToTable("Price-5");

                entity.Property(e => e.Prilozhenie14EdenicaIzmerenija)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie14EdenicaIzmerenija");

                entity.Property(e => e.Prilozhenie14Foot)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie14Foot");

                entity.Property(e => e.Prilozhenie14Head)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie14Head");

                entity.Property(e => e.Prilozhenie14HeadId).HasColumnName("prilozhenie14HeadId");

                entity.Property(e => e.Prilozhenie14Prim).HasColumnName("prilozhenie14Prim");

                entity.Property(e => e.Prilozhenie14Punkt).HasColumnName("prilozhenie14Punkt");

                entity.Property(e => e.Prilozhenie14Stoimost)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie14Stoimost");

                entity.Property(e => e.Prilozhenie14Text)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie14Text");
            });

            modelBuilder.Entity<Price6>(entity =>
            {
                entity.ToTable("Price-6");

                entity.Property(e => e.Prilozhenie15EdenicaIzmerenija)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie15EdenicaIzmerenija");

                entity.Property(e => e.Prilozhenie15Foot)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie15Foot");

                entity.Property(e => e.Prilozhenie15Head)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie15Head");

                entity.Property(e => e.Prilozhenie15HeadId).HasColumnName("prilozhenie15HeadId");

                entity.Property(e => e.Prilozhenie15Prim).HasColumnName("prilozhenie15Prim");

                entity.Property(e => e.Prilozhenie15Punkt).HasColumnName("prilozhenie15Punkt");

                entity.Property(e => e.Prilozhenie15Stoimost)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie15Stoimost");

                entity.Property(e => e.Prilozhenie15Text)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie15Text");
            });

            modelBuilder.Entity<Price7>(entity =>
            {
                entity.ToTable("Price-7");

                entity.Property(e => e.Prilozhenie16EdenicaIzmerenija)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie16EdenicaIzmerenija");

                entity.Property(e => e.Prilozhenie16Foot)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie16Foot");

                entity.Property(e => e.Prilozhenie16Head)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie16Head");

                entity.Property(e => e.Prilozhenie16HeadId).HasColumnName("prilozhenie16HeadId");

                entity.Property(e => e.Prilozhenie16Prim).HasColumnName("prilozhenie16Prim");

                entity.Property(e => e.Prilozhenie16Punkt).HasColumnName("prilozhenie16Punkt");

                entity.Property(e => e.Prilozhenie16Stoimost)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie16Stoimost");

                entity.Property(e => e.Prilozhenie16Text)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie16Text");
            });

            modelBuilder.Entity<Price8>(entity =>
            {
                entity.ToTable("Price-8");

                entity.Property(e => e.Prilozhenie17EdenicaIzmerenija)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie17EdenicaIzmerenija");

                entity.Property(e => e.Prilozhenie17Foot)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie17Foot");

                entity.Property(e => e.Prilozhenie17Head)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie17Head");

                entity.Property(e => e.Prilozhenie17HeadId).HasColumnName("prilozhenie17HeadId");

                entity.Property(e => e.Prilozhenie17Prim).HasColumnName("prilozhenie17Prim");

                entity.Property(e => e.Prilozhenie17Punkt).HasColumnName("prilozhenie17Punkt");

                entity.Property(e => e.Prilozhenie17Stoimost)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie17Stoimost");

                entity.Property(e => e.Prilozhenie17Text)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("prilozhenie17Text");
            });

            modelBuilder.Entity<Prilozhenie1>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Prilozhenie-1");

                entity.Property(e => e.Prilozhenie1Head)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-1_head");

                entity.Property(e => e.Prilozhenie1HeadId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-1_head_id");

                entity.Property(e => e.Prilozhenie1Id).HasColumnName("Prilozhenie-1_id");

                entity.Property(e => e.Prilozhenie1Text)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-1_text");
            });

            modelBuilder.Entity<Prilozhenie2>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Prilozhenie-2");

                entity.Property(e => e.Prilozhenie2Head)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-2_head");

                entity.Property(e => e.Prilozhenie2HeadId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-2_head_id");

                entity.Property(e => e.Prilozhenie2Id).HasColumnName("Prilozhenie-2_id");

                entity.Property(e => e.Prilozhenie2Text)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-2_text");
            });

            modelBuilder.Entity<Prilozhenie3>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Prilozhenie-3");

                entity.Property(e => e.Prilozhenie3Head)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-3_head");

                entity.Property(e => e.Prilozhenie3HeadId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("Prilozhenie-3_head_id");

                entity.Property(e => e.Prilozhenie3Id).HasColumnName("Prilozhenie-3_id");

                entity.Property(e => e.Prilozhenie3Text)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-3_text");
            });

            modelBuilder.Entity<Prilozhenie4>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Prilozhenie-4");

                entity.Property(e => e.Prilozhenie4Head)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-4_head");

                entity.Property(e => e.Prilozhenie4HeadId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-4_head_id");

                entity.Property(e => e.Prilozhenie4Id).HasColumnName("Prilozhenie-4_id");

                entity.Property(e => e.Prilozhenie4Text)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-4_text");
            });

            modelBuilder.Entity<Prilozhenie5>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Prilozhenie-5");

                entity.Property(e => e.Prilozhenie5Head)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-5_head");

                entity.Property(e => e.Prilozhenie5HeadId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-5_head_id");

                entity.Property(e => e.Prilozhenie5Id).HasColumnName("Prilozhenie-5_id");

                entity.Property(e => e.Prilozhenie5Text)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-5_text");
            });

            modelBuilder.Entity<Prilozhenie6>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Prilozhenie-6");

                entity.Property(e => e.Prilozhenie6Head)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-6_head");

                entity.Property(e => e.Prilozhenie6HeadId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-6_head_id");

                entity.Property(e => e.Prilozhenie6Id).HasColumnName("Prilozhenie-6_id");

                entity.Property(e => e.Prilozhenie6Text)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-6_text");
            });

            modelBuilder.Entity<Prilozhenie7>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Prilozhenie-7");

                entity.Property(e => e.Prilozhenie7Head)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-7_head");

                entity.Property(e => e.Prilozhenie7HeadId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-7_head_id");

                entity.Property(e => e.Prilozhenie7Id).HasColumnName("Prilozhenie-7_id");

                entity.Property(e => e.Prilozhenie7Text)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-7_text");
            });

            modelBuilder.Entity<Prilozhenie8>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Prilozhenie-8");

                entity.Property(e => e.Prilozhenie8Head)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-8_head");

                entity.Property(e => e.Prilozhenie8HeadId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-8_head_id");

                entity.Property(e => e.Prilozhenie8Id).HasColumnName("Prilozhenie-8_id");

                entity.Property(e => e.Prilozhenie8Text)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-8_text");
            });

            modelBuilder.Entity<Prilozhenie9>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Prilozhenie-9");

                entity.Property(e => e.Prilozhenie9Head)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-9_head");

                entity.Property(e => e.Prilozhenie9HeadId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-9_head_id");

                entity.Property(e => e.Prilozhenie9Id).HasColumnName("Prilozhenie-9_id");

                entity.Property(e => e.Prilozhenie9Text)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Prilozhenie-9_text");
            });

            modelBuilder.Entity<Profile>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.LastUpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.PropertyNames)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.PropertyValueBinary)
                    .IsRequired()
                    .HasColumnType("image");

                entity.Property(e => e.PropertyValueStrings)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.UserName);

                entity.HasIndex(e => e.UserName, "IDX_UserName");

                entity.Property(e => e.UserName).HasMaxLength(50);

                entity.Property(e => e.LastActivityDate).HasColumnType("datetime");

                entity.Property(e => e.Locked).HasColumnName("locked");

                entity.Property(e => e.OrganizId).HasColumnName("Organiz_id");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
