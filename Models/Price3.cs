﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Price3
    {
        public int Id { get; set; }
        public int Prilozhenie12Punkt { get; set; }
        public string Prilozhenie12Head { get; set; }
        public string Prilozhenie12Text { get; set; }
        public string Prilozhenie12EdenicaIzmerenija { get; set; }
        public string Prilozhenie12Stoimost { get; set; }
        public string Prilozhenie12Foot { get; set; }
        public int? Prilozhenie12Prim { get; set; }
        public int Prilozhenie12HeadId { get; set; }
    }
}
