﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Staff
    {
        public int CompaniesId { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeTelephone { get; set; }
        public string EmployeeEmail { get; set; }
    }
}

