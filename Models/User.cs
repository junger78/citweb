﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class User
    {
        public Guid ApplicationId { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public bool IsAnonymous { get; set; }
        public DateTime LastActivityDate { get; set; }
        public int? OrganizId { get; set; }
        public bool Locked { get; set; }
    }
}
