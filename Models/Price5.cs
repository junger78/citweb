﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Price5
    {
        public int Id { get; set; }
        public int Prilozhenie14Punkt { get; set; }
        public string Prilozhenie14Head { get; set; }
        public string Prilozhenie14Text { get; set; }
        public string Prilozhenie14EdenicaIzmerenija { get; set; }
        public string Prilozhenie14Stoimost { get; set; }
        public string Prilozhenie14Foot { get; set; }
        public int? Prilozhenie14Prim { get; set; }
        public int Prilozhenie14HeadId { get; set; }
    }
}
