﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Prilozhenie8
    {
        public int Prilozhenie8Id { get; set; }
        public string Prilozhenie8HeadId { get; set; }
        public string Prilozhenie8Text { get; set; }
        public string Prilozhenie8Head { get; set; }
    }
}
