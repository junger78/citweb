﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Price2
    {
        public int Id { get; set; }
        public int Prilozhenie11Punkt { get; set; }
        public string Prilozhenie11Head { get; set; }
        public string Prilozhenie11Text { get; set; }
        public string Prilozhenie11EdenicaIzmerenija { get; set; }
        public string Prilozhenie11Stoimost { get; set; }
        public string Prilozhenie11Foot { get; set; }
        public int? Prilozhenie11Prim { get; set; }
        public int Prilozhenie11HeadId { get; set; }
    }
}
