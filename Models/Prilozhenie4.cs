﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AspNetCoreReact.Models
{
    public partial class Prilozhenie4
    {
        public int Prilozhenie4Id { get; set; }
        public string Prilozhenie4HeadId { get; set; }
        public string Prilozhenie4Text { get; set; }
        public string Prilozhenie4Head { get; set; }
    }
}
