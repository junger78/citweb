
import React, { Component } from 'react';
import {
  Route,
  Switch,
  Redirect,
  withRouter
} from "react-router-dom"
import './App.css';
import './registracion1.css';
import './registracion2.css';

import Home from './Pages/Home';
import AccountForUnregistered from './Pages/AccountForUnregistered';
import AccountForRegistered from './Pages/AccountForRegistered';
import Order from './Pages/Order';
import AppReg from './Pages/Registration';
import AuthContainer from './Pages/AuthContainer';
import Contract from './Pages/Contract';
import About from './Pages/About';
import SmiOnas from './Pages/SmiOnas';
import InfoOkompanii from './Pages/InfoOkompanii';
import CompanyHistory from './Pages/CompanyHistory';
import RukovodstvoKompanii from './Pages/RukovodstvoKompanii';
//import Protocol from './Containers/ContentContract';


class App extends Component {
  render() {
    const { history } = this.props

    return (
      <div className="App">
        <Switch>
          <Route history={history} path='/home' component={Home} />
          <Route history={history} path='/account-unreg' component={AccountForUnregistered} />
          <Route history={history} path='/account' component={AccountForRegistered} />
          <Route history={history} path='/order' component={Order} />
          <Route history={history} path='/personalnye_menedzhery' component={Home} />
          <Route history={history} path='/registration' component={AppReg} />
          <Route history={history} path='/authorization' component={AuthContainer} />
          <Route history={history} path='/on-line_zakljuchenie_dogovora' component={Contract} />
          <Route history={history} path='/history' component={Home} />
          <Route history={history} path='/photoalbum' component={Home} />
          <Route history={history} path='/presentation' component={Home} />
          <Route history={history} path='/write' component={Home} />
          <Route history={history} path='/call' component={Home} />
          <Route history={history} path='/application' component={Home} />
          <Route history={history} path='/uslugi_kontejnernogo_terminala' component={Home} />
          <Route history={history} path='/organizacija_kontejnernyh_perevozok' component={Home} />
          <Route history={history} path='/uslugi_SVH_Tamozhennogo_sklada' component={Home} />
          <Route history={history} path='/Iskljuchitelnaja_komptetencija' component={Home} />
          <Route history={history} path='/fitosanitarnyj_kontrol' component={Home} />
          <Route history={history} path='/deklaracija_sootvetstvija_TZ_TS_021_022_skoroport' component={Home} />
          <Route history={history} path='/contacts' component={Home} />
          <Route history={history} path='/location_map' component={Home} />
          <Route history={history} path='/about_company' component={About} />
          <Route history={history} path='/smi-o-nas/' component={SmiOnas} />
          <Route history={history} path='/informaciya-o-kompanii/' component={InfoOkompanii} />
          <Route history={history} path='/istoriya-kompanii/' component={CompanyHistory} />
          <Route history={history} path='/rukovoditeli-kompanii/' component={RukovodstvoKompanii} />
         
                
                
          <Redirect from='/' to='/home'/>
        </Switch>
      </div>
    );
  }
}

export default withRouter(App)
