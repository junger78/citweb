﻿import React, { Component } from "react";
import LeftPanelAccount from '../Containers/LeftPanelAccountReg';
import MainSmiOanas from '../Containers/MainSmiOanas';
class SmiOnas extends Component {
    render() {
        return (
            <div className="width100">

                <div className="wrapper">
                    <LeftPanelAccount />
                    <MainSmiOanas />
                </div>
            </div>
        );
    }
}
export default SmiOnas;