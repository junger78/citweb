import React, {Component} from "react";
import LeftPanelAccount  from '../Containers/LeftPanelAccountReg';
import MainOrder  from '../Containers/MainOrder';
class Order extends Component {
 render() {
    return (
        <div className="width100">
            
            <div className="wrapper">     
                <LeftPanelAccount/>
                <MainOrder/>               
            </div>        
        </div>
        );
    }
}
export default Order;