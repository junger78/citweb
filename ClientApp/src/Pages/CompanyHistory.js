﻿import React, { Component } from "react";
import LeftPanelAccount from '../Containers/LeftPanelAccountReg';
import MainHistoryCompany from '../Containers/MainHistoryCompany';
class CompanyHistory extends Component {
    render() {
        return (
            <div className="width100">

                <div className="wrapper">
                    <LeftPanelAccount />
                    <MainHistoryCompany />
                </div>
            </div>
        );
    }
}
export default CompanyHistory;