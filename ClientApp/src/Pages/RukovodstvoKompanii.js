﻿import React, { Component } from "react";
import LeftPanelAccount from '../Containers/LeftPanelAccountReg';
import MainRukovodstvoKompanii from '../Containers/MainRukovodstvoKompanii';
class RukovodstvoKompanii extends Component {
    render() {
        return (
            <div className="width100">

                <div className="wrapper">
                    <LeftPanelAccount />
                    <MainRukovodstvoKompanii />
                </div>
            </div>
        );
    }
}
export default RukovodstvoKompanii;