﻿import React, { Component } from "react";
import Header from '../Containers/Header';
import LeftPanelHome from '../Containers/LeftPanelHome';
import MainAbout from '../Containers/MainAbout';
class About extends Component {
    render() {
        return (
            <div className="width100">
                <Header />
                <div className="wrapper">
                    <LeftPanelHome />
                    <MainAbout />
                </div>
            </div>
        );
    }
}
export default About;