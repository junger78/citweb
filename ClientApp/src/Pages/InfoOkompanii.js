﻿import React, { Component } from "react";
import LeftPanelAccount from '../Containers/LeftPanelAccountReg';
import MainInfoOkompanii from '../Containers/MainInfoOkompanii';
class InfoOkompanii extends Component {
    render() {
        return (
            <div className="width100">

                <div className="wrapper">
                    <LeftPanelAccount />
                    <MainInfoOkompanii />
                </div>
            </div>
        );
    }
}
export default InfoOkompanii;