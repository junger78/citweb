﻿import React, { Component } from "react";
import Header from '../Containers/Header';
import LeftPanelAccountReg from '../Containers/LeftPanelAccountReg';
import MainAccountReg from '../Containers/MainAccountReg';
class AccountForRegistered extends Component {
    constructor(props) {
        super(props);
        this.state = {
            noauth: true,
        };
    }



    render() {
        return (
            <div className="width100">
                <Header />
                <div className="reg_wrapper">
                    <LeftPanelAccountReg noauth={this.state.noauth} apiUrl="/api/Profile" />
                    <MainAccountReg />
                </div>
            </div>
        );
    }
}
export default AccountForRegistered;