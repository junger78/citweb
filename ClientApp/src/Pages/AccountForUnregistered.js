import React, {Component} from "react";

import LeftPanelAccountUnreg from '../Containers/LeftPanelAccountUnreg';
import MainAccountUnreg from '../Containers/MainAccountUnreg';
import Header from '../Containers/Header';
class AccountForUnregistered extends Component {
 render() {
    return (
        <div className="width100">
            <Header />
            <div className="unreg_wrapper">     
                <LeftPanelAccountUnreg />
                <MainAccountUnreg />
            </div>        
        </div>
        );
    }
}
export default AccountForUnregistered;