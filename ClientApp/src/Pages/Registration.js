﻿//import React, { useRef } from "react";
//import React from "react";
import React, { Component } from "react";
import { Link } from "react-router-dom";
//import "./App.scss";
import "../App.css";


class AppReg extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogginActive: 0,
            rows: [['', '', '']],
            warning: '',
            email: '',
            isvalidNoCompite: false,
        };
    }

    changeState(e) {

        e.preventDefault();
        const email = this.state.email;
        const localRows = this.state.rows;
        let isLogginActive = this.state.isLogginActive;
        let formValid = false;

        if (isLogginActive === 1) {
            formValid = true;

            for (let i = 0; i < localRows.length; i++) {
                if (localRows[i][0] === '' || localRows[i][1] === '' || localRows[i][2] === '' || email === '') {
                    formValid = false;
                }
            }
        }
        else
            formValid = true

        if (formValid)
            this.setState(x => ({ isLogginActive: x.isLogginActive + 1 }));
        else {
            this.setState({ isvalidNoCompite: true });
            this.setState(({ warning: 'Заполните все данные' }))
        }
    }
    changeStateDecr(e) {
        this.setState(x => ({ isLogginActive: x.isLogginActive - 1 }));
    }
    changeStateRedact(e) {
        this.setState(x => ({ isLogginActive: x.isLogginActive - 2 }));
    }
    changeStateTest(e) {
        e.preventDefault();
        const localRows = this.state.rows;
        localRows.push(['', '', '', ''])
        this.setState({ rows: localRows });
    }
    changePravForm(event) {

        let index = event.target.getAttribute("indexArr")
        const localRows = this.state.rows;
        localRows[index][0] = event.target.value
        this.setState({ rows: localRows });
        this.setState({ isvalidNoCompite: false });
    }
    changePravForm2(event) {
        let index = event.target.getAttribute("indexArr")
        const localRows = this.state.rows;
        localRows[index][1] = event.target.value
        this.setState({ rows: localRows });
        this.setState({ isvalidNoCompite: false });
    }
    changePravForm3(event) {
        let index = event.target.getAttribute("indexArr")
        const localRows = this.state.rows;
        localRows[index][2] = event.target.value
        this.setState({ rows: localRows });
        this.setState({ isvalidNoCompite: false });
    }
    changePravForm4(event) {
        this.setState({ email: event.target.value });
        this.setState({ isvalidNoCompite: false });
    }


    render() {
        const { isLogginActive, rows, warning, email, isvalidNoCompite } = this.state;
        let clasForTable = (isvalidNoCompite ? " WarningRed " : " ") + " oleg";

        return (
            <div>
                <div>
                    <div ref={ref => (this.container = ref)}>
                        {isLogginActive === 0 && (
                            <div>
                                <form className="information">
                                    <Link to="/account">
                                        <div onClick className="exit"> Выйти </div>
                                    </Link>
                                    <h2 className='Zag'> Общая информация</h2>
                                    <ol>
                                        <li>Для доступа к сервису "Личного кабинета" необходимо пройти регистрацию на сайте Исполнителя.</li>
                                        <li>Зарегестрированный пользователь получает доступ ко всем сервисам "Личного кабинета"
                                        с момента утверждения условий Договора,определяющего условия взаимодейсвия сторон.
                          С этого момента заказчик получает статус зарегестрированного пользователя с полным доступом ко всем электронным сервисам Исполнителя. </li>
                                        <li>На выбор Заказчика, утверждения условий Договора производится двумя способами:

                          <ol>
                                                <li>Дистанционное утверждение через сайт
                                                Исполнителя,путем подписания условий Договора с использованием простой
                                электронной подписи или усиленной квалифицированной электронной подписи</li>
                                                <li>Очное утверждение, рутем обмена документами на бумажном носителе, с применением процедуры прибытия в место заключения Договора Заказчика или Исполнителя.  </li>


                                            </ol>
                                        </li>

                                        <li>В обоих случаях утверждение Договора озможно только при прохождении процедуры регистрации Заказчика на сайте Исполнителя</li>
                                    </ol>

                                    <button className='start' onClick={this.changeState.bind(this)}> ПРИСТУПИТЬ К РЕГИСТРАЦИИ </button>

                                </form>
                            </div>
                        )}

                        {isLogginActive === 1 && (
                            <div>
                                <form className="information" >
                                    <button className="exit" onClick={this.changeStateDecr.bind(this)}> Назад </button>
                                    <h2 className='Zag'> Регистрация на сайте 2</h2>

                                    <table className='table'>
                                        <caption>Укажите данные идентификации юридического лица*,от имени которого будет заключаться Договор.</caption>
                                        <tr>

                                            <th align="center" >Правовая форма</th>
                                            <th align="center">Наименование организации</th>
                                            <th align="center">ИНН</th>
                                        </tr>
                                        {rows.map((r, i) => (
                                            <tr class='tdOleg'>
                                                <td> <input indexArr={i} key={i} className={clasForTable} onChange={this.changePravForm.bind(this)} value={r[0]}></input></td>
                                                <td> <input indexArr={i} key={i} className={clasForTable} onChange={this.changePravForm2.bind(this)} value={r[1]}></input></td>
                                                <td> <input indexArr={i} key={i} className={clasForTable} onChange={this.changePravForm3.bind(this)} value={r[2]}  ></input></td>
                                            </tr>
                                        ))}

                                    </table>
                                    <button className='DS' onClick={this.changeStateTest.bind(this)} > + Добавить строку </button>
                                    <br />
                                    <small>
                                        <i >*-в случае, если  работа через сайт будет производится с одного рабочего места от имени
                        нескольких юридических лиц группы компаний,воспользуйтесь сервисом "ДОБАВИТЬ СТРОКУ" и продолжайте оформление.</i>
                                    </small>

                                    <h4>Укажите адрес электронной почты, на который вы готовы принять уведосления для прохождения регистрации.</h4>

                                    <table class='table2'>

                                        <tr class='Oleg2'>

                                            <td align="center" > электронная почта </td>

                                        </tr>

                                        <tr >
                                            <td> <input className={clasForTable} onChange={this.changePravForm4.bind(this)} value={email}  ></input></td>
                                        </tr>
                                    </table>

                                    <h4>Воспользуйтесь функцией "отправить",для получения разового пароля</h4>


                                    {isvalidNoCompite && (<div className='ALARM' >{warning}</div>)}
                                    <div className="otpr">
                                        <button className='start2' onClick={this.changeState.bind(this)}> Отправить </button>
                                    </div>
                                </form>
                            </div>
                        )}


                        {isLogginActive === 2 && (
                            <div>

                                <form className="information">

                                    <button className="exit" onClick={this.changeStateDecr.bind(this)}> Назад </button>
                                    <h2 className='Zag'> Регистрация на сайте 3</h2>


                                    <table className='table'>
                                        <caption>Укажите данные идентификации юридического лица*,от имени которого будет заключаться Договор.</caption>
                                        <tr>
                                            <th align="center" >Правовая форма</th>
                                            <th align="center">Наименование организации</th>
                                            <th align="center">ИНН</th>
                                        </tr>
                                        {rows.map((r) => (
                                            <tr class='tdOleg'>
                                                <td> <input className="oleg" value={r[0]}></input></td>
                                                <td> <input className="oleg" value={r[1]}></input></td>
                                                <td> <input className="oleg" value={r[2]} ></input></td>
                                            </tr>
                                        ))}
                                    </table>

                                    <button className='DS' onClick={this.changeStateTest.bind(this)} > + Добавить строку </button>
                                    <br />
                                    <small><i >*-в случае, если  работа через сайт будет производится с одного рабочего места от имени
   нескольких юридических лиц группы компаний,воспользуйтесь сервисом "ДОБАВИТЬ СТРОКУ" и продолжайте оформление.</i></small>





                                    <table class='table2'>

                                        <tr class='Oleg2'>

                                            <td
                                                align="center" > электронная почта </td>

                                        </tr>
                                        <tr >
                                            <td> <input className="oleg" value={email}></input></td>

                                        </tr>
                                    </table>
                                    <h4>Внесите полученый по вышеуказанному адресу разовый пароль</h4>

                                    <table class='table2'>

                                        <tr class='Oleg2'>

                                            <td
                                                align="center" > разовый пароль </td>

                                        </tr>
                                        <tr >
                                            <td> <input className="oleg"></input></td>

                                        </tr>
                                    </table>

                                    <h4>Воспользуйтесь функцией "отправить",для подтвеждения информации</h4>
                                    <div className="otpr">
                                        <button className='start2' onClick={this.changeState.bind(this)}> Отправить </button>
                                    </div>



                                </form>

                            </div>
                        )}


                        {isLogginActive === 3 && (
                            <div>


                                <form className="information">

                                    <button className="exit" onClick={this.changeStateDecr.bind(this)}> Назад </button>
                                    <h2 className='Zag'> Регистрация на сайте 4</h2>


                                    <table className='table'>
                                        <caption>Укажите данные идентификации юридического лица*,от имени которого будет заключаться Договор.</caption>
                                        <tr>

                                            <th align="center" >Правовая форма</th>
                                            <th align="center">Наименование организации</th>
                                            <th align="center">ИНН</th>
                                        </tr>
                                        {rows.map((r) => (
                                            <tr class='tdOleg'>
                                                <td> <input className="oleg" value={r[0]}></input></td>
                                                <td> <input className="oleg" value={r[1]}></input></td>
                                                <td> <input className="oleg" value={r[2]} ></input></td>
                                            </tr>
                                        ))}


                                    </table>
                                    <button className='DS' onClick={this.changeStateTest.bind(this)} > + Добавить строку </button>
                                    <br />
                                    <small><i >*-в случае, если  работа через сайт будет производится с одного рабочего места от имени
                        нескольких юридических лиц группы компаний,воспользуйтесь сервисом "ДОБАВИТЬ СТРОКУ" и продолжайте оформление.</i></small>

                                    <h4>Для защиты персональных данных создайте свои уникальные данные(имя пользователя и пароль),
                                    необходимые для проверки права допуска к информации про входе в "Личный кабинет"
                        </h4>

                                    <div className="inform">
                                        <div className="text-i">
                                            <small><i>Перед созданием пароля настоятельно рекомендуем ознакомиться с сервисом "ХРАНИЛИЩЕ ПАРОЛЯ"
                                            и сформировать учетные записи на условиях данных рекомендаций.Это значительно успростит вход в "Личный кабинет",
                          У вас не будет обязанности вносить свои уникальные данные при каждом входе в "Личный кабинет"</i></small>
                                        </div>

                                        <div className="butt-par">
                                            <a href="https://cabinet.cit-ekb.ru/img/%D0%A5%D1%80%D0%B0%D0%BD%D0%B8%D0%BB%D0%B8%D1%89%D0%B5%20%D0%BF%D0%B0%D1%80%D0%BE%D0%BB%D0%B5%D0%B9%20(%D1%80%D0%B5%D0%B3%D0%B8%D1%81%D1%82%D1%80%D0%B0%D1%86%D0%B8%D1%8F).pdf" target="_blank" rel="noopener noreferrer" role="button" className="hranpar">ХРАНИЛИЩЕ ПАРОЛЯ</a>
                                        </div>
                                    </div>

                                    <div className="infform4">
                                        <p>При создании учетных данных на условиях рекомендаций раздела"ХРАНИЛИЩЕ ПАРОЛЯ"
                                        внисите эти данные в соответствующий раздел таблицы ввода уникальных данных.
                                        При игнорировании наших рекомендаций, создайте свои учетные записи путем заполнения таблицы, без ограничения символов ввода.
                        </p>
                                    </div>

                                    <table className='table3'>
                                        <tr align="center"><th colSpan="2">уникальные данные</th></tr>
                                        <tr>
                                            <th align="center" >Имя пользователя</th>
                                            <th align="center">Пароль</th>

                                        </tr>

                                        <tr >
                                            <td> <input className="oleg"></input></td>
                                            <td> <input id="password" type="password" className="oleg"></input></td>

                                        </tr>


                                    </table>

                                    <h4 >Воспользуйтесь функцией "отправить",для завершения процедуры регистрации на сайте.</h4>
                                    <div className="otpr">
                                        <button className='start2' onClick={this.changeState.bind(this)}> Отправить </button>
                                    </div>



                                </form>

                            </div>

                        )}

                        {isLogginActive === 4 && (
                            <div>


                                <form className="information">

                                    <button className="exit" onClick={this.changeStateDecr.bind(this)}> Назад</button>
                                    <button className="changeREG" onClick={this.changeState.bind(this)}> Изменить условия регистрации </button>
                                    <h2 className='Zag'> Регистрация на сайте 5</h2>


                                    <table className='table'>
                                        <caption>Юридическое лицо, от имени которого проводится регистрация на сайте</caption>
                                        <tr>

                                            <th align="center" >Правовая форма</th>
                                            <th align="center">Наименование организации</th>
                                            <th align="center">ИНН</th>
                                        </tr>

                                        {rows.map((r) => (
                                            <tr class='tdOleg'>
                                                <td> <input className="oleg" value={r[0]}></input></td>
                                                <td> <input className="oleg" value={r[1]}></input></td>
                                                <td> <input className="oleg" value={r[2]} ></input></td>
                                            </tr>
                                        ))}
                                    </table>




                                    <div className="inform">
                                        <div className="text-i">
                                            <h4>Уникальные данные для входа в "Личный кабинет"</h4>
                                        </div>

                                        <div className="butt-par">
                                            <a href="https://cabinet.cit-ekb.ru/img/%D0%A5%D1%80%D0%B0%D0%BD%D0%B8%D0%BB%D0%B8%D1%89%D0%B5%20%D0%BF%D0%B0%D1%80%D0%BE%D0%BB%D0%B5%D0%B9%20(%D1%80%D0%B5%D0%B3%D0%B8%D1%81%D1%82%D1%80%D0%B0%D1%86%D0%B8%D1%8F).pdf" target="_blank" rel="noopener noreferrer" role="button" className="hranpar">ХРАНИЛИЩЕ ПАРОЛЯ</a>
                                        </div>
                                    </div>


                                    <table className='table3'>
                                        <tr align="center"><th colSpan="2">уникальные данные</th></tr>
                                        <tr>
                                            <th align="center" >Имя пользователя</th>
                                            <th align="center">Пароль</th>

                                        </tr>

                                        <tr >
                                            <td> <input className="oleg"></input></td>
                                            <td> <input plaseholder="Введите пароль" id="password" type="password" className="oleg"></input></td>

                                        </tr>


                                    </table>

                                    <h4>Проверьте все внесенные данные и завершите процедуру регистрации на сайте.</h4>
                                    <div className="otpr">
                                        <button className='start2' onClick={this.changeState.bind(this)}> Завершить регистрацию </button>
                                    </div>



                                </form>

                            </div>
                        )}


                        {isLogginActive === 5 && (
                            <div>


                                <form className="informChange">

                                    <button onClick={this.changeStateDecr.bind(this)} className="exit"> Назад </button>



                                    <table className='table'>
                                        <caption><b>Изменяемые параметры</b>.</caption>
                                        <tr>

                                            <th align="center" >Реквизиты юридического лица</th>
                                            <th align="center">Имя пользователя</th>
                                            <th align="center">Пароль</th>
                                        </tr>

                                        <tr class='tdOleg'>
                                            <td> <input type="checkbox" className="oleg"></input></td>
                                            <td> <input type="checkbox" className="oleg"></input></td>
                                            <td> <input type="checkbox" className="oleg" ></input></td>
                                        </tr>



                                    </table>

                                    <br />






                                    <div className="otpr">
                                        <button className='start2' onClick={this.changeState.bind(this)}> Изменить </button>
                                    </div>



                                </form>

                            </div>
                        )}
                        {isLogginActive === 6 && (
                            <div>


                                <form className="information">

                                    <button className="exit" onClick={this.changeStateDecr.bind(this)}> Назад </button>

                                    <h2 className='Zag'> Регистрация на сайте 6</h2>


                                    <table className='table'>
                                        <caption>Юридическое лицо, от имени которого проводится регистрация на сайте</caption>
                                        <tr>

                                            <th align="center" >Правовая форма</th>
                                            <th align="center">Наименование организации</th>
                                            <th align="center">ИНН</th>
                                        </tr>

                                        {rows.map((r, i) => (
                                            <tr class='tdOleg'>
                                                <td> <input indexArr={i} key={i} className={clasForTable} onChange={this.changePravForm.bind(this)} value={r[0]}></input></td>
                                                <td> <input indexArr={i} key={i} className={clasForTable} onChange={this.changePravForm2.bind(this)} value={r[1]}></input></td>
                                                <td> <input indexArr={i} key={i} className={clasForTable} onChange={this.changePravForm3.bind(this)} value={r[2]}  ></input></td>
                                            </tr>
                                        ))}
                                    </table>




                                    <div className="inform">
                                        <div className="text-i">
                                            <h4>Уникальные данные для входа в "Личный кабинет"</h4>
                                        </div>

                                        <div className="butt-par">
                                            <a href="https://cabinet.cit-ekb.ru/img/%D0%A5%D1%80%D0%B0%D0%BD%D0%B8%D0%BB%D0%B8%D1%89%D0%B5%20%D0%BF%D0%B0%D1%80%D0%BE%D0%BB%D0%B5%D0%B9%20(%D1%80%D0%B5%D0%B3%D0%B8%D1%81%D1%82%D1%80%D0%B0%D1%86%D0%B8%D1%8F).pdf" target="_blank" rel="noopener noreferrer" role="button" className="hranpar">ХРАНИЛИЩЕ ПАРОЛЯ</a>
                                        </div>
                                    </div>


                                    <table className='table3'>
                                        <tr align="center"><th colSpan="2">уникальные данные</th></tr>
                                        <tr>
                                            <th align="center" >Имя пользователя</th>
                                            <th align="center">Пароль</th>

                                        </tr>

                                        <tr >
                                            <td> <input className="oleg"></input></td>
                                            <td> <input plaseholder="Введите пароль" id="password" type="password" className="oleg"></input></td>

                                        </tr>


                                    </table>


                                    <div className="otpr">
                                        <button className='start2' onClick={this.changeStateRedact.bind(this)}> Подтвердить изменения </button>
                                    </div>



                                </form>

                            </div>
                        )}



                    </div>

                </div>

            </div>

        );
    }
}


export default AppReg;
