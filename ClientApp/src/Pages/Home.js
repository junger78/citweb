import React, {Component} from "react";
import Header  from '../Containers/Header'; 
import LeftPanelHome  from '../Containers/LeftPanelHome';
import MainHome  from '../Containers/MainHome';
class Home extends Component {
 render() {
    return (
        <div className="width100">
            <Header/>
            <div className="home_wrapper">     
                <LeftPanelHome/>
                <MainHome/>               
            </div>        
        </div>
        );
    }
}
export default Home;