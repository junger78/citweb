﻿import React, { Component } from "react";
import LeftPanelAccountReg from '../Containers/LeftPanelAccountReg';
import MainContract from '../Containers/MainContract';
class Contract extends Component {
    constructor(props) {
        super(props);
        this.state = {
            noauth: true,
        };
    }
    render() {
        return (
            <div className="width100">
                <div className="online_dogovor_wrapper">
                    <LeftPanelAccountReg noauth={this.state.noauth} apiUrl={"/api/Profile "} gtlgn="/api/values/getlogin"/>
                    <MainContract />
                </div>
            </div>
        );
    }
}
export default Contract;