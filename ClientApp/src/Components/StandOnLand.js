import React  from "react";
import fitosanitarnaja_laboratorija from './1.svg'
import oktjabrskij_tamozhennyj_post from './2.svg'
import sklad_vremennogo_hranenija from './3.svg'
import vyhod_na_puti_obshhego_polzovanija from './4.svg'
class Schedule extends React.Component{		
        constructor(props) {
            super(props);           
            this.state = {              
            };
           }
	
	render(){
      
      
     
		return (		
			<div >	
                <div className="left_panel_text_bottom_title_wrap">			
				    <p className="left_panel_text_bottom_title">На площадке теминала располагаются:</p><p></p> 
                </div>        
                <div className="bottom_container">                   
                    <div className="inner_container">                         
                        <div className="col">
                            <img className="left_panel_img" style={{ width: 50, height: 35 }} src={fitosanitarnaja_laboratorija} alt={""} />
                            <p className="left_panel_text_bottom"></p> 
                        </div>
                        <div className="col">
                            <img className="left_panel_img" style={{ width: 50, height: 35 }} src={oktjabrskij_tamozhennyj_post} alt={""}/>
                            <p className="left_panel_text_bottom"></p> 
                        </div>                                            
                    </div>
                    <div className="inner_container">                        
                        <div className="col">
                            <img className="left_panel_img" style={{ width: 50, height: 35 }} src={sklad_vremennogo_hranenija} alt={""}/>
                            <p className="left_panel_text_bottom"></p> 
                        </div>
                        <div className="col">
                            <img className="left_panel_img" style={{ width: 50, height: 35 }} src={vyhod_na_puti_obshhego_polzovanija} alt={""}/>
                            <p className="left_panel_text_bottom" ></p> 
                        </div>
                    </div>
                </div>
            </div>            
					
		);
	}
}
export default Schedule;