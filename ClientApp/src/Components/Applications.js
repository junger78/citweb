import React, {Component} from "react";
import { Link} from "react-router-dom";
class Applications extends Component{		
        constructor(props) {
            super(props);           
            this.state = {
                color_black: true                         
            };
           }
           changeColor(){
            this.setState({color_black: !this.state.color_black})
        }
	render(){
    let bgColor = this.state.color_black ? "#000000" : "#104392"
      var amargin = {
        marginTop: 29

      }
		return (		
			<div >				
					<p className="left_panel_title">Заявки</p>         
                    <div style={amargin} className="pcontainer"> 
                    <Link to="/order">                  
                        <div onClick={this.changeColor.bind(this)} style={{backgroundColor: bgColor}} className="big_button">Оформление заявки</div> 
                    </Link>                   
                </div>
                <div style={amargin} className="pcontainer">                   
                    <div  className="big_button">Заявки в исполнении</div>                    
                </div>
                <div style={amargin} className="pcontainer">                   
                    <div  className="middle_low_button">Архив заявок</div>
                    <div  className="middle_low_button">Архив уведомлений</div>
                </div>
			</div>		
		);
	}
}
export default Applications;