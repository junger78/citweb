﻿import React, { Component } from "react";
import TopMenu from './TopMenu';
import Footer from './Footer';
import ContentSmiOnas from './ContentSmiOnas';
class MainSmiOanas extends Component {
    render() {
        return (
            <div className="width73">
                <div className="content_wrapper">
                    <TopMenu />
                    <ContentSmiOnas />
                    <Footer />
                </div>
            </div>
        );
    }
}
export default MainSmiOanas;