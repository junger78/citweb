import React  from "react";
import { Link} from "react-router-dom";
import men from './men.svg';
import telephon from './telephon.svg';

class Footer extends React.Component{		
        constructor(props) {
            super(props);           
            this.state = {              
            };
            this.onmenClick = this.onmenClick.bind(this);
    }
    onmenClick() { this.setState(prevState => ({ isToggleOn: !prevState.isToggleOn })); }

	render(){      
		return (
            		
                <div >			
                    <div className="footer">
                        <div className="footer_column_button">
                        <Link to="/account-unreg">
                                    <div  id="person_account" className="bottom_menu_button_big">
                                <div onClick={this.onmenClick} className="men">
                                            <img src={men} alt={"Вход в личный кабинет"}/> 
                                            <p>Личный кабинет</p>
                                        </div>
                                    </div>
                                
                            </Link>
                            
                        </div>                   
                        <div className="footer_column_button">
                            <Link to="/application">                           
                                <div id="application" href="#" className="bottom_menu_button_big">
                                    <div className="telephon">
                                        <img src={telephon} alt={"Приложение для автоперевозчиков"}/> 
                                        <p>Заголовок</p>
                                    </div>
                                </div>                            
                            </Link>
                        </div>
                        <div className="footer_column_button">
                            <Link to="/uslugi_kontejnernogo_terminala">
                            <div className="bottom_menu_button_black">Заголовок</div>
                            </Link> 
                            <Link to="/organizacija_kontejnernyh_perevozok">
                            <div className="bottom_menu_button_black">Заголовок</div>
                            </Link>
                        </div>
                        <div className="footer_column_button">
                            <Link to="/uslugi_SVH_Tamozhennogo_sklada">
                            <div className="bottom_menu_button_black">Заголовок</div>
                            </Link>   
                            <Link to="/Iskljuchitelnaja_komptetencija">
                            <div className="bottom_menu_button_black">Заголовок</div>
                            </Link>
                        </div>
                        <div className="footer_column_button">
                            <Link to="/fitosanitarnyj_kontrol">
                            <div className="bottom_menu_button_black">Заголовок</div>
                            </Link>
                            <Link to="/deklaracija_sootvetstvija_TZ_TS_021_022_skoroport"> 
                            <div className="bottom_menu_button_black">Заголовок</div>
                            </Link>
                        </div>
                        <div className="footer_column_button">
                            <Link to="/contacts">
                            <div className="bottom_menu_button_red">Заголовок</div>
                            </Link>
                            <Link to="/location_map ">
                            <div className="bottom_menu_button_red">Заголовок</div>
                            </Link>
                       </div>
                    </div>
                </div>
           		
		);
	}
}
export default Footer;