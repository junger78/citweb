import React, {Component} from "react";
import TopMenuAccount  from './TopMenu';
import ContentOrder  from './ContentOrder';
class MainOrder extends Component {
 render() {
    return (
        <div className="width73">
            <div className="content_wrapper">                   
                <TopMenuAccount/>
                <ContentOrder/>                              
            </div>        
        </div>
        );
    }
}
export default MainOrder;