import logo from './logo.png'
import React, {Component} from "react";
import Timeslot  from '../Components/TimeSlot';
import OnlineTablo  from '../Components/OnlineTablo';
import Schedule  from '../Components/Schedule';
import Treking  from '../Components/Treking';
import Calculator  from '../Components/Calculator';
import Documents  from '../Components/Documents';
//import StandOnLand  from '../Components/StandOnLand';

class LeftPanelHome extends Component {
 render() {
    return (
        <div className="leftpanel">
            <div className="left_panel_the_first_part"> 
                <a id="to_home_page" href="/">
                    <img src={logo} alt={"logo"}/>
                </a>
            </div>           
            <div className="left_panel_the_second_part"> 
                <Timeslot/>
                <OnlineTablo/>
                <Treking/>
                <Schedule/>
                <Calculator/>
                <Documents/>
            </div>
            <div className="left_panel_the_third_part">
               
            </div>
        </div>
        );
    }
}
export default LeftPanelHome;