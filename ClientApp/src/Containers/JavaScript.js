﻿class Protocol extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showPopup: props.showProtocol
        }
    }
    handleClickClose = () => {
        this.setState({ showPopup: false });

    }
    render() {

        return (
            <div style={{ display: (this.props.showProtocol) ? 'block' : 'none' }}>
                <div style={{ "position": "relative" }} >
                    <p className="protokol_raznoglasij_k_dogovoru">
                        Сформировать протокол разногласий к Договору?
                    </p>
                    <p className="protokol_buttons_group">
                        <button style={{ "marginRight": "5px" }}>Да</button>
                        <button onClick={this.handleClickClose} style={{ "marginLeft": "5px" }}>Закрыть</button>
                    </p>
                </div>
            </div>
        )
    }
}