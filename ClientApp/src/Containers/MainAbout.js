﻿import React, { Component } from "react";
import TopMenu from './TopMenu';
import Footer from './Footer';
import ContentAbout from './ContentAbout';
class MainHome extends Component {
    render() {
        return (
            <div className="width73">
                <div className="content_wrapper">
                    <TopMenu />
                    <ContentAbout />
                    <Footer />
                </div>
            </div>
        );
    }
}
export default MainHome;