﻿import React, { Component } from "react";
import TopMenuForUnregistered from './TopMenuForUnregistered';
import ContentContract from './ContentContract';
class MainContract extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };
    }
    
    render() {
        return (
            <div className="width73">
                <div className="content_wrapper">
                    <TopMenuForUnregistered />
                  {/* <ContentContract apiUrl={["/api/Companies", "/api/DocParagraphsHeads", "/api/DocParagraphs", "/api/Joining", "/api/Prilozhenie1", "/api/Prilozhenie2", "/api/Prilozhenie3", "/api/Prilozhenie4", "/api/Prilozhenie5", "/api/Prilozhenie6", "/api/Prilozhenie7", "/api/Prilozhenie8", "/api/Prilozhenie9", "/api/ClientData", "/api/Profile", "/api/contactorsSpisok", "/api/Price1", "/api/Price2", "/api/Price3", "/api/Price5", "/api/Price6", "/api/Price7", "/api/Price8", "/api/Price10"]} />*/}
                    <ContentContract apiUrl={["/api/Companies", "/api/Staff"]} />
                </div>
            </div>
        );
    }
}
export default MainContract;