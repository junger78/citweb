﻿import logo from './logo.png'
import React, { Component } from "react";
import Fesco from '../Components/Fesco';
import Timeslot from '../Components/TimeSlot';
import OnlineTablo from '../Components/OnlineTablo';
import Applications from '../Components/Applications';
import Treking from '../Components/Treking';
import TermsOfTheContract from '../Components/TermsOfTheContract';
import AuthorizationConditions from '../Components/AuthorizationConditions';



class LeftPanelAccountUnreg extends Component {

    constructor(props) {
        super(props);
        this.state = {
           
        };

    }
    componentDidMount() {
      
    }
    

    render() {
        return (
            <div className="leftpanel">
                <div className="left_panel_the_first_part">
                    <a id="to_home_page" href="/">
                        <img src={logo} alt={"logo"} />
                    </a>
                </div>
                <div className="left_panel_the_second_part">
                    <Fesco />
                    <Timeslot />
                    <OnlineTablo />
                    <Applications />
                    <Treking />
                    <TermsOfTheContract />
                    <AuthorizationConditions />
                </div>

            </div>
        );
    }
}
export default LeftPanelAccountUnreg;