import React from "react";
import Background from './citekb.jpg';
class Content extends React.Component{		
        constructor(props) {
            super(props);           
            this.state = {              
            };
    }
    createWordWrap=()=> {
         return { __html: 'Новые технологии автоматизации бизнеса' };
     }
    render() {
   
		return (		
			<div >
                <div className="home_content" >
                    <p >Новые технологии автоматизации бизнеса</p>
                    <img className="terminal_img" src={Background} alt={"Новые технологии автоматизации бизнеса"} />
                    <div className="terminal_block">
                        <div className="terminal_block_text" dangerouslySetInnerHTML={this.createWordWrap()}></div>
                    </div>
                </div>
			</div>		
		);
	}
}
export default Content;