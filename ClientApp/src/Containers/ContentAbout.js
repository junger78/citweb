﻿import React from "react";
import { Link } from "react-router-dom";
class ContentAbout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        var lispan = {fontSize: 24}
        return (
            <div >
                <div className="content">
                    <div className="">
                        <div className="page_title">О компании</div>
                        <div className="page_content">
                            <ul>
                                <li>
                                    <span style={lispan}>
                                        <strong>
                                            <Link to="/informaciya-o-kompanii/">
                                                <div>Информация о компании</div>
                                            </Link>
                                        </strong>
                                    </span>
                                </li>
                                <li>
                                    <span style={lispan}>
                                        <strong>
                                            <Link to="/istoriya-kompanii/">
                                                <div>История компании</div>
                                            </Link>
                                        </strong>
                                    </span>
                                </li>
                                <li>
                                    <span style={lispan}>
                                        <strong>
                                            <Link to="/rukovoditeli-kompanii/">
                                                <div>Руководство компании</div>
                                            </Link>
                                        </strong>
                                    </span>
                                </li>
                                <li>
                                    <span style={lispan}>
                                        <strong>
                                            <Link to="/smi-o-nas/">
                                                <div>Сми о нас</div>
                                            </Link>
                                        </strong>
                                    </span>
                                </li>

                            </ul>






                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default ContentAbout;