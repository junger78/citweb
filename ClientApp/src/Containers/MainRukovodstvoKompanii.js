﻿import React, { Component } from "react";
import TopMenu from './TopMenu';
import Footer from './Footer';
import ContentRukovodstvoKompanii from './ContentRukovodstvoKompanii';
class MainRukovodstvoKompanii extends Component {
    render() {
        return (
            <div className="width73">
                <div className="content_wrapper">
                    <TopMenu />
                    <ContentRukovodstvoKompanii />
                    <Footer />
                </div>
            </div>
        );
    }
}
export default MainRukovodstvoKompanii;