﻿import React from "react";
import { CSVLink, CSVDownload } from "react-csv";
import {  isOKPO} from '@utkonos/entrepreneur'
import request from "superagent";
import preloader from './Rhombus.gif';
import $, { type } from 'jquery';

//import { render, Document, Text } from 'redocx';
//import fs from 'fs';
class Popup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {           
            hideFormJoin:true,
            showProtocol: true,
            openProtocolhideFormJoin: props.openProtocolhideFormJoin,
            closePopup: props.closePopup,
            
        }
    }
    
    render() {
        return (
            <div style={{ display: (this.props.showPopup) ? 'block' : 'none' }}>
                <div style={{ "position": "relative" }} >
                    <div className="sformirovat_protokol_raznoglasij_k_dogovoru">
                        Открыть страницу сотрудников компании?
                         <div className="sformirovat_protokol_buttons_group">
                            <button onClick={e => this.props.openProtocolhideFormJoin(e, this.state.showProtocol, this.state.hideFormJoin)} style={{ "marginRight": "5px" }}>Да</button>
                            <button onClick={e =>this.handleClickNo(e)} style={{ "marginLeft": "5px" }}>Нет</button>
                          </div>
                    </div>
                   
                </div>
            </div>
        )
    }
}
class PopupCompanyDetailed extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hideFormJoin: true,
            showProtocol: true,
            openProtocolhideFormJoin: props.openProtocolhideFormJoin,
            closePopup: props.closePopup,

        }
    }

    componentDidMount(){
        //paragraphsKey.pop();
    }
    render() {
        return (
            <div style={{ display: (this.props.showPopupCompanyDetailed) ? 'block' : 'none' }}>
                <div style={{ "position": "relative" }} >
                    <div className="sformirovat_protokol_raznoglasij_k_dogovoru_detailed">
                        Подробная страница компании:
                        <ol className="doc_paragraphs_name">
                            {
                                this.props.paragraphsKey.map((pkey) => {
                                    return (

                                        this.props.companies.filter(item => item.companiesId == parseInt(pkey)).map(filteredCompany => {

                                            return (
                                                <li key={this.props.paragraphsKey[0]}>
                                                    <DocParagraphsCompanydetailed filteredCompany={filteredCompany} />
                                                </li>
                                            )
                                        })
                                    )
                                })
                            }
                        </ol>
                        <div className="sformirovat_protokol_buttons_group">
                            <button onClick={e => this.props.openProtocolhideFormJoin(e, this.state.showProtocol, this.state.hideFormJoin)} style={{ "marginRight": "5px" }}>Посмотреть сотрудников</button>
                            <button onClick={e => this.handleClickNo(e)} style={{ "marginLeft": "5px" }}>Закрыть</button>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}
class AddPointfromReglament extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            
            showProtocol: true,
            openProtocolhideFormReglament: props.openProtocolhideFormReglament,
            closePopup: props.closePopup,
            showPrilozhenie2: false,
            showReglamentPopup: false,
            indexForProtokolReglament:props.indexForProtokolReglament

        }
    }
    render() {
        const { showProtocol, showPrilozhenie2, showReglamentPopup } = this.state;
        return (
            <div style={{ display: (this.props.showReglamentPopup) ? 'block' : 'none' }}>
                <div style={{ "position": "relative" }} >
                    <div className="sformirovat_protokol_raznoglasij_k_dogovoru">
                        Добавить параграф Регламента в протокол разногласий к Договору?
                         <div className="sformirovat_protokol_buttons_group">
                            <button onClick={e => this.props.openProtocolhideFormReglament(e, showProtocol,  this.props.closePopupAddPointfromReglament(e, showReglamentPopup))} style={{ "marginRight": "5px" }}>Да</button>
                            <button onClick={e => this.handleClickNo(e)} style={{ "marginLeft": "5px" }}>Нет</button>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}
class AddPointfromDogovor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showPrilozhenie2: false,
            showProtocol: true,
            openProtocolhideFormReglament: props.openProtocolhideFormReglament,
            closeAddPoint: props.closeAddPoint,
            showReglamentPopup:false
        }
    }
    render() {
        return (
            <div style={{ display: (this.props.showAddPoint) ? 'block' : 'none' }}>
                <div style={{ "position": "relative" }} >
                    <div className="sformirovat_protokol_raznoglasij_k_dogovoru">
                        Добавить пункт в протокол разногласий к Договору?
                         <div className="sformirovat_protokol_buttons_group">
                            <button onClick={e => this.props.openProtocolhideFormReglament(e, this.state.showProtocol)} style={{ "marginRight": "5px" }}>Да</button>
                            <button onClick={e => this.handleClickNo(e)} style={{ "marginLeft": "5px" }}>Нет</button>
                        </div>
                    </div>

                </div>
            </div>
        )
    }


}
class Protocol extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editing: false,
            showProtocol: false,
            hideFormJoin: false,
            showPrilozhenie2: false,
            closeProtocol: props.closeProtocol,
            paragraphsKey: props.paragraphsKey,
            staff: props.staff,
            protokolparagraphs: props.paragraphs,
            objectForExcel: props.objectForExcel
           //reglamentParagraphs: props.reglamentParagraphs
        }
    }
      
    componentDidUpdate(prevProps, prevState, prevContext) {
        /*$('[id^="ReglPrgrphs_1"]').filter((idx, element) => {
            //console.log(element)
            return element.id.match(/ReglPrgrphs_\d/) 
        }).not(':first').hide();*/
       
        let existingData = []
        $('.ReglPrgrphs').each(function () {
            let thisData = $(this).data('filter-id');
            if (!existingData.includes(thisData)) {
                existingData.push(thisData)
            } else {
                $(this).hide();
            }
        });
    }
    componentWillUpdate(nextProps, nextState) {
        if (this.props.reglament1Paragraphs.length > 1) {
            this.props.reglament1Paragraphs.shift()
        } else if (this.props.reglament2Paragraphs.length > 1) {
            this.props.reglament2Paragraphs.shift();
        } else if (this.props.reglament3Paragraphs.length > 1) {
            this.props.reglament3Paragraphs.shift();
        } else if (this.props.reglament4Paragraphs.length > 1) {
            this.props.reglament4Paragraphs.shift();
        } else if (this.props.reglament5Paragraphs.length > 1) {
            this.props.reglament5Paragraphs.shift();
        } else if (this.props.reglament6Paragraphs.length > 1) {
            this.props.reglament6Paragraphs.shift();
        } else if (this.props.reglament7Paragraphs.length > 1) {
            this.props.reglament7Paragraphs.shift();
        } else if (this.props.reglament8Paragraphs.length > 1) {
            this.props.reglament8Paragraphs.shift();
        } else if (this.props.reglament9Paragraphs.length > 1) {
            this.props.reglament9Paragraphs.shift();
        } else if (this.props.price1Paragraphs.length > 1) {
            this.props.price1Paragraphs.shift();
        } else if (this.props.price2Paragraphs.length > 1) {
            this.props.price2Paragraphs.shift();
        } else if (this.props.price3Paragraphs.length > 1) {
            this.props.price3Paragraphs.shift();
        } else if (this.props.price5Paragraphs.length > 1) {
            this.props.price5Paragraphs.shift();
        } else if (this.props.price6Paragraphs.length > 1) {
            this.props.price6Paragraphs.shift();
        } else if (this.props.price7Paragraphs.length > 1) {
            this.props.price7Paragraphs.shift();
        } else if (this.props.price8Paragraphs.length > 1) {
            this.props.price8Paragraphs.shift();
        } else if (this.props.price10Paragraphs.length > 1) {
            this.props.price10Paragraphs.shift();
        }
    }
    
    
    updateText = (e, i) => {
        const { reglamentparagraph } = this.state;
        var newParagraph = { reglamentparagraph };
        JSON.stringify(newParagraph.paragraph);
        newParagraph[i] = e.target.value;
        this.setState({ reglamentparagraph: newParagraph[i] });
    }
    onFocus(i) {
        this.setState({ editing: true }, () => {
            this.refs.input.focus();
        });
    }

    onBlur() {
        this.setState({ editing: false });
        
    }
    render() {        
        const { staff, paragraphsKey, objectForExcel} = this.state;
            
        return (
            <div style={{ display: (this.props.showProtocol) ? 'block' : 'none' }}>
                <div style={{ "position": "relative" }} >
                    <div className="protokol_raznoglasij_k_dogovoru">
                        <div className="protokol_units_name">
                            <h3>Протокол разногласий к Договору №</h3>
                            <div><input type="text" name="numberofcontract" /></div>
                            <div> от</div>
                            <div><input type="text" name="dateofconclusion" /></div>
                        </div>
                        <div className="protokol_head">
                            <div className="protokol_punkt_dokumenta">№ п/п</div>
                            <div className="protokol_Ishodnyj_tekst_dokumenta">ФИО</div>
                            <div className="protokol_redakcija_zakazchika">Телефон</div>
                            <div className="protokol_soglasovannaja_redakcija">Email</div>
                        </div>
                        <ol className="doc_paragraphs_name">                        
                            {
                                paragraphsKey.map((pkey) => {
                                    return (
                                        
                                        this.props.staff.filter(comp => comp.companiesId == parseInt(pkey)).map(filteredCompanyStaff => {

                                           return (
                                               <li key={filteredCompanyStaff.EmployeeId}>
                                                    <ProtokolParagraphs  filteredCompanyStaff={filteredCompanyStaff}  />
                                               </li>
                                            )
                                        })
                                    )
                                })
                            }
                        </ol>
                        <div className="protokol_buttons_group">
                            <button onClick={e => this.props.closeProtocol(e, this.state.showProtocol, this.state.hideFormJoin)} style={{ "marginLeft": "38px" }} className="vernutsja_k_tekstu_dogovora">Вернуться к списку компаний</button>
                            <button onClick={e => this.props.closeProtocol(e, this.state.showProtocol, this.state.hideFormJoin)} style={{ "marginLeft": "38px" }} className="perejti_k_oformleniju_Dopolnitelnogo_soglashenija" >Перейти к оформлению Дополнительного соглашения</button>
                        </div>
                        {/*<CSVDownload data={objectForExcel} target="_blank" />*/}
                        {/*<CSVLink className="toExcel" filename={"protokol.csv"} data={objectForExcel}>Скачать протокол присоединения в формате csv(файл Excel)</CSVLink>*/}
                    </div>

                </div>
            </div>
        )
    }
}
class ProtokolParagraphs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editing: false,
            ProtokolParagraph: props.filteredCompanyStaff,
            tel: props.filteredCompanyStaff.employeeTelephone,
            memberId: props.filteredCompanyStaff.employeeId,
            
            
        }
    }

    updateText = (e, i) => {
        const { tel } = this.state;
        var protocol = this.state.tel;
        var newParagraph = { tel };
        //alert(JSON.stringify(newParagraph.paragraph));
        newParagraph[i] = e.target.value;
        this.setState({ protolkolparagraph: newParagraph[i] },
            this.props.addEditedCellToObjectForexcel(i, protocol)
        );
        //console.log("id  отредактированного параграфа" + i)
        //this.state.objectForExcel.map((obj) => {
        //if (obj.docParagraphsId == i) {
        
        
        
        
    }
    onFocus(i) {
        this.setState({ editing: true }, () => {
            this.refs.input.focus();
        });
    }

    onBlur() {
        this.setState({ editing: false });

    }
    componentDidMount() {
       
    }
    render() {
        const { memberId, ProtokolParagraph, editing, tel } = this.state;
        return (
            <div  className="protokol_table">
                <div className="protokol_punkt_dokumenta_text"> {memberId}</div>
                <div className="protokol_Ishodnyj_tekst_dokumenta_text">{ProtokolParagraph.employeeName}</div>

                {/* <div className="protokol_redakcija_zakazchika">{filteredParagraphs.docParagraphsText}</div>*/}
                {

                    (editing) ?
                        <textarea key={memberId} ref='input' className="protokol_redakcija_zakazchika_text" value={tel} onChange={(e) => this.updateText(e, memberId)} onBlur={() => this.onBlur()} /> :
                        <div className="protokol_redakcija_zakazchika_text" key={memberId} onClick={() => this.onFocus(memberId)}>{tel}</div>
                }
                
                <div className="protokol_soglasovannaja_redakcija_text">{ProtokolParagraph.employeeEmail}</div>
            </div>
        )
    }
}
class ProtokolReglamentParagraphs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editing: false,
            ReglamentParagraph: props.paragraph,
            reglamentparagraph: props.paragraph,
            prkey: props.prkey,
            reglamentId: props.reglamentkey
        }
    }
    

    updateText = (e, i) => {
        const { reglamentparagraph } = this.state;
        var newParagraph = { reglamentparagraph };
        var reglament = this.state.reglamentparagraph;
        //alert(JSON.stringify(newParagraph.paragraph));
        newParagraph[i] = e.target.value;
        console.log(i);
        let closest = e.target.closest('.protokol_table');
        let node = $(closest).find(':first-child').find(':nth-child(2)').text();
        let punkt = node.slice(3, 6);
        console.log(punkt);
        this.setState({ reglamentparagraph: newParagraph[i] },
            this.props.addEditedCellToObjectForexcel(punkt, reglament)
        );
    }
    onFocus(i) {
        this.setState({ editing: true }, () => {
            this.refs.input.focus();
        });
    }

    onBlur() {
        this.setState({ editing: false });

    }
    render() {
        const { reglamentparagraph, reglamentId, ReglamentParagraph, prkey, editing } = this.state;
       
        
        return (
            <div className="protokol_table">
                <div className="protokol_punkt_dokumenta_text">
                    <div style={{ height: "30%" }}></div>
                    <div className="Protokol_Punkt_Reglamenta_And_Price" style={{ height: "30%" }}>п. {reglamentId + "." + prkey} Регламента/Прайса</div>
                    <div style={{ height: "30%" }}></div>
                </div>
                <div className="protokol_Ishodnyj_tekst_dokumenta_text">{ReglamentParagraph}</div>

                
                {

                    (editing) ?
                        <textarea key={prkey} ref='input' className="protokol_redakcija_zakazchika_text" value={reglamentparagraph} onChange={(e) => this.updateText(e, prkey)} onBlur={() => this.onBlur()} /> :
                        <div className="protokol_redakcija_zakazchika_text" key={prkey} onClick={() => this.onFocus(prkey)}>{reglamentparagraph}</div>
                }
                <div className="protokol_soglasovannaja_redakcija_text"></div>
            </div>
        )
    }
}


class JoiningReglament extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dataJoin: props.join,
            index: props.index,
            showButtonPrisoed: true,
            showButtonAbort: false,
            showPrilozhenie2: true,
            showPrilozhenie3:true,
            hideFormJoin: true,
            openPrilozhenie: props.openPrilozhenie,
            prilozhenie1: props.prilozhenie1,
            prilozhenie2: props.prilozhenie2,
            prilozhenie3: props.prilozhenie3,
            prilozhenie4: props.prilozhenie4,
            prilozhenie5: props.prilozhenie5,
            prilozhenie6: props.prilozhenie6,
            prilozhenie7: props.prilozhenie7,
            prilozhenie8: props.prilozhenie8,
            prilozhenie9: props.prilozhenie9,
            prilozhenie1Href: {},
            click: [],
            showButtonOpened: false,
            showButtonClosed:true,           
            message: "",
           // getContactorsSpisokPprisoedinenija: props.getContactorsSpisokPprisoedinenija,
            arrReglamentHref: props.arrReglamentHref,
            //getContactorsSpisokPprisoedinenija: [],
        };
        this.prisoedinenie = this.prisoedinenie.bind(this);
        this.prisoedinenieAbort = this.prisoedinenieAbort.bind(this);
        this.pushReglamentHref = this.pushReglamentHref.bind(this);
        this.deleteReglamentHref = this.deleteReglamentHref.bind(this);
        //this.contactorsSpisokPprisoedinenija = this.contactorsSpisokPprisoedinenija.bind(this);
       
    }
  
    prisoedinenie(e) {
        let target = e.target.id;
        e.preventDefault();
        this.setState({
            showButtonPrisoed: false
        },
            this.pushReglamentHref(target)
        );
        this.setState({
            showButtonAbort: true
        },
            this.getSpisokPprisoedinenija
        );
        
                
    }
    prisoedinenieAbort(e) {
        e.preventDefault();
        let target = e.target.id;       
        this.setState({
            showButtonPrisoed: true
        },
            this.deleteReglamentHref(target)
        );
        this.setState({
            showButtonAbort: false
        },
            this.getSpisokPprisoedinenija
        );       
                       
    }
    getSpisokPprisoedinenija = () => {
        //fetch(this.props.apiUrl)
        //    .then(response => response.json())
        //    .then(response => this.setState({ getContactorsSpisokPprisoedinenija: response }));

        //console.log("ок")
        Promise.all(this.props.apiUrl.map(a => {
            return new Promise((resolve, reject) => {
                //using superagent here (w/o its promise api), "import request as 'superagent'. You'd import this at the top of your file.
                request.get(a)
                    .end((error, response) => {
                        if (error) {
                            return reject()
                        } else {
                            resolve(response)
                        }
                    })
            })
        }))
            .then(v => {
                //console.log(v);
                this.setState({
                    getContactorsSpisokPprisoedinenija: v[0].body,

                })



            })
            .catch(() => {
                console.error("Error in data retrieval")
            })
    }

    getButtonPosition = () => {
        this.state.getContactorsSpisokPprisoedinenija.map((element) => {
            if (element.prilozhenie5 != null) {
                console.log("prilozhenie5")
                this.setState({ showButtonPrisoed: false })
                this.setState({ showButtonAbort: true })
            }
            if (element.prilozhenie6 != null) {
                console.log("prilozhenie6")
                this.setState({ showButtonPrisoed: false })
                this.setState({ showButtonAbort: true })
            }
            if (element.prilozhenie7 != null) {
                console.log("prilozhenie7")
                this.setState({ showButtonPrisoed: false })
                this.setState({ showButtonAbort: true })
            }
            if (element.prilozhenie8 != null) {
                console.log("prilozhenie8")
                this.setState({ showButtonPrisoed: false })
                this.setState({ showButtonAbort: true })
            }
            if (element.prilozhenie9 != null) {
                console.log("prilozhenie9")
                this.setState({ showButtonPrisoed: false })
                this.setState({ showButtonAbort: true })
            }
        })
    }
    showButttonOpen(e) {
        e.preventDefault();
        console.log(e.target.id); // button element
        this.setState({ showButtonOpened: false });
        this.setState({ showButtonClosed: true });
        this.props.naimenovanieDokumentaOpen(e)
        //console.log("стоп");
    }
    showButttonClose(e) {
        e.preventDefault();
        console.log(e.target.id); // button element
        this.setState({ showButtonOpened: true });
        this.setState({ showButtonClosed: false });
        this.props.naimenovanieDokumentaClose(e)
        //console.log("стоп");
    }
    deleteReglamentHref(target) {
        
        var idHref = target.replace(/[^\d.-]/g, '');
        //var reglamentHref = $('#' + target).closest('.naimenovanie_prilozhenija_k_dejstvujushhemu_dogovoru').find('div').find('div').find('a').attr('href');
        //console.log(reglamentHref);
        console.log(idHref);
        this.setState(prevState => ({
            arrReglamentHref: prevState.arrReglamentHref.map(
                obj => (
                    obj.id == idHref ? Object.assign(obj, { href: null }) : obj)
            )            
        }),
            this.contactorsSpisokPprisoedinenija
        )
    }
    pushReglamentHref(target) {        
        var idHref = target.replace(/[^\d.-]/g, '');
        var reglamentHref = $('#' + target).closest('.naimenovanie_prilozhenija_k_dejstvujushhemu_dogovoru').find('div').find('div').find('a').attr('href');        
        console.log(idHref);        
        this.setState(prevState => ({
            arrReglamentHref: prevState.arrReglamentHref.map(
                obj => (
                    obj.id == idHref ? Object.assign(obj, { href: reglamentHref }) : obj)
            )
        }),
            this.contactorsSpisokPprisoedinenija
        )
    }
    componentWillMount() {
        //this.getButtonPosition();
    }
    componentWillReceiveProps(nextProps) {
        
    }
    
    
    
    componentDidUpdate() {
        
        
    }
    componentDidMount() {
        //fetch(this.props.apiUrl)
        //    .then(response => response.json())
        //    .then(response => this.setState({ getContactorsSpisokPprisoedinenija: response }));
        //this.getSpisokPprisoedinenija();
        //$('.prisoedinenie_k_uslovijam_head').click(function () {
        //    var reglamentHref = $(this).closest('.naimenovanie_prilozhenija_k_dejstvujushhemu_dogovoru').find('div').find('div').find('a').attr('href');
        //});
        Promise.all(this.props.apiUrl.map(a => {
            return new Promise((resolve, reject) => {
                //using superagent here (w/o its promise api), "import request as 'superagent'. You'd import this at the top of your file.
                request.get(a)
                    .end((error, response) => {
                        if (error) {
                            return reject()
                        } else {
                            resolve(response)
                        }
                    })
            })
        }))
            .then(v => {
                //console.log(v);
                this.setState({
                    getContactorsSpisokPprisoedinenija: v[0].body,

                })



            })
            .catch(() => {
                console.error("Error in data retrieval")
            })
       
        var prisoedinenie = document.getElementsByClassName("prisoedinenie_k_uslovijam_head");
        for (let i = 0; i < prisoedinenie.length; i++) {
            $(prisoedinenie[1]).css("display", "none");
            $(prisoedinenie[2]).css("display", "none");
            $(prisoedinenie[3]).css("display", "none");
            $(prisoedinenie[4]).css("display", "none");           
        }
        var naimenovanie = document.getElementsByClassName("naimenovanie_dokumenta_head");
        for (let i = 0; i < naimenovanie.length; i++) {
            $(naimenovanie[1]).css("margin-left", 170);
            $(naimenovanie[2]).css("margin-left", 170);
            $(naimenovanie[3]).css("margin-left", 170);
            $(naimenovanie[4]).css("margin-left", 170);
        }
        var spann = document.getElementsByClassName('DocUnitTitle');
        var click = [];
        for (var i = 0; i < spann.length; i++) {
            click.push(spann[i]);
        }
        this.setState({click:click})
        
    }
    contactorsSpisokPprisoedinenija = () =>{
        var match,
        id = this.props.contractClient.id,
        pril5 = null,
        pril6 = null,
        pril7 = null,
        pril8 = null,
        pril9 = null;
        this.state.getContactorsSpisokPprisoedinenija.forEach((element) => {
            if (element.organizId == id) {
                match = true;
               alert(match);
            }
        })
        this.state.arrReglamentHref.forEach((item) => {
            //console.log(item.id)
            if (item.id == 5) {
                pril5 = item.href
                //console.log(item.id)
            } else if (item.id == 6) {
                pril6 = item.href
                //console.log(item.id)
            } else if (item.id == 7) {
                pril7 = item.href
                //console.log(item.id)
            } else if (item.id == 8) {
                pril8 = item.href
                //console.log(item.id)
            } else if (item.id == 9) {
                pril9 = item.href
                //console.log(item.id)
            }
        })
        if (match!=true) {
            fetch(this.props.apiUrl,
                {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: "POST",
                    body: JSON.stringify({
                        OrganizId: id,
                        Prilozhenie1: "/doc/Prilozhenie-1-reglament-podachi-zajavki.docx",
                        Prilozhenie2: "/doc/Prilozhenie-2-reglament-pribytija-avtotransporta-na-terminal.docx",
                        Prilozhenie3: "/doc/Prilozhenie-3-reglament-obrabotki-gruza-pod-tamozhennymi-procedurami.docx",
                        Prilozhenie4: "/doc/Prilozhenie-4-reglamenet-priema-otpravlenija-kontejnerov-kontrejlerov-vagonov.docx",
                        Prilozhenie5: pril5,
                        Prilozhenie6: pril6,
                        Prilozhenie7: pril7,
                        Prilozhenie8: pril8,
                        Prilozhenie9: pril9
                    })

                }).
                then(response => response.text())
                .then(body => {
                    this.setState({ message: 'New Employee is Created Successfully' });
                });
        } else {
            
            fetch(this.props.apiUrl,
                {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: "PUT",
                    body: JSON.stringify({
                        OrganizId: id,
                        Prilozhenie1: "/doc/Prilozhenie-1-reglament-podachi-zajavki.docx",
                        Prilozhenie2: "/doc/Prilozhenie-2-reglament-pribytija-avtotransporta-na-terminal.docx",
                        Prilozhenie3: "/doc/Prilozhenie-3-reglament-obrabotki-gruza-pod-tamozhennymi-procedurami.docx",
                        Prilozhenie4: "/doc/Prilozhenie-4-reglamenet-priema-otpravlenija-kontejnerov-kontrejlerov-vagonov.docx",
                        Prilozhenie5: pril5,
                        Prilozhenie6: pril6,
                        Prilozhenie7: pril7,
                        Prilozhenie8: pril8,
                        Prilozhenie9: pril9
                    })

                }).
                then(response => response.text())
                .then(body => {
                    this.setState({ message: 'New Employee is Created Successfully' });
                });
        }
        //this.getButtonPosition();
    }
    render() {
        const { dataJoin, showButtonPrisoed, showButtonAbort, prilozhenie1, prilozhenie2, prilozhenie3, prilozhenie4, prilozhenie5, prilozhenie6, prilozhenie7, prilozhenie8, prilozhenie9, showButtonOpened, showButtonClosed, prilozhenie1Href, getContactorsSpisokPprisoedinenija } = this.state;
       
        
        return (
           /* <Text>*/
            <div  className="doc_unit_head_marker">
                    <div className="naimenovanie_prilozhenija_k_dejstvujushhemu_dogovoru">
                        <div className="agreementappendixid"><b>{dataJoin.docUnitId}</b></div>
                    <button id={"btnPris" + dataJoin.docUnitId} className="prisoedinenie_k_uslovijam_head" onClick={e => this.prisoedinenie(e)} style={{ display: showButtonPrisoed ? 'block' : 'none' }} >присоединиться</button>

                    <div className="wrap_galochka">
                        <div className="box_galochka">
                            <div id="check-part-1" className="check-sign_galochka"></div>
                            <div id="check-part-2" className="check-sign_galochka"></div>
                        </div>
                        </div>
                    <div id={"dokumentaHead" + dataJoin.docUnitId} className="naimenovanie_dokumenta_head" style={{ marginLeft: showButtonPrisoed ? 0 : 170 }}>
                        <div className="naimenovanie_dokumenta_stroka" onClick={e => this.props.openPrilozhenie(e, this.state.showPrilozhenie2, this.state.hideFormJoin)} >
                            <a className="reglamentLink" href={dataJoin.docUnitHttpslink} target="_blank" rel="noopener noreferrer">
                                <span className="DocUnitTitle" id={dataJoin.docUnitId}><b>{dataJoin.docUnitText}</b></span>
                            </a>
                        </div>
                        <div className="naimenovanie_dokumenta_buttons">
                            <span id={"naimenovanie" + dataJoin.docUnitId} className="naimenovanie_dokumenta_open" onClick={e => this.showButttonOpen(e)} style={{ display: showButtonOpened ? 'inline' : 'none' }}>Закрыть</span>
                            <span id={"naimenovanie" + dataJoin.docUnitId} className="naimenovanie_dokumenta_close" onClick={e => this.showButttonClose(e)} style={{ display: showButtonClosed ? 'inline' :  'none'  }}>Открыть</span>
                         </div>
                    </div>                                  
                    <button className="cancel_join_head" id={"btnCancel" + dataJoin.docUnitId}
                            style={{ display: showButtonAbort ? 'block' : 'none' }} onClick={e => this.prisoedinenieAbort(e)}>отменить</button>
                        <div style={{ position: showButtonAbort ? 'absolute' : 'relative' }} className="background_cancel">
                        
                        </div>
                    </div>
                </div>
           /* </Text>*/
        )
    }
}
class JoiningPrice extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dataJoin: props.join,
            showButtonPrisoed: true,
            showButtonAbort: false,
            showPrilozhenie2: true,
            hideFormJoin: true,
            openPrilozhenie: props.openPrilozhenie,
            showButtonOpened: false,
            showButtonClosed: true,
        };
        this.prisoedinenie = this.prisoedinenie.bind(this);
        this.prisoedinenieAbort = this.prisoedinenieAbort.bind(this);
    }

    prisoedinenie(e) {
        e.preventDefault();       
        
        this.setState({ showButtonPrisoed: false });
        this.setState({ showButtonAbort: true });
        //this.contactorsSpisokPprisoedinenija();
        //console.log("стоп");
    }
    
    prisoedinenieAbort(e) {
        e.preventDefault();
        console.log(e.target.id); // button element
        this.setState({ showButtonPrisoed: true });
        this.setState({ showButtonAbort: false });
        //console.log("стоп");
    }



    componentDidMount() {
        var prisoedinenie = document.getElementsByClassName("prisoedinenie_k_uslovijam_head");
        for (let i = 0; i < prisoedinenie.length; i++) {
            $(prisoedinenie[1]).css("display", "none");
            $(prisoedinenie[2]).css("display", "none");
            $(prisoedinenie[3]).css("display", "none");
            $(prisoedinenie[4]).css("display", "none");
        }
        var naimenovanie = document.getElementsByClassName("naimenovanie_dokumenta_head");
        for (let i = 0; i < naimenovanie.length; i++) {
            $(naimenovanie[1]).css("margin-left", 170);
            $(naimenovanie[2]).css("margin-left", 170);
            $(naimenovanie[3]).css("margin-left", 170);
            $(naimenovanie[4]).css("margin-left", 170);
        }
    }

    showButttonOpen(e) {
        e.preventDefault();
        console.log(e.target.id); // button element
        this.setState({ showButtonOpened: false });
        this.setState({ showButtonClosed: true });
        this.props.naimenovanieDokumentaOpen(e)
        //console.log("стоп");
    }
    showButttonClose(e) {
        e.preventDefault();
        console.log(e.target.id); // button element
        this.setState({ showButtonOpened: true });
        this.setState({ showButtonClosed: false });
        this.props.naimenovanieDokumentaClose(e)
        //console.log("стоп");
    }
    render() {
        const { dataJoin, showButtonPrisoed, showButtonAbort, showButtonOpened, showButtonClosed} = this.state;
        return (
            /* <Text>*/
            <div className="doc_unit_head_marker">
                <div className="naimenovanie_prilozhenija_k_dejstvujushhemu_dogovoru">
                    <div className="agreementappendixid"><b>{dataJoin.docUnitId}</b></div>
                    <button id={"btnPris" + dataJoin.docUnitId } className="prisoedinenie_k_uslovijam_head" onClick={e => this.prisoedinenie(e)} style={{ display: showButtonPrisoed ? 'block' : 'none' }} >присоединиться</button>

                    <div className="wrap_galochka">
                        <div className="box_galochka">
                            <div id="check-part-1" className="check-sign_galochka"></div>
                            <div id="check-part-2" className="check-sign_galochka"></div>
                        </div>
                    </div>
                    <div className="naimenovanie_dokumenta_head " style={{ marginLeft: showButtonPrisoed ? 0 : 170 }}>
                        <div className="naimenovanie_dokumenta_stroka">
                            <a className="reglamentLink" href={dataJoin.docUnitHttpslink} target="_blank" rel="noopener noreferrer"><b>{dataJoin.docUnitText}</b></a>
                        </div>
                        <div className="naimenovanie_dokumenta_buttons">                           
                                <span id={"naimenovanie" + dataJoin.docUnitId} className="naimenovanie_dokumenta_open" onClick={e => this.showButttonOpen(e)} style={{ display: showButtonOpened ? 'inline' : 'none' }}>Закрыть</span>
                                <span id={"naimenovanie" + dataJoin.docUnitId} className="naimenovanie_dokumenta_close" onClick={e => this.showButttonClose(e)} style={{ display: showButtonClosed ? 'inline' : 'none' }}>Открыть</span>
                        </div>
                    </div>
                    <button className="cancel_join_head"
                        style={{ display: showButtonAbort ? 'block' : 'none' }} onClick={e => this.prisoedinenieAbort(e)}>отменить</button>
                    <div style={{ position: showButtonAbort ? 'absolute' : 'relative' }} className="background_cancel">

                    </div>
                </div>
            </div>
            /* </Text>*/
        )
    }
}
class DocParagraphs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            company: props.paragraph,

            DocParagraph: props.paragraph,
            //paragraph: props.paragraph.docParagraphsText,
            index: props.index,
            editing: false,
            showPopup: true,
            showPopupCompanyDetailed: true,
            showAddPoint: true,
            openPopup: props.openPopup,
            openAddPoint: props.openDialog,
            openDialog: props.openAddPoint,
            paragraphsKey: props.paragraphsKey,
        }
    }
    //shouldComponentUpdate() {
    //    if (this.state.updateparagraph !== this.state.dataDocParagraphs.docParagraphsHeadText) {
    //        this.setState({ matched: event.target.value });
    //        return true;
    //    }
    //    return false;
    //}
    //updateParagraph = (event) => {
    //    event.preventDefault();
    //    this.setState({ dataDocParagraphs: event.target.value });

    //}
    //updateParagraph(newText) {
      
    //    var newParagraph = paragraph;
    //    newParagraph[index] = newText;
    //    this.setState({ paragraph: newParagraph});
    //}

  
    updateText = (e, i) => {
        const { paragraph } = this.state;
        var newParagraph = { paragraph };
        //alert(JSON.stringify(newParagraph.paragraph));
        newParagraph[i] = e.target.value;       
        this.setState({ paragraph: newParagraph[i] });       
    }
    onFocus(i) {
        this.setState({ editing: true }, () => {
            this.refs.input.focus();
        });
    }

    onBlur() {
        this.setState({ editing: false });

    }
    openDialog = (e) => {
        //console.log(this.props.paragraphsKey)
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }
    //handleChangeParagraph() {
    //    this.setstate(this.state.dataDocParagraphs = e.target.value)
    //}
    render() {
        const { DocParagraph, company, index, editing } = this.state;
        return (
            <li key={index} className="doc_paragraphs_li spisok">
                    {/*<h3 className={"docParagraphHeadText"}>{DocParagraph.companiesShortName}</h3>*/}
                <span id={DocParagraph.companiesId} onClick={e => this.props.openPopup(e, this.state.showPopup)} className="docParagraphsId num_prilozhenija_k_dogovoru">{index+1}.</span>
                <span id={DocParagraph.companiesId} className="prisoedinenie_k_uslovijam_head" onClick={e => this.props.openPopupCompanyDetailed(e, this.state.showPopupCompanyDetailed)}>{DocParagraph.companiesShortName}</span>
                <span className="naimenovanie_dokumenta_head upper_head">{DocParagraph.companiesTelephone}.</span>
                <span className="cancel_join_head">{DocParagraph.companiesEmail}.</span>
                    {/* <span id={DocParagraph.docParagraphsId} onClick={e => this.openDialog(e)} className="docParagraphsId">{DocParagraph.docParagraphsId}.</span>*/}
                    {/* <div style={{ "display": "none" }}>{DocParagraph.docParagraphsHeadId}</div>*/}
                    {/* <div>{company}</div>*/}                
                  </li>
            )
    }
}
class DocParagraphsCompanydetailed extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
           

            DocParagraph: props.filteredCompany,
            //paragraph: props.paragraph.docParagraphsText,
            index: props.index,
            editing: false,

        }
    }


    updateText = (e, i) => {
        const { paragraph } = this.state;
        var newParagraph = { paragraph };
        //alert(JSON.stringify(newParagraph.paragraph));
        newParagraph[i] = e.target.value;
        this.setState({ paragraph: newParagraph[i] });
    }
    onFocus(i) {
        this.setState({ editing: true }, () => {
            this.refs.input.focus();
        });
    }

    onBlur() {
        this.setState({ editing: false });

    }
    openDialog = (e) => {
        //console.log(this.props.paragraphsKey)
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }
    //handleChangeParagraph() {
    //    this.setstate(this.state.dataDocParagraphs = e.target.value)
    //}
    render() {
        const { DocParagraph, company, index, editing } = this.state;
        return (
            <div className="doc_paragraphs_li spisok_detailed">              
                <span > {DocParagraph.companiesShortName}</span>
                <span >{DocParagraph.companiesTelephone}.</span>
                <span >{DocParagraph.companiesEmail}.</span>
                <span >{DocParagraph.companiesINN}.</span>
                <span >{DocParagraph.companiesKPP}.</span>
                <span >{DocParagraph.companiesAdress}.</span>
            </div>
        )
    }
}
class Prilozhenie1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            prilozhenie1: props.prilozhenie1,
            openPopupAddPointfromReglament: props.openPopupAddPointfromReglament,
            


        }
    }
    componentDidMount() {
        let paragraphsreglament = this.props.reglament1Paragraphs
        $('.reglament1ParagraphsId').click(function () {
            let reglamentParagraphText = $(this).closest('.doc_paragraphs_li').find('div').text();
            paragraphsreglament.push(reglamentParagraphText);

        });
        
    }
    render() {
        const { prilozhenie1, openPopupAddPointfromReglament } = this.state;
        return (
            <ol className="spisok_prisoedinenija" style={{ display: this.props.reglament1Unfold ? 'inline' : 'none' }}>
                {
                    prilozhenie1.map((prilozhenie, index) => {
                        return <Prilozhenie1Text key={index} prilozhenie={prilozhenie} openPopupAddPointfromReglament={openPopupAddPointfromReglament}/>
                    })
                }
            </ol>
        )
    }
}
class Prilozhenie2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            prilozhenie2: props.prilozhenie2,
            openPopupAddPointfromReglament: props.openPopupAddPointfromReglament,
            
           
        }
    }
    componentDidMount() {
        let paragraphsreglament = this.props.reglament2Paragraphs
        $('.reglament2ParagraphsId').click(function () {
            let reglamentParagraphText = $(this).closest('.doc_paragraphs_li').find('div').text();
            paragraphsreglament.push(reglamentParagraphText);

        });
    }
    render() {
        const { prilozhenie2, openPopupAddPointfromReglament} = this.state;
        return(
            <ol className="spisok_prisoedinenija" style={{ display: this.props.reglament2Unfold ? 'inline' : 'none' }}>
                {
                    prilozhenie2.map((prilozhenie, index) => {
                        return <Prilozhenie2Text key={index} prilozhenie={prilozhenie} openPopupAddPointfromReglament={openPopupAddPointfromReglament}/>
                    })
                }
            </ol>
        )
    }
}
class Prilozhenie3 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            prilozhenie3: props.prilozhenie3,
            openPopupAddPointfromReglament: props.openPopupAddPointfromReglament,
           
        }
    }
    componentDidMount() {
        let paragraphsreglament = this.props.reglament3Paragraphs
        $('.reglament3ParagraphsId').click(function () {
            let reglamentParagraphText = $(this).closest('.doc_paragraphs_li').find('div').text();
            paragraphsreglament.push(reglamentParagraphText);

        });
    }
    render() {
        const { prilozhenie3, openPopupAddPointfromReglament  } = this.state;
        return (
            <ol className="spisok_prisoedinenija" style={{ display: this.props.reglament3Unfold ? 'inline' : 'none' }}>
                {
                    prilozhenie3.map((prilozhenie, index) => {
                        return <Prilozhenie3Text key={index} prilozhenie={prilozhenie} openPopupAddPointfromReglament={openPopupAddPointfromReglament}/>
                    })
                }
            </ol>
        )
    }
}
class Prilozhenie4 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            prilozhenie4: props.prilozhenie4,
            openPopupAddPointfromReglament: props.openPopupAddPointfromReglament,
            
        }
    }
    componentDidMount() {
        let paragraphsreglament = this.props.reglament4Paragraphs
        $('.reglament4ParagraphsId').click(function () {
            let reglamentParagraphText = $(this).closest('.doc_paragraphs_li').find('div').text();
            paragraphsreglament.push(reglamentParagraphText);

        });
    }
    render() {
        const { prilozhenie4, openPopupAddPointfromReglament  } = this.state;
        return (
            <ol className="spisok_prisoedinenija" style={{ display: this.props.reglament4Unfold ? 'inline' : 'none' }}>
                {
                    prilozhenie4.map((prilozhenie, index) => {
                        return <Prilozhenie4Text key={index} prilozhenie={prilozhenie} openPopupAddPointfromReglament={openPopupAddPointfromReglament}/>
                    })
                }
            </ol>
        )
    }
}
class Prilozhenie5 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            prilozhenie5: props.prilozhenie5,
            openPopupAddPointfromReglament: props.openPopupAddPointfromReglament,
           
        }
    }
    componentDidMount() {
        let paragraphsreglament = this.props.reglament5Paragraphs
        $('.reglament5ParagraphsId').click(function () {
            let reglamentParagraphText = $(this).closest('.doc_paragraphs_li').find('div').text();
            paragraphsreglament.push(reglamentParagraphText);

        });
    }
    render() {
        const { prilozhenie5, openPopupAddPointfromReglament } = this.state;
        return (
            <ol className="spisok_prisoedinenija" style={{ display: this.props.reglament5Unfold ? 'inline' : 'none' }}>
                {
                    prilozhenie5.map((prilozhenie, index) => {
                        return <Prilozhenie5Text key={index} prilozhenie={prilozhenie} openPopupAddPointfromReglament={openPopupAddPointfromReglament}/>
                    })
                }
            </ol>
        )
    }
}
class Prilozhenie6 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            prilozhenie6: props.prilozhenie6,
            openPopupAddPointfromReglament: props.openPopupAddPointfromReglament,
            
        }
    }
    componentDidMount() {
        let paragraphsreglament = this.props.reglament6Paragraphs
        $('.reglament6ParagraphsId').click(function () {
            let reglamentParagraphText = $(this).closest('.doc_paragraphs_li').find('div').text();
            paragraphsreglament.push(reglamentParagraphText);

        });
    }
    render() {
        const { prilozhenie6, openPopupAddPointfromReglament  } = this.state;
        return (
            <ol className="spisok_prisoedinenija" style={{ display: this.props.reglament6Unfold ? 'inline' : 'none' }}>
                {
                    prilozhenie6.map((prilozhenie, index) => {
                        return <Prilozhenie6Text key={index} prilozhenie={prilozhenie} openPopupAddPointfromReglament={openPopupAddPointfromReglament}/>
                    })
                }
            </ol>
        )
    }
}
class Prilozhenie7 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            prilozhenie7: props.prilozhenie7,
            openPopupAddPointfromReglament: props.openPopupAddPointfromReglament,
            
        }
    }
    componentDidMount() {
        let paragraphsreglament = this.props.reglament7Paragraphs
        $('.reglament7ParagraphsId').click(function () {
            let reglamentParagraphText = $(this).closest('.doc_paragraphs_li').find('div').text();
            paragraphsreglament.push(reglamentParagraphText);

        });
    }
    render() {
        const { prilozhenie7, openPopupAddPointfromReglament } = this.state;
        return (
            <ol className="spisok_prisoedinenija" style={{ display: this.props.reglament7Unfold ? 'inline' : 'none' }}>
                {
                    prilozhenie7.map((prilozhenie, index) => {
                        return <Prilozhenie7Text key={index} prilozhenie={prilozhenie} openPopupAddPointfromReglament={openPopupAddPointfromReglament}/>
                    })
                }
            </ol>
        )
    }
}
class Prilozhenie8 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            prilozhenie8: props.prilozhenie8,
            openPopupAddPointfromReglament: props.openPopupAddPointfromReglament,
            
        }
    }
    componentDidMount() {
        let paragraphsreglament = this.props.reglament8Paragraphs
        $('.reglament8ParagraphsId').click(function () {
            let reglamentParagraphText = $(this).closest('.doc_paragraphs_li').find('div').text();
            paragraphsreglament.push(reglamentParagraphText);

        });
    }
    render() {
        const { prilozhenie8, openPopupAddPointfromReglament } = this.state;
        return (
            <ol className="spisok_prisoedinenija" style={{ display: this.props.reglament8Unfold ? 'inline' : 'none' }}>
                {
                    prilozhenie8.map((prilozhenie, index) => {
                        return <Prilozhenie8Text key={index} prilozhenie={prilozhenie} openPopupAddPointfromReglament={openPopupAddPointfromReglament}/>
                    })
                }
            </ol>
        )
    }
}
class Prilozhenie9 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            prilozhenie9: props.prilozhenie9,
            openPopupAddPointfromReglament: props.openPopupAddPointfromReglament,
             
        }
    }
    componentDidMount() {
        let paragraphsreglament = this.props.reglament9Paragraphs
        $('.reglament9ParagraphsId').click(function () {
            let reglamentParagraphText = $(this).closest('.doc_paragraphs_li').find('div').text();
            paragraphsreglament.push(reglamentParagraphText);

        });
    }
    render() {
        const { prilozhenie9, openPopupAddPointfromReglament } = this.state;
        return (
            <ol className="spisok_prisoedinenija" style={{ display: this.props.reglament9Unfold ? 'inline' : 'none' }}>
                {
                    prilozhenie9.map((prilozhenie, index) => {
                        return <Prilozhenie9Text key={index} prilozhenie={prilozhenie} openPopupAddPointfromReglament={openPopupAddPointfromReglament}/>
                    })
                }
            </ol>
        )
    }
}
class Prilozhenie1Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Pril1: props.prilozhenie,
            prilozhenie1: props.prilozhenie.prilozhenie1Text,
            index: props.index,
            showReglamentPopup: true,
            showAddPoint: true,
            openPopup: props.openPopup,
            openAddPoint: props.openDialog,
            openDialog: props.openAddPoint,
            paragraphsKey: props.paragraphsKey,
        }
    }
    componentDidMount() {

       
    }


    openDialogPrilozhenie1 = (e) => {
        //console.log(this.props.paragraphsKey)
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }
    //handleChangeParagraph() {
    //    this.setstate(this.state.dataDocParagraphs = e.target.value)
    //}
    render() {
        const { Pril1, prilozhenie1, index } = this.state;
        return (
            /* <Text>*/
            <li key={index} className="doc_paragraphs_li">

                <h3 className={"docParagraphHeadText1"}>{Pril1.prilozhenie1Head}</h3>
                <span id={Pril1.prilozhenie1Id} onClick={e => this.props.openPopupAddPointfromReglament(e, this.state.showReglamentPopup)} className="reglament1ParagraphsId">1.{Pril1.prilozhenie1Id}.</span>
                <div>{prilozhenie1}</div>

            </li>
            /*</Text>*/
        )

    }
}
class Prilozhenie2Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Pril2: props.prilozhenie,
            prilozhenie2: props.prilozhenie.prilozhenie2Text,
            index: props.index,           
            showReglamentPopup: true,
            showAddPoint: true,
            openPopup: props.openPopup,
            openAddPoint: props.openDialog,
            openDialog: props.openAddPoint,
            paragraphsKey: props.paragraphsKey,
        }
    }
    

    openDialogPrilozhenie1 = (e) => {
        //console.log(this.props.paragraphsKey)
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }
    //handleChangeParagraph() {
    //    this.setstate(this.state.dataDocParagraphs = e.target.value)
    //}
    render() {
        const { Pril2, prilozhenie2, index} = this.state;
        return (
            /* <Text>*/
            <li key={index} className="doc_paragraphs_li">
                <h3 className={"docParagraphHeadText2"}>{Pril2.prilozhenie2Head}</h3>
                <span id={Pril2.prilozhenie2Id} onClick={e => this.props.openPopupAddPointfromReglament(e, this.state.showReglamentPopup)} className="reglament2ParagraphsId">2.{Pril2.prilozhenie2Id}.</span>
                <div>{prilozhenie2}</div>                
            </li>
            /*</Text>*/
        )

    }
}
class Prilozhenie3Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Pril3: props.prilozhenie,
            prilozhenie3: props.prilozhenie.prilozhenie3Text,
            index: props.index,
            showReglamentPopup: true,
            showAddPoint: true,
            openPopup: props.openPopup,
            openAddPoint: props.openDialog,
            openDialog: props.openAddPoint,
            paragraphsKey: props.paragraphsKey,
        }
    }

    openDialogPrilozhenie1 = (e) => {        
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }    
    render() {
        const { Pril3, prilozhenie3, index } = this.state;
        return (            
            <li key={index} className="doc_paragraphs_li">
                <h3 className={"docParagraphHeadText3"}>{Pril3.prilozhenie3Head}</h3>
                <span id={Pril3.prilozhenie3Id} onClick={e => this.props.openPopupAddPointfromReglament(e, this.state.showReglamentPopup)} className="reglament3ParagraphsId">3.{Pril3.prilozhenie3Id}.</span>
                <div>{prilozhenie3}</div>
            </li>            
        )

    }
}
class Prilozhenie4Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Pril4: props.prilozhenie,
            prilozhenie4: props.prilozhenie.prilozhenie4Text,
            index: props.index,
            showReglamentPopup: true,
            showAddPoint: true,
            openPopup: props.openPopup,
            openAddPoint: props.openDialog,
            openDialog: props.openAddPoint,
            paragraphsKey: props.paragraphsKey,
        }
    }

    openDialogPrilozhenie1 = (e) => {
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }
    render() {
        const { Pril4, prilozhenie4, index } = this.state;
        return (
            <li key={index} className="doc_paragraphs_li">
                <h3 className={"docParagraphHeadText4"}>{Pril4.prilozhenie4Head}</h3>
                <span id={Pril4.prilozhenie4Id} onClick={e => this.props.openPopupAddPointfromReglament(e, this.state.showReglamentPopup)} className="reglament4ParagraphsId">4.{Pril4.prilozhenie4Id}.</span>
                <div>{prilozhenie4}</div>
            </li>
        )

    }
}
class Prilozhenie5Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Pril5: props.prilozhenie,
            prilozhenie5: props.prilozhenie.prilozhenie5Text,
            index: props.index,
            showReglamentPopup: true,
            showAddPoint: true,
            openPopup: props.openPopup,
            openAddPoint: props.openDialog,
            openDialog: props.openAddPoint,
            paragraphsKey: props.paragraphsKey,
        }
    }

    openDialogPrilozhenie1 = (e) => {
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }
    render() {
        const { Pril5, prilozhenie5, index } = this.state;
        return (
            <li key={index} className="doc_paragraphs_li">
                <h3 className={"docParagraphHeadText5"}>{Pril5.prilozhenie5Head}</h3>
                <span id={Pril5.prilozhenie5Id} onClick={e => this.props.openPopupAddPointfromReglament(e, this.state.showReglamentPopup)} className="reglament5ParagraphsId">5.{Pril5.prilozhenie5Id}.</span>
                <div>{prilozhenie5}</div>
            </li>
        )

    }
}
class Prilozhenie6Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Pril6: props.prilozhenie,
            prilozhenie6: props.prilozhenie.prilozhenie6Text,
            index: props.index,
            showReglamentPopup: true,
            showAddPoint: true,
            openPopup: props.openPopup,
            openAddPoint: props.openDialog,
            openDialog: props.openAddPoint,
            paragraphsKey: props.paragraphsKey,
        }
    }

    openDialogPrilozhenie1 = (e) => {
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }
    render() {
        const { Pril6, prilozhenie6, index } = this.state;
        return (
            <li key={index} className="doc_paragraphs_li">
                <h3 className={"docParagraphHeadText6"}>{Pril6.prilozhenie6Head}</h3>
                <span id={Pril6.prilozhenie6Id} onClick={e => this.props.openPopupAddPointfromReglament(e, this.state.showReglamentPopup)} className="reglament6ParagraphsId">6.{Pril6.prilozhenie6Id}.</span>
                <div>{prilozhenie6}</div>
            </li>
        )

    }
}
class Prilozhenie7Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Pril7: props.prilozhenie,
            prilozhenie7: props.prilozhenie.prilozhenie7Text,
            index: props.index,
            showReglamentPopup: true,
            showAddPoint: true,
            openPopup: props.openPopup,
            openAddPoint: props.openDialog,
            openDialog: props.openAddPoint,
            paragraphsKey: props.paragraphsKey,
        }
    }

    openDialogPrilozhenie1 = (e) => {
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }
    render() {
        const { Pril7, prilozhenie7, index } = this.state;
        return (
            <li key={index} className="doc_paragraphs_li">
                <h3 className={"docParagraphHeadText7"}>{Pril7.prilozhenie7Head}</h3>
                <span id={Pril7.prilozhenie7Id} onClick={e => this.props.openPopupAddPointfromReglament(e, this.state.showReglamentPopup)} className="reglament7ParagraphsId">7.{Pril7.prilozhenie7Id}.</span>
                <div>{prilozhenie7}</div>
            </li>
        )

    }
}
class Prilozhenie8Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Pril8: props.prilozhenie,
            prilozhenie8: props.prilozhenie.prilozhenie8Text,
            index: props.index,
            showReglamentPopup: true,
            showAddPoint: true,
            openPopup: props.openPopup,
            openAddPoint: props.openDialog,
            openDialog: props.openAddPoint,
            paragraphsKey: props.paragraphsKey,
        }
    }

    openDialogPrilozhenie1 = (e) => {
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }
    render() {
        const { Pril8, prilozhenie8, index } = this.state;
        return (
            <li key={index} className="doc_paragraphs_li">
                <h3 className={"docParagraphHeadText8"}>{Pril8.prilozhenie8Head}</h3>
                <span id={Pril8.prilozhenie8Id} onClick={e => this.props.openPopupAddPointfromReglament(e, this.state.showReglamentPopup)} className="reglament8ParagraphsId">8.{Pril8.prilozhenie8Id}.</span>
                <div>{prilozhenie8}</div>
            </li>
        )

    }
}
class Prilozhenie9Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Pril9: props.prilozhenie,
            prilozhenie9: props.prilozhenie.prilozhenie9Text,
            index: props.index,
            showReglamentPopup: true,
            showAddPoint: true,
            openPopup: props.openPopup,
            openAddPoint: props.openDialog,
            openDialog: props.openAddPoint,
            paragraphsKey: props.paragraphsKey,
        }
    }

    openDialogPrilozhenie1 = (e) => {
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }
    render() {
        const { Pril9, prilozhenie9, index } = this.state;
        return (
            <li key={index} className="doc_paragraphs_li">
                <h3 className={"docParagraphHeadText9"}>{Pril9.prilozhenie9Head}</h3>
                <span id={Pril9.prilozhenie9Id} onClick={e => this.props.openPopupAddPointfromReglament(e, this.state.showReglamentPopup)} className="reglament9ParagraphsId">9.{Pril9.prilozhenie9Id}.</span>
                <div>{prilozhenie9}</div>
            </li>
        )

    }
}

class Price1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            price1: props.price1,
            
        }
    }
    componentDidMount() {
        let paragraphsreglament = this.props.price1Paragraphs
        $('.Price1ParagraphsId').click(function () {
            let reglamentParagraphText = $(this).closest('.doc_paragraphs_li').find('div').find('.price_stoimost_spisok').text();
            console.log(reglamentParagraphText)
            paragraphsreglament.push(reglamentParagraphText);

        });

    }
    render() {
        const { price1 } = this.state;
        return (
            <div style={{ display: this.props.price1Unfold ? 'inline' : 'none' }}>
                <div className="price_head">
                    <div className="price_punkt_dokumenta">№</div>
                    <div className="price_naimenovanie_uslugi">Наименование услуги</div>
                    <div className="price_edeniza_izmereniya">еденица измерения</div>
                    <div className="price_stoimost"><span>Стоимость руб.</span><span>(в т.ч. НДС 20%)</span></div>
                </div>
                <ol className="price_wrapper" >
                    {
                        price1.map((price, index) => {
                            return <Price1Text key={index} index={index} price={price} openPopupAddPointfromReglament={this.props.openPopupAddPointfromReglament}/>
                        })
                    }
                </ol>
            </div>
        )
    }
}
class Price2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            price2: props.price2,
            openPopupAddPointfromReglament: props.openPopupAddPointfromReglament,
        }
    }
    componentDidMount() {
        let paragraphsreglament = this.props.price2Paragraphs
        $('.Price2ParagraphsId').click(function () {
            let reglamentParagraphText = $(this).closest('.doc_paragraphs_li').find('div').find('.price_stoimost_spisok').text();
            //console.log(reglamentParagraphText)
            paragraphsreglament.push(reglamentParagraphText);

        });

    }
    render() {
        const { price2 } = this.state;
        return (
            <div style={{ display: this.props.price2Unfold ? 'inline' : 'none' }}>
                <div className="price_head">
                    <div className="price_punkt_dokumenta">№</div>
                    <div className="price_naimenovanie_uslugi">Наименование услуги</div>
                    <div className="price_edeniza_izmereniya">еденица измерения</div>
                    <div className="price_stoimost"><span>Стоимость руб.</span><span>(в т.ч. НДС 20%)</span></div>
                </div>
                <ol className="price_wrapper" >
                    {
                        price2.map((price, index) => {
                            return <Price2Text key={index} index={index} price={price} openPopupAddPointfromReglament={this.props.openPopupAddPointfromReglament}/>
                        })
                    }
                </ol>
            </div>
        )
    }
}
class Price3 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            price3: props.price3,
            openPopupAddPointfromReglament: props.openPopupAddPointfromReglament,
        }
    }
    componentDidMount() {
        let paragraphsreglament = this.props.price3Paragraphs
        $('.Price3ParagraphsId').click(function () {
            let reglamentParagraphText = $(this).closest('.doc_paragraphs_li').find('div').find('.price_stoimost_spisok').text();
            console.log(reglamentParagraphText)

            paragraphsreglament.push(reglamentParagraphText);
            //console.log(paragraphsreglament)

        });
    }
    render() {
        const { price3 } = this.state;
        return (
            <div style={{ display: this.props.price3Unfold ? 'inline' : 'none' }}>
                <div className="price_head">
                    <div className="price_punkt_dokumenta">№</div>
                    <div className="price_naimenovanie_uslugi">Наименование услуги</div>
                    <div className="price_edeniza_izmereniya">еденица измерения</div>
                    <div className="price_stoimost"><span>Стоимость руб.</span><span>(в т.ч. НДС 20%)</span></div>
                </div>
                <ol className="price_wrapper" >
                    {
                        price3.map((price, index) => {
                            return <Price3Text key={index} index={index} price={price} openPopupAddPointfromReglament={this.props.openPopupAddPointfromReglament}/>
                        })
                    }
                </ol>
            </div>
        )
    }
}
class Price5 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            price5: props.price5,
            openPopupAddPointfromReglament: props.openPopupAddPointfromReglament,
        }
    }
    componentDidMount() {
        let paragraphsreglament = this.props.price5Paragraphs
        $('.Price5ParagraphsId').click(function () {
            let reglamentParagraphText = $(this).closest('.doc_paragraphs_li').find('div').find('.price_stoimost_spisok').text();
            //console.log(reglamentParagraphText)
            paragraphsreglament.push(reglamentParagraphText);

        });
    }
    render() {
        const { price5 } = this.state;
        return (
            <div style={{ display: this.props.price5Unfold ? 'inline' : 'none' }}>
                <div className="price_head">
                    <div className="price_punkt_dokumenta">№</div>
                    <div className="price_naimenovanie_uslugi">Наименование услуги</div>
                    <div className="price_edeniza_izmereniya">еденица измерения</div>
                    <div className="price_stoimost"><span>Стоимость руб.</span><span>(в т.ч. НДС 20%)</span></div>
                </div>
                <ol className="price_wrapper" >
                    {
                        price5.map((price, index) => {
                            return <Price5Text key={index} index={index} price={price} openPopupAddPointfromReglament={this.props.openPopupAddPointfromReglament}/>
                        })
                    }
                </ol>
            </div>
        )
    }
}
class Price6 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            price6: props.price6,
            openPopupAddPointfromReglament: props.openPopupAddPointfromReglament,
        }
    }
    componentDidMount() {
        let paragraphsreglament = this.props.price6Paragraphs
        $('.Price6ParagraphsId').click(function () {
            let reglamentParagraphText = $(this).closest('.doc_paragraphs_li').find('div').find('.price_stoimost_spisok').text();
            //console.log(reglamentParagraphText)
            paragraphsreglament.push(reglamentParagraphText);

        });
    }
    render() {
        const { price6 } = this.state;
        return (
            <div style={{ display: this.props.price6Unfold ? 'inline' : 'none' }}>
                <div className="price_head">
                    <div className="price_punkt_dokumenta">№</div>
                    <div className="price_naimenovanie_uslugi">Наименование услуги</div>
                    <div className="price_edeniza_izmereniya">еденица измерения</div>
                    <div className="price_stoimost"><span>Стоимость руб.</span><span>(в т.ч. НДС 20%)</span></div>
                </div>
                <ol className="price_wrapper" >
                    {
                        price6.map((price, index) => {
                            return <Price6Text key={index} index={index} price={price} openPopupAddPointfromReglament={this.props.openPopupAddPointfromReglament} />
                        })
                    }
                </ol>
            </div>
        )
    }
}
class Price7 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            price7: props.price7,
            openPopupAddPointfromReglament: props.openPopupAddPointfromReglament,
        }
    }
    componentDidMount() {
        let paragraphsreglament = this.props.price7Paragraphs
        $('.Price7ParagraphsId').click(function () {
            let reglamentParagraphText = $(this).closest('.doc_paragraphs_li').find('div').find('.price_stoimost_spisok').text();
            //console.log(reglamentParagraphText)
            paragraphsreglament.push(reglamentParagraphText);

        });
    }
    render() {
        const { price7 } = this.state;
        return (
            <div style={{ display: this.props.price7Unfold ? 'inline' : 'none' }}>
                <div className="price_head">
                    <div className="price_punkt_dokumenta">№</div>
                    <div className="price_naimenovanie_uslugi">Наименование услуги</div>
                    <div className="price_edeniza_izmereniya">еденица измерения</div>
                    <div className="price_stoimost"><span>Стоимость руб.</span><span>(в т.ч. НДС 20%)</span></div>
                </div>
                <ol className="price_wrapper" >
                    {
                        price7.map((price, index) => {
                            return <Price7Text key={index} index={index} price={price} openPopupAddPointfromReglament={this.props.openPopupAddPointfromReglament} />
                        })
                    }
                </ol>
            </div>
        )
    }
}
class Price8 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            price8: props.price8,
            openPopupAddPointfromReglament: props.openPopupAddPointfromReglament,
        }
    }
    componentDidMount() {
        let paragraphsreglament = this.props.price8Paragraphs
        $('.Price8ParagraphsId').click(function () {
            let reglamentParagraphText = $(this).closest('.doc_paragraphs_li').find('div').find('.price_stoimost_spisok').text();
            //console.log(reglamentParagraphText)
            paragraphsreglament.push(reglamentParagraphText);

        });
    }
    render() {
        const { price8 } = this.state;
        return (
            <div style={{ display: this.props.price8Unfold ? 'inline' : 'none' }}>
                <div className="price_head">
                    <div className="price_punkt_dokumenta">№</div>
                    <div className="price_naimenovanie_uslugi">Наименование услуги</div>
                    <div className="price_edeniza_izmereniya">еденица измерения</div>
                    <div className="price_stoimost"><span>Стоимость руб.</span><span>(в т.ч. НДС 20%)</span></div>
                </div>
                <ol className="price_wrapper" >
                    {
                        price8.map((price, index) => {
                            return <Price8Text key={index} index={index} price={price} openPopupAddPointfromReglament={this.props.openPopupAddPointfromReglament} />
                        })
                    }
                </ol>
            </div>
        )
    }
}
class Price10 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            price10: props.price10,
            openPopupAddPointfromReglament: props.openPopupAddPointfromReglament,
        }
    }
    componentDidMount() {
        let paragraphsreglament = this.props.price10Paragraphs
        $('.Price10ParagraphsId').click(function () {
            let reglamentParagraphText = $(this).closest('.doc_paragraphs_li').find('div').find('.price_stoimost_spisok').text();
            console.log(reglamentParagraphText)
            paragraphsreglament.push(reglamentParagraphText);

        });
    }
    render() {
        const { price10 } = this.state;
        return (
            <div style={{ display: this.props.price10Unfold ? 'inline' : 'none' }}>
                <div className="price_head">
                    <div className="price_punkt_dokumenta">№</div>
                    <div className="price_naimenovanie_uslugi">Наименование услуги</div>
                    <div className="price_edeniza_izmereniya">еденица измерения</div>
                    <div className="price_stoimost"><span>Стоимость руб.</span><span>(в т.ч. НДС 20%)</span></div>
                </div>
                <ol className="price_wrapper" >
                    {
                        price10.map((price, index) => {
                            return <Price10Text key={index} index={index} price={price} openPopupAddPointfromReglament={this.props.openPopupAddPointfromReglament} />
                        })
                    }
                </ol>
            </div>
        )
    }
}
class Price1Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            index: props.index,
            price: props.price,
            showReglamentPopup: true,
        }
    }

    openDialogPrilozhenie1 = (e) => {
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }
    render() {
        const { index } = this.state;
        return (
            <li key={index} className="doc_paragraphs_li">
                <div className="price_tr">
                    
                                  
                   
                   {/* <h3 className={"docParagraphHeadText9"}>{this.props.price.prilozhenie10Head}</h3>*/}

                    <div className="price_punkt_dokumenta_spisok"  className="price_punkt_dokumenta_spisok">
                        <strong><span id={this.props.price.prilozhenie10Punkt} className="Price1ParagraphsId" onClick={e => this.props.openPopupAddPointfromReglament(e, this.state.showReglamentPopup)}>10.{this.props.price.prilozhenie10Punkt}.</span></strong>
                    </div>
                    <div className="price_naimenovanie_uslugi_spisok">
                        <div className="PriceText">{this.props.price.prilozhenie10Text}</div>
                        <div className="PriceFoot">{this.props.price.prilozhenie10Foot}</div>
                        <div className="PricePrim"><a href="#">прим. {this.props.price.prilozhenie10Prim}</a></div>
                    </div>
                   
                    
                    <div className="price_edeniza_izmereniya_spisok">{this.props.price.prilozhenie10EdenicaIzmerenija}</div>
                    <div className="price_stoimost_spisok">{this.props.price.prilozhenie10Stoimost}</div>

                </div>
            </li>
        )
    }
}
class Price2Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            index: props.index,
            price: props.price,
            showReglamentPopup: true,
        }
    }

    openDialogPrilozhenie1 = (e) => {
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }
    render() {
        const { index } = this.state;
        return (
            <li key={index} className="doc_paragraphs_li">
                <div className="price_tr">



                    {/* <h3 className={"docParagraphHeadText9"}>{this.props.price.prilozhenie10Head}</h3>*/}

                    <div className="price_punkt_dokumenta_spisok" id={this.props.price.prilozhenie11Punkt} className="price_punkt_dokumenta_spisok">
                        <strong><span id={this.props.price.prilozhenie11Punkt} className="Price2ParagraphsId" onClick={e => this.props.openPopupAddPointfromReglament(e, this.state.showReglamentPopup)}>11.{this.props.price.prilozhenie11Punkt}.</span></strong>
                    </div>
                    <div className="price_naimenovanie_uslugi_spisok">
                        <div className="PriceText">{this.props.price.prilozhenie11Text}</div>
                        <div className="PriceFoot">{this.props.price.prilozhenie11Foot}</div>
                        <div className="PricePrim"><a href="#">прим. {this.props.price.prilozhenie11Prim}</a></div>
                    </div>


                    <div className="price_edeniza_izmereniya_spisok">{this.props.price.prilozhenie11EdenicaIzmerenija}</div>
                    <div className="price_stoimost_spisok">{this.props.price.prilozhenie11Stoimost}</div>

                </div>
            </li>
        )
    }
}
class Price3Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            index: props.index,
            price: props.price,
            showReglamentPopup: true,
        }
    }

    openDialogPrilozhenie1 = (e) => {
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }
    render() {
        const { index } = this.state;
        return (
            <li key={index} className="doc_paragraphs_li">
                <div className="price_tr">



                    {/* <h3 className={"docParagraphHeadText9"}>{this.props.price.prilozhenie10Head}</h3>*/}

                    <div className="price_punkt_dokumenta_spisok" id={this.props.price.prilozhenie12Punkt} className="price_punkt_dokumenta_spisok">
                        <strong><span id={this.props.price.prilozhenie12Punkt} className="Price3ParagraphsId" onClick={e => this.props.openPopupAddPointfromReglament(e, this.state.showReglamentPopup)}>12.{this.props.price.prilozhenie12Punkt}.</span></strong>
                    </div>
                    <div className="price_naimenovanie_uslugi_spisok">
                        <div className="PriceText">{this.props.price.prilozhenie12Text}</div>
                        <div className="PriceFoot">{this.props.price.prilozhenie12Foot}</div>
                        <div className="PricePrim"><a href="#">прим. {this.props.price.prilozhenie12Prim}</a></div>
                    </div>


                    <div className="price_edeniza_izmereniya_spisok">{this.props.price.prilozhenie12EdenicaIzmerenija}</div>
                    <div className="price_stoimost_spisok">{this.props.price.prilozhenie12Stoimost}</div>

                </div>
            </li>
        )
    }
}
class Price5Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            index: props.index,
            price: props.price,
            showReglamentPopup: true,
        }
    }

    openDialogPrilozhenie1 = (e) => {
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }
    render() {
        const { index } = this.state;
        return (
            <li key={index} className="doc_paragraphs_li">
                <div className="price_tr">



                    {/* <h3 className={"docParagraphHeadText9"}>{this.props.price.prilozhenie10Head}</h3>*/}

                    <div className="price_punkt_dokumenta_spisok" id={this.props.price.prilozhenie14Punkt} className="price_punkt_dokumenta_spisok">
                        <strong><span id={this.props.price.prilozhenie14Punkt} className="Price5ParagraphsId" onClick={e => this.props.openPopupAddPointfromReglament(e, this.state.showReglamentPopup)}>14.{this.props.price.prilozhenie14Punkt}.</span></strong>
                    </div>
                    <div className="price_naimenovanie_uslugi_spisok">
                        <div className="PriceText">{this.props.price.prilozhenie14Text}</div>
                        <div className="PriceFoot">{this.props.price.prilozhenie14Foot}</div>
                        <div className="PricePrim"><a href="#">прим. {this.props.price.prilozhenie14Prim}</a></div>
                    </div>


                    <div className="price_edeniza_izmereniya_spisok">{this.props.price.prilozhenie14EdenicaIzmerenija}</div>
                    <div className="price_stoimost_spisok">{this.props.price.prilozhenie14Stoimost}</div>

                </div>
            </li>
        )
    }
}

class Price6Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            index: props.index,
            price: props.price,
            showReglamentPopup: true,
        }
    }

    openDialogPrilozhenie1 = (e) => {
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }
    render() {
        const { index } = this.state;
        return (
            <li key={index} className="doc_paragraphs_li">
                <div className="price_tr">



                    {/* <h3 className={"docParagraphHeadText9"}>{this.props.price.prilozhenie10Head}</h3>*/}

                    <div className="price_punkt_dokumenta_spisok" id={this.props.price.prilozhenie15Punkt} className="price_punkt_dokumenta_spisok">
                        <strong><span id={this.props.price.prilozhenie15Punkt} className="Price6ParagraphsId" onClick={e => this.props.openPopupAddPointfromReglament(e, this.state.showReglamentPopup)}>15.{this.props.price.prilozhenie15Punkt}.</span></strong>
                    </div>
                    <div className="price_naimenovanie_uslugi_spisok">
                        <div className="PriceText">{this.props.price.prilozhenie15Text}</div>
                        <div className="PriceFoot">{this.props.price.prilozhenie15Foot}</div>
                        <div className="PricePrim"><a href="#">прим. {this.props.price.prilozhenie15Prim}</a></div>
                    </div>


                    <div className="price_edeniza_izmereniya_spisok">{this.props.price.prilozhenie15EdenicaIzmerenija}</div>
                    <div className="price_stoimost_spisok">{this.props.price.prilozhenie15Stoimost}</div>

                </div>
            </li>
        )
    }
}

class Price7Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            index: props.index,
            price: props.price,
            showReglamentPopup: true,
        }
    }

    openDialogPrilozhenie1 = (e) => {
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }
    render() {
        const { index } = this.state;
        return (
            <li key={index} className="doc_paragraphs_li">
                <div className="price_tr">



                    {/* <h3 className={"docParagraphHeadText9"}>{this.props.price.prilozhenie10Head}</h3>*/}

                    <div className="price_punkt_dokumenta_spisok" id={this.props.price.prilozhenie16Punkt} className="price_punkt_dokumenta_spisok">
                        <strong><span id={this.props.price.prilozhenie16Punkt} className="Price7ParagraphsId" onClick={e => this.props.openPopupAddPointfromReglament(e, this.state.showReglamentPopup)}>16.{this.props.price.prilozhenie16Punkt}.</span></strong>
                    </div>
                    <div className="price_naimenovanie_uslugi_spisok">
                        <div className="PriceText">{this.props.price.prilozhenie16Text}</div>
                        <div className="PriceFoot">{this.props.price.prilozhenie16Foot}</div>
                        <div className="PricePrim"><a href="#">прим. {this.props.price.prilozhenie16Prim}</a></div>
                    </div>


                    <div className="price_edeniza_izmereniya_spisok">{this.props.price.prilozhenie16EdenicaIzmerenija}</div>
                    <div className="price_stoimost_spisok">{this.props.price.prilozhenie16Stoimost}</div>

                </div>
            </li>
        )
    }
}
class Price8Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            index: props.index,
            price: props.price,
            showReglamentPopup: true,
        }
    }

    openDialogPrilozhenie1 = (e) => {
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }
    render() {
        const { index } = this.state;
        return (
            <li key={index} className="doc_paragraphs_li">
                <div className="price_tr">



                    {/* <h3 className={"docParagraphHeadText9"}>{this.props.price.prilozhenie10Head}</h3>*/}

                    <div className="price_punkt_dokumenta_spisok" id={this.props.price.prilozhenie17Punkt} className="price_punkt_dokumenta_spisok">
                        <strong><span id={this.props.price.prilozhenie17Punkt} className="Price8ParagraphsId" onClick={e => this.props.openPopupAddPointfromReglament(e, this.state.showReglamentPopup)}>17.{this.props.price.prilozhenie17Punkt}.</span></strong>
                    </div>
                    <div className="price_naimenovanie_uslugi_spisok">
                        <div className="PriceText">{this.props.price.prilozhenie17Text}</div>
                        <div className="PriceFoot">{this.props.price.prilozhenie17Foot}</div>
                        <div className="PricePrim"><a href="#">прим. {this.props.price.prilozhenie17Prim}</a></div>
                    </div>


                    <div className="price_edeniza_izmereniya_spisok">{this.props.price.prilozhenie17EdenicaIzmerenija}</div>
                    <div className="price_stoimost_spisok">{this.props.price.prilozhenie17Stoimost}</div>

                </div>
            </li>
        )
    }
}
class Price10Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            index: props.index,
            price: props.price,
            showReglamentPopup: true,
        }
    }

    openDialogPrilozhenie1 = (e) => {
        if (this.props.paragraphsKey.length <= 0) {
            this.props.openPopup(e, this.state.showPopup);
        }
        if (this.props.paragraphsKey.length > 0) {
            this.props.openAddPoint(e, this.state.showAddPoint);
        }
    }
    render() {
        const { index } = this.state;
        return (
            <li key={index} className="doc_paragraphs_li">
                <div className="price_tr">



                    {/* <h3 className={"docParagraphHeadText9"}>{this.props.price.prilozhenie10Head}</h3>*/}

                    <div className="price_punkt_dokumenta_spisok"  className="price_punkt_dokumenta_spisok">
                        <strong><span id={this.props.price.prilozhenie19Punkt} className="Price10ParagraphsId" onClick={e => this.props.openPopupAddPointfromReglament(e, this.state.showReglamentPopup)}>19.{this.props.price.prilozhenie19Punkt}.</span></strong>
                    </div>
                    <div className="price_naimenovanie_uslugi_spisok">
                        <div className="PriceText">{this.props.price.prilozhenie19Text}</div>
                        <div className="PriceFoot">{this.props.price.prilozhenie19Foot}</div>
                        <div className="PricePrim"><a href="#">прим. {this.props.price.prilozhenie19Prim}</a></div>
                    </div>


                    <div className="price_edeniza_izmereniya_spisok">{this.props.price.prilozhenie19EdenicaIzmerenija}</div>
                    <div className="price_stoimost_spisok">{this.props.price.prilozhenie19Stoimost}</div>

                </div>
            </li>
        )
    }
}
class ButtonJoinedToPublicDoc extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            newcontractors:[],          
            conn_1C: [],
            match: false,
            
        };
    }
    componentDidUpdate() {
       

    }
 
    connect_1C = (e) => {
        e.preventDefault();

        Promise.all(this.props.apiUrl.map(a => {
            return new Promise((resolve, reject) => {
                //using superagent here (w/o its promise api), "import request as 'superagent'. You'd import this at the top of your file.
                request.get(a)
                    .end((error, response) => {
                        if (error) {
                            return reject()
                        } else {
                            resolve(response)
                        }
                    })
            })
        }))
            .then(v => {
                //console.log(v);
                this.setState({
                    conn_1C: v[0].body,
                   
                })



            })
            .catch(() => {
                console.error("Error in data retrieval")
            })

       

    }
    createContragent =  (e) => {
        e.preventDefault();
        var match = false;
        this.state.newcontractors.map((element) => {
            if (element.inn == this.props.inn) {             
                match = true;
                this.setState({ match: match },
                   // this.reqNewContractors(this.state)
                    () => {
                        fetch(this.props.apiUrl)
                            .then(response => response.json())
                            .then(response => this.setState({ newcontractors: response }));
                    }
                )
                //this.props.setInn("");
            }            
        })
        
        alert(match)
        
        if (match == false) {
            var self = this;
            $.ajax({
                url: this.props.apiUrl,
                method: 'post',                
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'dataType': "json"

                },
                //'dataType': 'json',
                'data': JSON.stringify({
                    emails: this.props.email,
                    fullname: this.props.legalStatus + this.props.organizationName,
                    inn: this.props.inn,
                    kpp: this.props.kpp,
                    name: this.props.organizationName,
                    okpo: this.props.okpo,
                    phones: this.props.phone,
                    phoneforsms: this.props.phoneforsms,
                }),                
                beforeSend: function (data) {
                    
                    self.props.launchPreloader();
                },
                success: function (data, msg, response) {
                    var jsonUpdatedData = response.responseJSON;
                    
                    if (jsonUpdatedData != null || jsonUpdatedData != undefined) {
                        
                        self.props.getNameUzhe_zaregestrirovanogo(jsonUpdatedData);
                        self.props.popupUzhe_zaregestrirovanShow();
                    }
                    console.log(jsonUpdatedData);
                    self.props.stopPreloader();
                    self.props.setInn("");
                }
            });
            e.preventDefault();
            // let response = await fetch(this.props.apiUrl, {
            //    method: 'POST',
            //    headers: {
            //        'Accept': 'application/json',
            //        'Content-Type': 'application/json'
            //    },
            //    body: JSON.stringify({
            //        emails: this.props.email,
            //        fullname: this.props.legalStatus + this.props.organizationName,
            //        inn: this.props.inn,
            //        kpp: this.props.kpp,
            //        name: this.props.organizationName,
            //        okpo: this.props.okpo,
            //        phones: this.props.phone,
            //        phoneforsms: this.props.phoneforsms,
            //    })
            //});
            //var result = response.json();
            //console.log(JSON.stringify(result));
            //
           
        }
        this.setState({ match: match },
            //this.reqNewContractors(this.state)
             function(){
                var xhr = new XMLHttpRequest();
                xhr.open('get', this.props.apiUrl, true);
                xhr.onload = function () {
                    var response = JSON.parse(xhr.responseText);
                    //console.log(JSON.stringify(response))
                    this.setState({ newcontractors: response });
                }.bind(this);
                xhr.send();
            }
        )
    }
    reqNewContractors=()=>{
        Promise.all(this.props.apiUrl.map(a => {
            return new Promise((resolve, reject) => {
                //using superagent here (w/o its promise api), "import request as 'superagent'. You'd import this at the top of your file.
                request.get(a)
                    .end((error, response) => {
                        if (error) {
                            return reject()
                        } else {
                            resolve(response)
                        }
                    })
            })
        }))
            .then(v => {
               // console.log(JSON.stringify(v));
                this.setState({
                    newcontractors: v[0].body,
                })
            })
            .catch(() => {
                console.error("newcontractors: Error in data retrieval")
            })

    }
    componentDidMount() {
        //this.reqNewContractors()
        //this.timer = setInterval(() => this.reqNewContractors(), 2000);       
    }
    render() {
        return (           
            <div className="signature">
                <input type="submit" className="electron_signature" value="ПОДПИСАНО ЭЛЕКТРОННОЙ ПОДПИСЬЮ" />
                <input type="submit" className="join" onClick={e => this.createContragent(e)} value="ПРИСОЕДИНИТЬСЯ К ПУБЛИЧНЫМ ДОКУМЕНТАМ" />
           </div>
        );
    }
}
class ContentContract extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            companies: [],
            staff:[],
            joining: [],
            heads: [],
            
            paragraphs: [],
            paragraphsForexcel: [],
            protiokolparagraphs:[],
            showPopup: false,
            showPopupCompanyDetailed: false,
            showReglamentPopup:false,
            showProtocol: false,
            hideFormJoin: false,
            joiningReglament: [],
            joiningPrice: [],
            paragraphsKey: [],
            objectForExcel: [],

            reglament1ParagraphsKey: [],
            reglament2ParagraphsKey: [],
            reglament3ParagraphsKey: [],
            reglament4ParagraphsKey: [],
            reglament5ParagraphsKey: [],
            reglament6ParagraphsKey: [],
            reglament7ParagraphsKey: [],
            reglament8ParagraphsKey: [],
            reglament9ParagraphsKey: [],

            price1ParagraphsKey: [],
            price2ParagraphsKey: [],
            price3ParagraphsKey: [],
            
            price5ParagraphsKey: [],
            price6ParagraphsKey: [],
            price7ParagraphsKey: [],
            price8ParagraphsKey: [],
            price10ParagraphsKey: [],

            indexForProtokolReglament: null,
            showAddPoint: false,
            showPrilozhenie2: false,
            showPrilozhenie3: false,
            prilozhenie1: [],
            prilozhenie2: [],
            prilozhenie3: [],
            prilozhenie4: [],
            prilozhenie5: [],
            prilozhenie6: [],
            prilozhenie7: [],
            prilozhenie8: [],
            prilozhenie9: [],
            price1: [],
            price2: [],
            price3: [],
            price5: [],
            price6: [],
            price7: [],
            price8: [],
            price10: [],
            
            contractClient: [],
            user:"",
            userLastName: [],
            prilozhenie2Key: [],

            reglament1IndexKey: [],
            reglament2IndexKey: [],
            reglament3IndexKey: [],
            reglament4IndexKey: [],
            reglament5IndexKey: [],
            reglament6IndexKey: [],
            reglament7IndexKey: [],
            reglament8IndexKey: [],
            reglament9IndexKey: [],

            price1IndexKey: [],
            price2IndexKey: [],
            price3IndexKey: [],
            price5IndexKey: [],
            price6IndexKey: [],
            price7IndexKey: [],
            price8IndexKey: [],
            price10IndexKey: [],

            reglament1Paragraphs: [],
            reglament2Paragraphs: [],
            reglament3Paragraphs: [],
            reglament4Paragraphs: [],
            reglament5Paragraphs: [],
            reglament6Paragraphs: [],
            reglament7Paragraphs: [],
            reglament8Paragraphs: [],
            reglament9Paragraphs: [],

            price1Paragraphs: [],
            price2Paragraphs: [],
            price3Paragraphs: [],
            
            price5Paragraphs: [],
            price6Paragraphs: [],
            price7Paragraphs: [],

            price8Paragraphs: [],
            price10Paragraphs: [],
           
            reglament1Unfold: false,
            reglament2Unfold: false,
            reglament3Unfold: false,
            reglament4Unfold: false,
            reglament5Unfold: false,
            reglament6Unfold: false,
            reglament7Unfold: false,
            reglament8Unfold: false,
            reglament9Unfold: false,


            price1Unfold: false,
            price2Unfold: false,
            price3Unfold: false,
            price5Unfold: false,
            price6Unfold: false,
            price7Unfold: false,
            price8Unfold: false,
            price10Unfold: false,
            

            reglamentParagraphsIndex:[],
            reglamentIndex: 1,

            getContactorsSpisokPprisoedinenija: [],

            arrReglamentHref: [{
                id: 1,
                href: '/doc/Prilozhenie - 1 - reglament - podachi - zajavki.docx',
            }, {
                id: 2,
                href: '/doc/Prilozhenie - 2 - reglament - pribytija - avtotransporta - na - terminal.docx',
            }, {
                id: 3,
                href: '/doc/Prilozhenie-3-reglament-obrabotki-gruza-pod-tamozhennymi-procedurami.docx',
            }, {
                id: 4,
                href: '/doc/Prilozhenie-4-reglamenet-priema-otpravlenija-kontejnerov-kontrejlerov-vagonov.docx',
            }, {
                id: 5,
                href: null,
            }, {
                id: 6,
                href: null,
            }, {
                id: 7,
                href: null,
            }, {
                id: 8,
                href: null,
            }, {
                id: 9,
                href: null,
                }],
        
            legalStatus: "",
            organizationName: "",          
            downOrganizationName:"",
            doljnost: "",
            dokumentname: "",
            adress: "",
            inn: "",
            kpp: "",
            okpo: "",
            rekvizity_rc: null,
            rekvizity_bank: null,
            rekvizity_kc: null,
            rekvizity_bik: null,
            phone: "",
            phoneforsms: "",
            email: "",
            isinn: false,
            iskpp: false,
            isokpo: false,
            isbik: false,
            isrc: false,
            iskc: false,
            istelephone: false,
            istelephoneSMS: false,
            isemail: false,
            href: null,
            goloader: true,
            uzhe_zaregestrirovan: true,
            uzhe_zaregestrirovan_inn: null,
            
           /*address: null, emails: null, fullName: null, id: null, inn: null, kpp: null, name: null, 
           okpo: null, phones: null, */
          
           
            /*abbreviated: null, actualContract: null, address: null, autoDelivery: null, badPayer: null, carriageService1c: null,
            checkTtnMail: null, color: null, containerService1c: null, country: null, debet: null, diadoc: null, emails: null, fnsParticipantId: null, forAlterInter: null,
            freightCharge: null, freightChargeService: null, fullName: null, group: null, id: null, inn: null, isTrusted: null, isUnreliable: null, kod1c: null, kpp: null,
            ktPriceVersion: null, loadedGateInConstraint: null, maxDebt: null, name: null, nightGateOut: null, nightGateOutReserve: null, noNds: null, notifyContainersEmails: null,
            notifyEmails: null, okpo: null, operationsDenial: null, ownerDisposerPlatform: null, pasteTogetherNumber: null, payFreightCharge: null, phones: null, postpay: null,
            priceVersion: null, repairChargeType: null, repairContract: null, sbEmptyInbound: null, sbEmptyOutput: null, sbFullInbound: null, sbFullOutput: null, separateAtBids: null,
            separateAtBidsReceivers: null, transporter: null, useAddressFom1c: null, write: null,*/
        };
    }
    mapParagraphsForexcel() {
        function clone(obj) {
            if (null == obj || "object" != typeof obj) return obj;
            var copy = obj.constructor();
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
            }
            return copy;
        }      
        var newParagraphs = clone(this.state.paragraphs);        
        newParagraphs.forEach((item)=>{
        delete item['docParagraphsHeadId'];
        delete item['docParagraphsHeadText'];
        })
        this.setState({ paragraphsForexcel: newParagraphs})
    }
    openPopup = (e, value) => {
        console.log(e.target.id); // paragraph number element
        this.setState({ showPopup: value });
        this.state.paragraphsKey.push(e.target.id);
        //this.sortParagraphsKey();
    }
    openPopupCompanyDetailed = (e, value) => {
        console.log(e.target.id); // paragraph number element
        this.setState({ showPopupCompanyDetailed: value });
        this.state.paragraphsKey.push(e.target.id);
        //this.sortParagraphsKey();
    }
    pushReglamentParagraphsIndex() {
        this.setState(prevState => {
            return { reglamentIndex: prevState.reglamentIndex + 1 }
        })
        this.state.reglamentParagraphsIndex.push(this.state.reglamentIndex);
    }
    openPopupAddPointfromReglament = (e, value) => {
        let punkt = null;
        let punktTextContent = e.target.textContent;
        if (punktTextContent.length >= 5) {
            var from = 0;
            var to = 2;
            punkt = punktTextContent.substring(from, to);
        } else {
            punkt = e.target.textContent.slice(0, 1);
        }
        //let punkt = e.target.textContent.slice(0, 1);       

        
       // let reglamentNumber = parseFloat(e.target.id); // paragraph number element
        let reglamentNumber = parseInt(e.target.id);
        //console.log(reglamentNumber);
        this.setState({ showReglamentPopup: value },
            function () {
                console.log(punkt);
                if (punkt == 1) {
                    //console.log("записваем из 1 регламента...")
                    this.createreglament1ObjectForexcel();
                }
                if (punkt == 2) {
                    //console.log("записваем из 2 регламента...")
                    this.createreglament2ObjectForexcel();
                }
                if (punkt == 3) {
                   //console.log("записваем из 3 регламента...")
                   this.createreglament3ObjectForexcel();
                }
                if (punkt == 4) {
                    //console.log("записваем из 4 регламента...")
                    this.createreglament4ObjectForexcel();
                }
                if (punkt == 5) {
                    //console.log("записваем из 5 регламента...")
                    this.createreglament5ObjectForexcel();
                }
                if (punkt == 6) {
                    //console.log("записваем из 6 регламента...")
                    this.createreglament6ObjectForexcel();
                }
                if (punkt == 7) {
                    //console.log("записваем из 7 регламента...")
                    this.createreglament7ObjectForexcel();
                }
                if (punkt == 8) {
                    //console.log("записваем из 8 регламента...")
                    this.createreglament8ObjectForexcel();
                }
            });
        this.pushReglamentParagraphsIndex();
        //this.state.reglamentParagraphsKey.push(reglamentNumber);
        this.pushReglamentParagraphsKeys = () => {
            //console.log(reglamentNumber);
            if (this.state.indexForProtokolReglament == 0) {
                //console.log(reglamentNumber);
                this.state.reglament1ParagraphsKey.push(reglamentNumber);
            } else if (this.state.indexForProtokolReglament == 1) {
                this.state.reglament2ParagraphsKey.push(reglamentNumber);
            } else if (this.state.indexForProtokolReglament == 2) {
                this.state.reglament3ParagraphsKey.push(reglamentNumber);
            } else if (this.state.indexForProtokolReglament == 3) {
                this.state.reglament4ParagraphsKey.push(reglamentNumber);
            } else if (this.state.indexForProtokolReglament == 4) {
                this.state.reglament5ParagraphsKey.push(reglamentNumber);
            } else if (this.state.indexForProtokolReglament == 5) {
                this.state.reglament6ParagraphsKey.push(reglamentNumber);
            } else if (this.state.indexForProtokolReglament == 6) {
                this.state.reglament7ParagraphsKey.push(reglamentNumber);
            } else if (this.state.indexForProtokolReglament == 7) {
                this.state.reglament8ParagraphsKey.push(reglamentNumber);
            } else if (this.state.indexForProtokolReglament == 8) {
                this.state.reglament9ParagraphsKey.push(reglamentNumber);
            } else if (this.state.indexForProtokolReglament == 9) {
                this.state.price1ParagraphsKey.push(reglamentNumber);
            } else if (this.state.indexForProtokolReglament == 10) {
                this.state.price2ParagraphsKey.push(reglamentNumber);
            } else if (this.state.indexForProtokolReglament == 11) {
                this.state.price3ParagraphsKey.push(reglamentNumber);        
            } else if (this.state.indexForProtokolReglament == 13) {
                this.state.price5ParagraphsKey.push(reglamentNumber);
            } else if (this.state.indexForProtokolReglament == 14) {
                this.state.price6ParagraphsKey.push(reglamentNumber);
            } else if (this.state.indexForProtokolReglament == 15) {
                this.state.price7ParagraphsKey.push(reglamentNumber);
            } else if (this.state.indexForProtokolReglament == 16) {
                this.state.price8ParagraphsKey.push(reglamentNumber);
            } else if (this.state.indexForProtokolReglament == 17) {
                this.state.price10ParagraphsKey.push(reglamentNumber);
            }
        };
        this.pushReglamentParagraphsKeys();
        this.sortParagraphsKey();
        this.checkIndexForProtokolReglament();
    }
    
    closePopupAddPointfromReglament = (e, value) => {       
        this.setState({ showReglamentPopup: value });        
    }
    openAddPoint = (e, value) => {
        console.log(e.target.id); // paragraph number element
        this.setState({ showAddPoint: value });
        this.state.paragraphsKey.push(e.target.id);
    }
    openProtocolhideFormJoin = (e, value1, value2) => {
        console.log(e.target);
        this.setState({ showProtocol: value1 });
        this.setState({ hideFormJoin: value2 });
        this.setState({ showPopup: false });
        this.setState({ protokolparagraphs: this.state.paragraphs });
        //console.log(this.state.paragraphs);
        this.createObjectForexcel();
        e.preventDefault();       
    }
    openProtocolhideFormReglament = (e, value1, value2, value3) => {
        console.log(e.target);        
        this.setState({ showProtocol: value1 });
        this.setState({ showPrilozhenie2: value2 });
        this.setState({ showreglamentPopup: value3 }           
        );
        //this.setState({ protokolparagraphs: this.state.paragraphs });
        //console.log(this.state.paragraphs);
        e.preventDefault();
    }
    openPrilozhenie = (e, value1, value2) => {
        console.log(e.target.id);
       
        if (e.target.id == 2) {
            this.setState({ showPrilozhenie2: value1 });
            this.setState({ showPrilozhenie3: false });
        }
        else if (e.target.id == 3) {
            this.setState({ showPrilozhenie3: value1 });
            this.setState({ showPrilozhenie2: false });
        }
        this.setState({ hideFormJoin: value2 });
        e.preventDefault();
    }
    closeProtocol = (e, value1, valuel2) => {
        this.setState({ showProtocol: value1 });
        this.setState({ hideFormJoin: valuel2 });
        this.state.paragraphsKey.pop();
        e.preventDefault();
    }
    closePopup = (e, value) => {
        this.setState({ showPopup: value });
        e.preventDefault();
    }
    sortParagraphsKey() {

        this.state.paragraphsKey.sort(function (a, b) {
            return a - b;
        });

    }
    sortReglamentParagraphsKey() {
        //let uniqueArray = this.state.reglamentParagraphsKey.filter((item, pos)=> {
        //    return this.state.reglamentParagraphsKey.indexOf(item) == pos;           
        //});
        //let sortArray = uniqueArray.sort(function (a, b) {
        //    return a - b;
        //});
        //this.setState({ reglamentParagraphsKey: sortArray });
        this.state.reglamentParagraphsKey.sort(function (a, b) {
            return a - b;
        });
          
    } 
    closeAddPoint = (e, value) => {
        this.setState({ showAddPoint: value });
        e.preventDefault();
    }
    appendH3() {
        $('.doc_paragraphs_name').append($('.doc_paragraphs_li>h3'));
    }
    componentWillReceiveProps() {
       
       
    }

    theFirstRequest() {
        var self = this;
        $.each(this.props.apiUrl, function (i, u) {
            console.log(u);
            $.ajax(u,
                {                    
                    method: 'get',
                    beforeSend: function () {                        
                        self.launchPreloader();
                    },
                    success: function (data) {                        
                        self.stopPreloader();
                        console.log(data)
                        self.setState({
                            heads: data[0],
                            paragraphs: data[1],
                            joining: data[2],
                            prilozhenie1: data[3],
                            prilozhenie2: data[4],
                            prilozhenie3: data[5],
                            prilozhenie4: data[6],
                            prilozhenie5: data[7],
                            prilozhenie6: data[8],
                            prilozhenie7: data[9],
                            prilozhenie8: data[10],
                            prilozhenie9: data[11],
                            prilozhenie10: data[12],
                            contractClient: data[13],
                            //userLastName: data.replace(/[0-9]/g, ''),
                            getContactorsSpisokPprisoedinenija: data[15],                       
                        })
                        //alert();
                    }
                }
            );
        })        
    }
            componentDidMount() {
                //this.theFirstRequest();
                Promise.all(this.props.apiUrl.map(a => {
                    this.launchPreloader();
                    return new Promise((resolve, reject) => {
                        //using superagent here (w/o its promise api), "import request as 'superagent'. You'd import this at the top of your file.
                        request.get(a)
                            .end((error, response) => {
                                if (error) {
                                    return reject()
                                } else {
                                    resolve(response)
                                }
                            })
                    })
                }))
                    .then(v => {
                        console.log(v);
                        this.setState({
                            companies: v[0].body,
                            staff: v[1].body
                            /*heads: v[0].body,
                            paragraphs: v[1].body,
                            joining: v[2].body,
                            prilozhenie1: v[3].body,
                            prilozhenie2: v[4].body,
                            prilozhenie3: v[5].body,
                            prilozhenie4: v[6].body,
                            prilozhenie5: v[7].body,
                            prilozhenie6: v[8].body,
                            prilozhenie7: v[9].body,
                            prilozhenie8: v[10].body,
                            prilozhenie9: v[11].body,
                            
                            contractClient: v[12].body,
                            userLastName: v[13].text.replace(/[0-9]/g, ''),
                            getContactorsSpisokPprisoedinenija: v[14].body,
                            price1: v[15].body,
                            price2: v[16].body,
                            price3: v[17].body,
                            price5: v[18].body,
                            price6: v[19].body,
                            price7: v[20].body,
                            price8: v[21].body,
                            price10: v[22].body,
                            */
                        },
                            this.stopPreloader
                    )

                        
                        var joiningReglamentFilter = this.state.joining.filter(reglament => reglament.docUnitId <= 9);
                        var joiningPriceFilter = this.state.joining.filter(price => price.docUnitId > 9);
                        this.setState({ joiningReglament: joiningReglamentFilter });
                        this.setState({ joiningPrice: joiningPriceFilter },
                            this.mapParagraphsForexcel
                            
                        )

                    })
                    .catch(() => {
                        console.error("Error in data retrieval")
                        const myError = new Error('please improve your code')
                        console.log(myError.message) // please improve your code
                    })

                this.appendH3();
                this.mapParagraphsForexcel();
    }
    createObjectForexcel() {       
        let arrExc = [...this.state.objectForExcel];           
            this.state.paragraphsKey.map((pkey) => {
                this.state.paragraphsForexcel.filter(protolkolparagraph => protolkolparagraph.docParagraphsId == pkey).map(filteredParagraphs => {
                    if (arrExc.length > 0) {
                        arrExc.map((obj) => {
                            if (obj.docParagraphsId != pkey) {
                                arrExc.push(filteredParagraphs);
                                this.setState({ objectForExcel: arrExc })
                            }
                        })
                    } else {
                        console.log("пушим параграф первый раз");
                        arrExc.push(filteredParagraphs);
                        this.setState({ objectForExcel: arrExc })
                    }
                })
            })
        }

    createreglament1ObjectForexcel = () => {
        let arrExc = [...this.state.objectForExcel];
        let newArrExc;
        this.state.reglament1IndexKey.map((reglamentkey) => {
            this.state.reglament1ParagraphsKey.map((prkey) => {
                this.state.reglament1Paragraphs.map((paragraph) => {
                    let insertPunkt = reglamentkey + '.' + prkey + '.';
                    let insertParagraph = paragraph;
                    let objectReglament = {
                        docParagraphsId: parseFloat(insertPunkt),
                        docParagraphsText: insertParagraph
                    }

                    //console.log("добавляем регламент");
                    arrExc.push(objectReglament);
                })
            })
        })
        newArrExc = arrExc.filter(function (elem) {
            if (this[elem.docParagraphsId]) return false;
            return this[elem.docParagraphsId] = true
        }, {})
        //console.log(newArrExc)/
        this.setState({ objectForExcel: newArrExc });
    }

    createreglament2ObjectForexcel = () => {
        let arrExc = [...this.state.objectForExcel];
        let newArrExc;
        this.state.reglament2IndexKey.map((reglamentkey) => {
            this.state.reglament2ParagraphsKey.map((prkey) => {
                this.state.reglament2Paragraphs.map((paragraph) => {
                    let insertPunkt = reglamentkey + '.' + prkey + '.';
                    let insertParagraph = paragraph;
                    let objectReglament = {
                        docParagraphsId: parseFloat(insertPunkt),
                        docParagraphsText: insertParagraph                    }

                    //console.log("добавляем регламент");
                    arrExc.push(objectReglament);
                })                
            })
        })
            newArrExc = arrExc.filter(function (elem) {
                    if (this[elem.docParagraphsId]) return false;
                    return this[elem.docParagraphsId] = true
                }, {})
             //console.log(newArrExc)/
        this.setState({ objectForExcel: newArrExc });       
    }

    createreglament3ObjectForexcel = () => {
        let arrExc = [...this.state.objectForExcel];
        let newArrExc;
        this.state.reglament3IndexKey.map((reglamentkey) => {
            this.state.reglament3ParagraphsKey.map((prkey) => {
                this.state.reglament3Paragraphs.map((paragraph) => {
                    let insertPunkt = reglamentkey + '.' + prkey + '.';
                    let insertParagraph = paragraph;
                    let objectReglament = {
                        docParagraphsId: parseFloat(insertPunkt),
                        docParagraphsText: insertParagraph
                    }

                    //console.log("добавляем регламент");
                    arrExc.push(objectReglament);
                })
            })
        })
        newArrExc = arrExc.filter(function (elem) {
            if (this[elem.docParagraphsId]) return false;
            return this[elem.docParagraphsId] = true
        }, {})
        //console.log(newArrExc)/
        this.setState({ objectForExcel: newArrExc });
    }

    createreglament4ObjectForexcel = () => {
        let arrExc = [...this.state.objectForExcel];
        let newArrExc;
        this.state.reglament4IndexKey.map((reglamentkey) => {
            this.state.reglament4ParagraphsKey.map((prkey) => {
                this.state.reglament4Paragraphs.map((paragraph) => {
                    let insertPunkt = reglamentkey + '.' + prkey + '.';
                    let insertParagraph = paragraph;
                    let objectReglament = {
                        docParagraphsId: parseFloat(insertPunkt),
                        docParagraphsText: insertParagraph
                    }

                    //console.log("добавляем регламент");
                    arrExc.push(objectReglament);
                })
            })
        })
        newArrExc = arrExc.filter(function (elem) {
            if (this[elem.docParagraphsId]) return false;
            return this[elem.docParagraphsId] = true
        }, {})
        //console.log(newArrExc)/
        this.setState({ objectForExcel: newArrExc });
    }

    createreglament5ObjectForexcel = () => {
        let arrExc = [...this.state.objectForExcel];
        let newArrExc;
        this.state.reglament5IndexKey.map((reglamentkey) => {
            this.state.reglament5ParagraphsKey.map((prkey) => {
                this.state.reglament5Paragraphs.map((paragraph) => {
                    let insertPunkt = reglamentkey + '.' + prkey + '.';
                    let insertParagraph = paragraph;
                    let objectReglament = {
                        docParagraphsId: parseFloat(insertPunkt),
                        docParagraphsText: insertParagraph
                    }

                    //console.log("добавляем регламент");
                    arrExc.push(objectReglament);
                })
            })
        })
        newArrExc = arrExc.filter(function (elem) {
            if (this[elem.docParagraphsId]) return false;
            return this[elem.docParagraphsId] = true
        }, {})
        //console.log(newArrExc)/
        this.setState({ objectForExcel: newArrExc });
    }

    createreglament6ObjectForexcel = () => {
        let arrExc = [...this.state.objectForExcel];
        let newArrExc;
        this.state.reglament6IndexKey.map((reglamentkey) => {
            this.state.reglament6ParagraphsKey.map((prkey) => {
                this.state.reglament6Paragraphs.map((paragraph) => {
                    let insertPunkt = reglamentkey + '.' + prkey + '.';
                    let insertParagraph = paragraph;
                    let objectReglament = {
                        docParagraphsId: parseFloat(insertPunkt),
                        docParagraphsText: insertParagraph
                    }

                    //console.log("добавляем регламент");
                    arrExc.push(objectReglament);
                })
            })
        })
        newArrExc = arrExc.filter(function (elem) {
            if (this[elem.docParagraphsId]) return false;
            return this[elem.docParagraphsId] = true
        }, {})
        //console.log(newArrExc)/
        this.setState({ objectForExcel: newArrExc });
    }
                
    createreglament7ObjectForexcel = () => {
        let arrExc = [...this.state.objectForExcel];
        let newArrExc;
        this.state.reglament7IndexKey.map((reglamentkey) => {
            this.state.reglament7ParagraphsKey.map((prkey) => {
                this.state.reglament7Paragraphs.map((paragraph) => {
                    let insertPunkt = reglamentkey + '.' + prkey + '.';
                    let insertParagraph = paragraph;
                    let objectReglament = {
                        docParagraphsId: parseFloat(insertPunkt),
                        docParagraphsText: insertParagraph
                    }

                    //console.log("добавляем регламент");
                    arrExc.push(objectReglament);
                })
            })
        })
        newArrExc = arrExc.filter(function (elem) {
            if (this[elem.docParagraphsId]) return false;
            return this[elem.docParagraphsId] = true
        }, {})
        //console.log(newArrExc)/
        this.setState({ objectForExcel: newArrExc });
    }
    
    createreglament8ObjectForexcel = () => {
        let arrExc = [...this.state.objectForExcel];
        let newArrExc;
        this.state.reglament8IndexKey.map((reglamentkey) => {
            this.state.reglament8ParagraphsKey.map((prkey) => {
                this.state.reglament8Paragraphs.map((paragraph) => {
                    let insertPunkt = reglamentkey + '.' + prkey + '.';
                    let insertParagraph = paragraph;
                    let objectReglament = {
                        docParagraphsId: parseFloat(insertPunkt),
                        docParagraphsText: insertParagraph
                    }

                    //console.log("добавляем регламент");
                    arrExc.push(objectReglament);
                })
            })
        })
        newArrExc = arrExc.filter(function (elem) {
            if (this[elem.docParagraphsId]) return false;
            return this[elem.docParagraphsId] = true
        }, {})
        //console.log(newArrExc)/
        this.setState({ objectForExcel: newArrExc });
    }
    



    addEditedCellToObjectForexcel=(index, cell) =>{
        this.setState(prevState => ({
              objectForExcel: prevState.objectForExcel.map(
                  obj => (
                      obj.docParagraphsId == index ? Object.assign(obj, { editedCellResult: cell}) : obj)
                   )
               })
        )
    }
  
    updateParagraph(newText, index) {
        const { paragraph } = this.state;
        const newParagraph = paragraph;
        newParagraph[index] = newText;
        this.setState({ paragraph: newParagraph });
    }
    onFocus() {
        this.setState({ editing: true }, () => {
            this.refs.input.focus();
        });
    }

    onBlur() {
        this.setState({ editing: false });

    }
    naimenovanieDokumentaOpen=(e) =>{
        if (e.target.id == "naimenovanie1") {
            this.setState({ reglament1Unfold: false });
        }
        else if (e.target.id == "naimenovanie2") {
            this.setState({ reglament2Unfold: false });
        }
        else if (e.target.id == "naimenovanie3") {
            this.setState({ reglament3Unfold: false });
        }
        else if (e.target.id == "naimenovanie4") {
            this.setState({ reglament4Unfold: false });
        }
        else if (e.target.id == "naimenovanie5") {
            this.setState({ reglament5Unfold: false });
        }
        else if (e.target.id == "naimenovanie6") {
            this.setState({ reglament6Unfold: false });
        }
        else if (e.target.id == "naimenovanie7") {
            this.setState({ reglament7Unfold: false });
        }
        else if (e.target.id == "naimenovanie8") {
            this.setState({ reglament8Unfold: false });
        }
        else if (e.target.id == "naimenovanie9") {
            this.setState({ reglament9Unfold: false });
        }
        else if (e.target.id == "naimenovanie10") {
            this.setState({ price1Unfold: false });
        }
        else if (e.target.id == "naimenovanie11") {
            this.setState({ price2Unfold: false });
        }
        else if (e.target.id == "naimenovanie12") {
            this.setState({ price3Unfold: false });
        }
        else if (e.target.id == "naimenovanie14") {
            this.setState({ price5Unfold: false });
        }
        else if (e.target.id == "naimenovanie15") {
            this.setState({ price6Unfold: false });
        }
        else if (e.target.id == "naimenovanie16") {
            this.setState({ price7Unfold: false });
        }
        else if (e.target.id == "naimenovanie17") {
            this.setState({ price8Unfold: false });
        }
        else if (e.target.id == "naimenovanie19") {
            this.setState({ price10Unfold: false });
        }
        //console.log(e.target.id); // button element
        
    }
    naimenovanieDokumentaClose=(e) =>{
        if (e.target.id == "naimenovanie1") {
            this.setState({ reglament1Unfold: true });
            this.setState({ indexForProtokolReglament: 0 });            
        }
        else if (e.target.id == "naimenovanie2") {
            this.setState({ reglament2Unfold: true });
            this.setState({ indexForProtokolReglament: 1 });
        }
        else if (e.target.id == "naimenovanie3") {
            this.setState({ reglament3Unfold: true });
            this.setState({ indexForProtokolReglament: 2 });
        }
        else if (e.target.id == "naimenovanie4") {
            this.setState({ reglament4Unfold: true });
            this.setState({ indexForProtokolReglament: 3 });
        }
        else if (e.target.id == "naimenovanie5") {
            this.setState({ reglament5Unfold: true });
            this.setState({ indexForProtokolReglament: 4 });
        }
        else if (e.target.id == "naimenovanie6") {
            this.setState({ reglament6Unfold: true });
            this.setState({ indexForProtokolReglament: 5 });
        }
        else if (e.target.id == "naimenovanie7") {
            this.setState({ reglament7Unfold: true });
            this.setState({ indexForProtokolReglament: 6 });
        }
        else if (e.target.id == "naimenovanie8") {
            this.setState({ reglament8Unfold: true });
            this.setState({ indexForProtokolReglament: 7 });
        }
        else if (e.target.id == "naimenovanie9") {
            this.setState({ reglament9Unfold: true });
            this.setState({ indexForProtokolReglament: 8 });
        }
        else if (e.target.id == "naimenovanie10") {
            this.setState({ price1Unfold: true });
            this.setState({ indexForProtokolReglament: 9 });
        }
        else if (e.target.id == "naimenovanie11") {
            this.setState({ price2Unfold: true });
            this.setState({ indexForProtokolReglament: 10 });
        }
        else if (e.target.id == "naimenovanie12") {
            this.setState({ price3Unfold: true });
            this.setState({ indexForProtokolReglament: 11 });
        }
        else if (e.target.id == "naimenovanie14") {
            this.setState({ price5Unfold: true });
            this.setState({ indexForProtokolReglament: 13 });
        }
        else if (e.target.id == "naimenovanie15") {
            this.setState({ price6Unfold: true });
            this.setState({ indexForProtokolReglament: 14 });
        }
        else if (e.target.id == "naimenovanie16") {
            this.setState({ price7Unfold: true });
            this.setState({ indexForProtokolReglament: 15 });
        }
        else if (e.target.id == "naimenovanie17") {
            this.setState({ price8Unfold: true });
            this.setState({ indexForProtokolReglament: 16 });
        }
        else if (e.target.id == "naimenovanie19") {
            this.setState({ price10Unfold: true });
            this.setState({ indexForProtokolReglament: 17 });
        }
        //console.log(e.target.id); // button element
        
    }
    checkIndexForProtokolReglament() {
        if (this.state.indexForProtokolReglament == 0) {
            //alert("рагламент")
            this.state.reglament1IndexKey.push(this.state.indexForProtokolReglament + 1)
        } else if (this.state.indexForProtokolReglament == 1) {
            this.state.reglament2IndexKey.push(this.state.indexForProtokolReglament + 1)
        } else if (this.state.indexForProtokolReglament == 2) {
            this.state.reglament3IndexKey.push(this.state.indexForProtokolReglament + 1)
        } else if (this.state.indexForProtokolReglament == 3) {
            this.state.reglament4IndexKey.push(this.state.indexForProtokolReglament + 1)
        } else if (this.state.indexForProtokolReglament == 4) {
            this.state.reglament5IndexKey.push(this.state.indexForProtokolReglament + 1)
        } else if (this.state.indexForProtokolReglament == 5) {
            this.state.reglament6IndexKey.push(this.state.indexForProtokolReglament + 1)
        } else if (this.state.indexForProtokolReglament == 6) {
            this.state.reglament7IndexKey.push(this.state.indexForProtokolReglament + 1)
        } else if (this.state.indexForProtokolReglament == 7) {
            this.state.reglament8IndexKey.push(this.state.indexForProtokolReglament + 1)
        } else if (this.state.indexForProtokolReglament == 8) {
            this.state.reglament9IndexKey.push(this.state.indexForProtokolReglament + 1)
        } else if (this.state.indexForProtokolReglament == 9) {
            this.state.price1IndexKey.push(this.state.indexForProtokolReglament + 1)
        } else if (this.state.indexForProtokolReglament == 9) {
            this.state.price1IndexKey.push(this.state.indexForProtokolReglament + 1)
        } else if (this.state.indexForProtokolReglament == 10) {
            this.state.price2IndexKey.push(this.state.indexForProtokolReglament + 1)
        } else if (this.state.indexForProtokolReglament == 11) {
            this.state.price3IndexKey.push(this.state.indexForProtokolReglament + 1)
        } else if (this.state.indexForProtokolReglament == 13) {
            this.state.price5IndexKey.push(this.state.indexForProtokolReglament + 1)
        } else if (this.state.indexForProtokolReglament == 14) {
            this.state.price6IndexKey.push(this.state.indexForProtokolReglament + 1)
        } else if (this.state.indexForProtokolReglament == 15) {
            this.state.price7IndexKey.push(this.state.indexForProtokolReglament + 1)
        } else if (this.state.indexForProtokolReglament == 16) {
            this.state.price8IndexKey.push(this.state.indexForProtokolReglament + 1)
        } else if (this.state.indexForProtokolReglament == 17) {
            this.state.price10IndexKey.push(this.state.indexForProtokolReglament + 2)
        }
    }
    //setNewLegalStatus=(e) =>{
    //    this.setState({
    //        legalstatus: this.state.legalStatusValue
    //    });
    //    this._input.focus();
    //    this._input.value = "";
    //    e.preventDefault();
    //}
    legalStatusValue=(e)=> {
        this.setState({
            legalStatus: e.target.value
        })
    }
    organizationNameValue = (e) => {
        this.setState({
            organizationName: e.target.value
        },
            this.bottomFieldForOrganization
        )        
    }
    bottomFieldForOrganization() {
        let org = this.state.legalStatus + this.state.organizationName;
        let result = org.replace(/^(.{3})(.*)$/, "$1 $2");//вставить пробел после 3 символа.
        this.setState({ downOrganizationName: result })
    }
    doljnostValue = (e) => {
        let name = e.target.value;
        let result = name.charAt(0).toUpperCase() + name.slice(1);
       
       
        this.setState({
            doljnost: result
        })
    }
   
    dokumentnameValue = (e) => {
        this.setState({
            dokumentname: e.target.value
        })
    }
    adressValue = (e) => {
        this.setState({
            adress: e.target.value
        })
    }

    validateInn(inn) {
        let el = document.getElementById('isinn');
    var result = false;
    if (typeof inn === 'number') {
        inn = inn.toString();
    } else if (typeof inn !== 'string') {
        inn = '';
    }
    if (!inn.length) {
        //error.code = 1;
        //error.message = 'ИНН пуст';
        el.textContent ='ИНН пуст';
    } else if (/[^0-9]/.test(inn)) {
        //error.code = 2;
        //error.message = 'ИНН может состоять только из цифр';
        el.textContent = 'ИНН может состоять только из цифр';
    } else if ([10, 12].indexOf(inn.length) === -1) {
        //error.code = 3;
        //error.message = 'ИНН может состоять только из 10 или 12 цифр';
        el.textContent = 'ИНН может состоять только из 10 или 12 цифр';
    } else {
        var checkDigit = function (inn, coefficients) {
            var n = 0;
            for (var i in coefficients) {
                n += coefficients[i] * inn[i];
            }
            return parseInt(n % 11 % 10);
        };
        switch (inn.length) {
            case 10:
                var n10 = checkDigit(inn, [2, 4, 10, 3, 5, 9, 4, 6, 8]);
                if (n10 === parseInt(inn[9])) {
                    result = true;
                }
                break;
            case 12:
                var n11 = checkDigit(inn, [7, 2, 4, 10, 3, 5, 9, 4, 6, 8]);
                var n12 = checkDigit(inn, [3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8]);
                if ((n11 === parseInt(inn[10])) && (n12 === parseInt(inn[11]))) {
                    result = true;
                }
                break;
        }
        if (!result) {
            //error.code = 4;
            //error.message = 'Неправильное контрольное число';
            el.textContent = 'Неправильное контрольное число';
        }
    }
    return result;
}
    innValue = (e) => {
        let val = e.target.value;
        let correctInn = this.validateInn(val);
        if (correctInn == true) {
            this.setState({
                inn: e.target.value
            },
                function () {
                    console.log(correctInn);
                })
            this.setState({
                isinn: true
            })

        } else {
            this.setState({
                isinn: false
            })
        }  
    }

    setInn=(value)=>{
        this.setState({
            inn: value
        })
    }

    validateKPP(kpp) {
        let el = document.getElementById('iskpp');
        var result = false;
        if (typeof kpp === 'number') {
            kpp = kpp.toString();
        } else if (typeof kpp !== 'string') {
            kpp = '';
        }
        if (!kpp.length) {            
            console.log('КПП пуст')
            el.textContent = 'КПП пуст';
        } else if (kpp.length !== 9) {            
            console.log('КПП может состоять только из 9 знаков (цифр или заглавных букв латинского алфавита от A до Z)');
            el.textContent='КПП может состоять только из 9 знаков (цифр или заглавных букв латинского алфавита от A до Z)';
        } else if (!/^[0-9]{4}[0-9A-Z]{2}[0-9]{3}$/.test(kpp)) {            
            console.log('Неправильный формат КПП')
            el.textContent = 'Неправильный формат КПП';
        } else {
            result = true;       
        }
        return result;
    }
    kppValue = (e) => {
        let val = e.target.value;
        let correctokpo = this.validateKPP(val);
        if (correctokpo == true) {
            this.setState({
                kpp: e.target.value
            },
                function () {
                    console.log(correctokpo);
                })
            this.setState({
                iskpp: true
            })
        } else {
            this.setState({
                iskpp: false
            });
        }
     
    }

    calculateControlSummOKPO=(value)=>{       
        const expectedValue = Number(value.slice(-1))
        const getWeight = (index)=> {
            if (index < 10) return index + 1
            else return index % 10 + 1
        }
        const testingValue = value.slice(0, -1)
        let summ = 0
        for (const i in testingValue.split('')) {
            summ += Number(testingValue[i]) * getWeight(Number(i))
        }
        let del11 = summ % 11
        if (del11 === 10) {
            summ = 0
            for (const i in testingValue.split('')) {
                summ += Number(testingValue[i]) * (getWeight(Number(i)) + 2)
            }
            del11 = (del11 === 10) ? 0 : del11
        }
        return (del11 === expectedValue)
    }
    validateOKPO(okpo) {
        let el = document.getElementById('isokpo');        
        let result = false;
        if (typeof okpo === 'number') {
            okpo = okpo.toString();
        } else if (typeof okpo !== 'string') {
            okpo = '';
        }
        if (!okpo.length) {           
            el.textContent = 'ОКПО пуст';
        } else if (okpo.length !== 8) {          
            el.textContent = 'ОКПО может состоять только из 8 знаков (цифр или заглавных букв латинского алфавита от A до Z)';
        } else if (!this.calculateControlSummOKPO(okpo)) {
            el.textContent = 'Неправильный формат ОКПО';
        } else {
            result = true;
        }
        return result;
    }
    okpoValue = (e) => {
        let val = e.target.value;
        let correctokpo = this.validateOKPO(val);
        if (correctokpo == true) {
            this.setState({
                okpo: e.target.value
            },
                function () {
                    console.log(correctokpo);
                })
            this.setState({
                isokpo: true
            })
        } else {
            this.setState({
                isokpo: false
            })
        }
    }

    validateRs(rs, bik) {
        let el = document.getElementById('isrc');
        var result = false;
        if (this.validateBik(bik)) {
            if (typeof rs === 'number') {
                rs = rs.toString();
            } else if (typeof rs !== 'string') {
                rs = '';
            }
            if (!rs.length) {                
                el.textContent = 'Р/С пуст';
            } else if (/[^0-9]/.test(rs)) {                
                el.textContent = 'Р/С может состоять только из цифр';
            } else if (rs.length !== 20) {                
                el.textContent = 'Р/С может состоять только из 20 цифр';
            } else {
                var bikRs = bik.toString().slice(-3) + rs;
                var checksum = 0;
                var coefficients = [7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1];
                for (var i in coefficients) {
                    checksum += coefficients[i] * (bikRs[i] % 10);
                }
                if (checksum % 10 === 0) {
                    result = true;
                } else {                   
                    el.textContent = 'Неправильное контрольное число';
                }
            }
        }
        return result;
    }
    rekvizity_rcValue = (e) => {
        let el = document.getElementById('isrc');
        let correctrc = null,
            val = e.target.value,
            thisbik = this.state.rekvizity_bik;            
        if (thisbik != null) {            
            correctrc = this.validateRs(val, thisbik);
            if (correctrc == true) {
                this.setState({
                    rekvizity_rc: e.target.value
                },
                    function () {
                        console.log(correctrc);
                    })
                this.setState({
                    isrc: true
                })
            } else {
                this.setState({
                    isrc: false
                })
            }
        } else {
            el.textContent = "не заполнен БИК";            
        }        
    }

    rekvizity_bankValue = (e) => {
        this.setState({
            rekvizity_bank: e.target.value
        })
    }

    validateKs(ks, bik) {
    let el = document.getElementById('iskc');
    var result = false;
    if (this.validateBik(bik)) {
        if (typeof ks === 'number') {
            ks = ks.toString();
        } else if (typeof ks !== 'string') {
            ks = '';
        }
        if (!ks.length) {           
            el.textContent = 'К/С пуст';
        } else if (/[^0-9]/.test(ks)) {            
            el.textContent = 'К/С может состоять только из цифр';
        } else if (ks.length !== 20) {            
            el.textContent = 'К/С может состоять только из 20 цифр';
        } else {
            var bikKs = '0' + bik.toString().slice(4, 6) + ks;
            var checksum = 0;
            var coefficients = [7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1];
            for (var i in coefficients) {
                checksum += coefficients[i] * (bikKs[i] % 10);
            }
            if (checksum % 10 === 0) {
                result = true;
            } else {                
                el.textContent = 'Неправильное контрольное число';
            }
        }
    }
    return result;
}

    rekvizity_kcValue = (e) => {
        let val = e.target.value,
        thisbik = this.state.rekvizity_bik;
        let correctkc = this.validateKs(val, thisbik);
        if (correctkc == true) {
            this.setState({
                rekvizity_kc: e.target.value
            },
                function () {
                    console.log(correctkc);
                })
            this.setState({
                iskc: true
            })
        } else {
            this.setState({
                iskc: false
            })
        }
    }

    validateBik(bik) {
        let el = document.getElementById('isbik');
        var result = false;
        if (typeof bik === 'number') {
            bik = bik.toString();
        } else if (typeof bik !== 'string') {
            bik = '';
        }
        if (!bik.length) {           
            el.textContent = 'БИК пуст';
        } else if (/[^0-9]/.test(bik)) {
            el.textContent = 'БИК может состоять только из цифр';
        } else if (bik.length !== 9) {
            el.textContent = 'БИК может состоять только из 9 цифр';
        } else {
            result = true;
        }
        return result;
    }

    rekvizity_bikValue = (e) => {
        let val = e.target.value;
        let correctbik = this.validateBik(val);
        if (correctbik == true) {
            this.setState({
                rekvizity_bik: e.target.value
            },
                function () {
                    console.log(correctbik);
                })
            this.setState({
                isbik: true
            })
        } else {
            this.setState({
                isbik: false
            })
        }
    }


    validateTELEPHONE(telephone) {
        let el = document.getElementById('istelephone');
        var result = false;
        if (typeof telephone === 'number') {
            telephone = telephone.toString();
        } else if (typeof telephone !== 'string') {
            telephone = '';
        }
        if (!telephone.length) {
            console.log('телефон пуст')
            el.textContent = 'телефон пуст';
        } else if (!/^[\d]{1}\ \([\d]{2,3}\)\ [\d]{2,3}-[\d]{2,3}-[\d]{2,3}$/.test(telephone)) {
            el.textContent = 'Номер телефона введен неправильно!';
        } else {
            result = true;
        }
        return result;
    }
    phoneValue = (e) => {
        let val = e.target.value,
            correcttelephone = this.validateTELEPHONE(val);
        if (correcttelephone == true) {
            this.setState({
                phone: e.target.value
            },
                function () {
                    console.log(correcttelephone);
                })
            this.setState({
                istelephone: true
            })
        } else {
            this.setState({
                istelephone: false
            })
        }
    }

    validateTELEPHONEFORSMS(telephoneSMS) {
        let el = document.getElementById('istelephoneSMS');
        var result = false;
        if (typeof telephoneSMS === 'number') {
            telephoneSMS = telephoneSMS.toString();
        } else if (typeof telephoneSMS !== 'string') {
            telephoneSMS = '';
        }
        if (!telephoneSMS.length) {
            console.log('телефон пуст')
            el.textContent = 'телефон пуст';
        } else if (!/^[\d]{1}\ \([\d]{2,3}\)\ [\d]{2,3}-[\d]{2,3}-[\d]{2,3}$/.test(telephoneSMS)) {
            el.textContent = 'Номер телефона введен неправильно!';
        } else {
            result = true;
        }
        return result;
    }
    phoneforsmsValue = (e) => {
        let val = e.target.value,
            correcttelephoneSMS = this.validateTELEPHONEFORSMS(val);
        if (correcttelephoneSMS == true) {
            this.setState({
                phoneforsms: e.target.value
            }, function () {
                console.log(correcttelephoneSMS);
            })
            this.setState({
                istelephoneSMS: true
            })
        } else {
            this.setState({
                istelephoneSMS: false
            })
        }
    }

    validateEMAIL(email) {
        let el = document.getElementById('isemail');
        var result = false;
        if (typeof email === 'number') {
            email = email.toString();
        } else if (typeof email !== 'string') {
            email = '';
        }
        if (!email.length) {
            console.log('email пуст')
            el.textContent = 'email пуст';
        } else if (!/^[\w]{1}[\w-\.]*@[\w-]+\.[a-z]{2,4}$/i.test(email)) {
            el.textContent = 'Адрес электронной почты введен неправильно!';
        } else {
            result = true;
        }
        return result;
    }
    emailValue = (e) => {
        let val = e.target.value,            
            correctemail = this.validateEMAIL(val);
        if (correctemail == true) {
            this.setState({
                email: e.target.value
            },
                function () {
                    console.log(correctemail);
                })
            this.setState({
                isemail: true
            })
        } else {
            this.setState({
                isemail: false
            })
        }
    }
    
    getExcelFile = (value) => {
        this.setState({ href: value })
            //function () {
            //    //e.preventDefault();
            //    var blobUrl = this.state.href
            //    var xhr = new XMLHttpRequest();
            //    console.log(blobUrl)
            //    xhr.open('GET', blobUrl, true);
            //    xhr.responseType = 'blob';
            //    xhr.onload = function (e) {
            //        if (this.status == 200) {
            //            var myBlob = this.response;
            //            console.log(JSON.stringify(myBlob));
            //            // myBlob is now the blob that the object URL pointed to.
            //        }
            //    };
            //    xhr.send();
             
   
    }
    
    launchPreloader = () => {
        //console.log("Запустили")
        this.setState({ goloader: false })
    }
    stopPreloader = () => {
        //console.log("Остановили")
        this.setState({ goloader: true })
    }
    popupUzhe_zaregestrirovanShow=()=>{
        this.setState({ uzhe_zaregestrirovan: false })
    }
    getNameUzhe_zaregestrirovanogo = (value) => {
        this.setState({ uzhe_zaregestrirovan_inn: value })
    }
    //self.setState({ uzhe_zaregestrirovan_inn: jsonUpdatedData })
    popupUzhe_zaregestrirovanHide = () => {
        this.setState({ uzhe_zaregestrirovan: true })
    }
    render() {
        
        const {
            companies,
            staff,
            paragraphs,
            joining,
            joiningReglament,
            joiningPrice,
            showPopup,
            showPopupCompanyDetailed,
            showReglamentPopup,
            showProtocol,
            hideFormJoin,
            contractClient,
            userLastName,

            reglament1ParagraphsKey,
            reglament2ParagraphsKey,
            reglament3ParagraphsKey,
            reglament4ParagraphsKey,
            reglament5ParagraphsKey,
            reglament6ParagraphsKey,
            reglament7ParagraphsKey,
            reglament8ParagraphsKey,
            reglament9ParagraphsKey,

            price1ParagraphsKey,
            price2ParagraphsKey,
            price3ParagraphsKey,

            price5ParagraphsKey,
            price6ParagraphsKey,
            price7ParagraphsKey,
            price8ParagraphsKey,
            price10ParagraphsKey,

            reglament1Paragraphs,
            reglament2Paragraphs,
            reglament3Paragraphs,
            reglament4Paragraphs,
            reglament5Paragraphs,
            reglament6Paragraphs,
            reglament7Paragraphs,
            reglament8Paragraphs,
            reglament9Paragraphs,

            price1Paragraphs,
            price2Paragraphs,
            price3Paragraphs,
            
            price5Paragraphs,
            price6Paragraphs,
            price7Paragraphs,
            price8Paragraphs,
            price10Paragraphs,

            reglament1IndexKey,
            reglament2IndexKey,
            reglament3IndexKey,
            reglament4IndexKey,
            reglament5IndexKey,
            reglament6IndexKey,
            reglament7IndexKey,
            reglament8IndexKey,
            reglament9IndexKey,

            price1IndexKey,
            price2IndexKey,
            price3IndexKey,
            price5IndexKey,
            price6IndexKey,
            price7IndexKey,
            price8IndexKey,
            price10IndexKey,

            paragraphsKey,
            reglamentParagraphsIndex,
            
            prilozhenie2key,
            prilozhenie3key,
            showAddPoint,
            prilozhenie1,
            prilozhenie2,
            prilozhenie3,
            prilozhenie4,
            prilozhenie5,
            prilozhenie6,
            prilozhenie7,
            prilozhenie8,
            prilozhenie9,

            price1,
            price2,
            price3,
            price5,
            price6,
            price7,
            price8,
            price10,
            

            showPrilozhenie2,
            showPrilozhenie3,
            indexForProtokolReglament,

            reglament1Unfold,
            reglament2Unfold,
            reglament3Unfold,
            reglament4Unfold,
            reglament5Unfold,
            reglament6Unfold,
            reglament7Unfold,
            reglament8Unfold,
            reglament9Unfold,


            price1Unfold,
            price2Unfold,
            price3Unfold,
            price5Unfold,
            price6Unfold,
            price7Unfold,
            price8Unfold,
            price10Unfold,
            getContactorsSpisokPprisoedinenija,
            arrReglamentHref,
            /*abbreviated, actualcontract, address, autodelivery, badpayer, carriageservice1c , 
                            checkttnmail, color, containerservice1c, country, debet, diadoc, emails, fnsparticipantid, foralterinter, 
                            freightcharge, freightchargeservice, fullname, group, id, inn, istrusted, isunreliable, kod1c, kpp, 
                            ktpriceversion, loadedgateinconstraint, maxdebt, name, nightgateout, nightgateoutreserve, nonds, notifycontainersemails, 
                            notifyemails, okpo, operationsdenial, ownerdisposerplatform, pastetogethernumber, payfreightcharge, phones, postpay,
                            priceversion, repairchargetype,  repaircontract, sbemptyinbound, sbemptyoutput, sbfullinbound, sbfulloutput, separateatbids,
                            separateatbidsreceivers, transporter, useaddressfom1c,*/
            legalStatus, downOrganizationName, organizationName,  doljnost, dokumentname, adress,  inn,  kpp,  okpo,  rekvizity_rc,  rekvizity_bank,  rekvizity_kc, rekvizity_bik, phone, phoneforsms,  email,
            paragraphsForexcel,
            objectForExcel,
            isinn, iskpp, isokpo, isbik, isrc, iskc, istelephone, istelephoneSMS, isemail,
            goloader,
            uzhe_zaregestrirovan,
            uzhe_zaregestrirovan_inn
            

        } = this.state;
        var self = this;
        return (
           
            <div >
                {/*<div className="App-preloader" style={{ display: (goloader) ? 'none' : 'block' }}>*/}
                {/*    <img src={preloader}  alt="Загрузка..."  />*/}
                {/*</div>*/}
                <div className="App-preloader" style={{ display: (goloader) ? 'none' : 'block' }}>
                    <div className="preloader" >
                        <div className="item item-1"></div>
                        <div className="item item-2"></div>
                        <div className="item item-3"></div>
                        <div className="item item-4"></div>
                    </div>                    
                </div>
                <div className="Uzhe_zaregestrirovan_popup" style={{ display: (uzhe_zaregestrirovan) ? 'none' : 'block' }}>
                    <div>Контрагент <span className="Uzhe_zaregestrirovan_name">{this.state.uzhe_zaregestrirovan_inn}</span> с таким ИНН уже зарегистрирован!</div>
                    <button className="Uzhe_zaregestrirovan_popup_close" onClick={() => {
                        this.popupUzhe_zaregestrirovanHide();
                    }}>Закрыть</button>
                </div>

                <div className="content_contract">
                    <Protocol showProtocol={showProtocol}
                        closeProtocol={this.closeProtocol}
                        paragraphs={paragraphs}
                        staff={staff}
                        paragraphsForexcel={paragraphsForexcel}
                        prilozhenie1={prilozhenie1}
                        prilozhenie2={prilozhenie2}
                        prilozhenie3={prilozhenie3}
                        prilozhenie4={prilozhenie4}
                        prilozhenie5={prilozhenie5}
                        prilozhenie6={prilozhenie6}
                        prilozhenie7={prilozhenie7}
                        prilozhenie8={prilozhenie8}
                        prilozhenie9={prilozhenie9}
                        indexForProtokolReglament={indexForProtokolReglament}
                        paragraphsKey={paragraphsKey}

                        reglament1ParagraphsKey={reglament1ParagraphsKey}
                        reglament2ParagraphsKey={reglament2ParagraphsKey}
                        reglament3ParagraphsKey={reglament3ParagraphsKey}
                        reglament4ParagraphsKey={reglament4ParagraphsKey}
                        reglament5ParagraphsKey={reglament5ParagraphsKey}
                        reglament6ParagraphsKey={reglament6ParagraphsKey}
                        reglament7ParagraphsKey={reglament7ParagraphsKey}
                        reglament8ParagraphsKey={reglament8ParagraphsKey}
                        reglament9ParagraphsKey={reglament9ParagraphsKey}

                        price1IndexKey={price1IndexKey}
                        price2IndexKey={price2IndexKey}
                        price3IndexKey={price3IndexKey}
                        price5IndexKey={price5IndexKey}
                        price6IndexKey={price6IndexKey}
                        price7IndexKey={price7IndexKey}
                        price8IndexKey={price8IndexKey}
                        price10IndexKey={price10IndexKey}

                        price1ParagraphsKey={price1ParagraphsKey}
                        price2ParagraphsKey={price2ParagraphsKey}
                        price3ParagraphsKey={price3ParagraphsKey}
                        
                        price5ParagraphsKey={price5ParagraphsKey}
                        price6ParagraphsKey={price6ParagraphsKey}
                        price7ParagraphsKey={price7ParagraphsKey}
                        price8ParagraphsKey={price8ParagraphsKey}
                        price10ParagraphsKey={price10ParagraphsKey}

                        reglamentParagraphsIndex={reglamentParagraphsIndex}



                        showPrilozhenie2={showPrilozhenie2}


                        reglament1IndexKey={reglament1IndexKey}
                        reglament2IndexKey={reglament2IndexKey}
                        reglament3IndexKey={reglament3IndexKey}
                        reglament4IndexKey={reglament4IndexKey}
                        reglament5IndexKey={reglament5IndexKey}
                        reglament6IndexKey={reglament6IndexKey}
                        reglament7IndexKey={reglament7IndexKey}
                        reglament8IndexKey={reglament8IndexKey}
                        reglament9IndexKey={reglament9IndexKey}

                        reglament1Paragraphs={reglament1Paragraphs}
                        reglament2Paragraphs={reglament2Paragraphs}
                        reglament3Paragraphs={reglament3Paragraphs}
                        reglament4Paragraphs={reglament4Paragraphs}
                        reglament5Paragraphs={reglament5Paragraphs}
                        reglament6Paragraphs={reglament6Paragraphs}
                        reglament7Paragraphs={reglament7Paragraphs}
                        reglament8Paragraphs={reglament8Paragraphs}
                        reglament9Paragraphs={reglament9Paragraphs}

                        price1Paragraphs={price1Paragraphs}
                        price2Paragraphs={price2Paragraphs}
                        price3Paragraphs={price3Paragraphs}
                        
                        price5Paragraphs={price5Paragraphs}
                        price6Paragraphs={price6Paragraphs}
                        price7Paragraphs={price7Paragraphs}
                        price8Paragraphs={price8Paragraphs}
                        price10Paragraphs={price10Paragraphs}

                        objectForExcel={objectForExcel}
                        addEditedCellToObjectForexcel={this.addEditedCellToObjectForexcel}
                    />
                    <form id="prisoedinenie" className="form_for_contract" >
                        <div className="doc_units_name" style={{ display: (hideFormJoin) ? 'none' : 'flex' }}>
                                <h3>ДОГОВОР №</h3>
                                <div><input type="text" name="numberofcontract" /></div>
                            <div><input type="text" name="dateofconclusion" /></div>
                            <h5 className="ob_okazanii_uslug"><strong>Об оказании услуг</strong></h5>
                         </div>
                            
                        <div className="doc_units_head" style={{ display: (hideFormJoin) ? 'none' : 'block' }}>
                                <ol className="dogovor_head">
                                    <li><span className="AOSiAjTiTerminal"><strong> АО </strong></span>, именуемое в дальнейшем «Исполнитель», в лице управляющего Иванова Ивана Ивановича, действующего на </li>
                                <li>основании <span className="doverennosti">доверенности</span>, с одной стороны, и <input type="text" size="30" placeholder="правовая форма" onChange={this.legalStatusValue} ref={function (el) {self._input = el; }}
                                    name="legalstatus" /><input type="text" size="30" placeholder="название организации" name="organizationname" ref={function (el) {self._input = el; }} onChange={this.organizationNameValue}/>
                                </li>
                                    
                                <li>именуемое в дальнейшем <strong>«Заказчик»</strong> в лице <input type="text" size="22" placeholder="должность" name="doljnost" ref={function (el) { self._input = el;}} onChange={this.doljnostValue} />
                                    <span>{userLastName}</span></li>
                                <li>действующего на основании<input type="text" size="50" ref={function (el) { self._input = el; }} onChange={this.dokumentnameValue} placeholder="наименование, № документа" name="dokumentname" />с другой стороны, совместно именуемые в дальнейшем «Стороны»,</li>
                                    <li>заключили настоящий договор о нижеследующем:</li>  
                                </ol>
                            </div>
                        <Popup showPopup={showPopup} closePopup={this.closePopup} openProtocolhideFormJoin={this.openProtocolhideFormJoin} showPrilozhenie2={showPrilozhenie2} paragraphsForexcel={paragraphsForexcel} paragraphsKey={paragraphsKey}/>
                        <PopupCompanyDetailed companies={companies} closePopup={this.closePopup} showPopupCompanyDetailed={showPopupCompanyDetailed} paragraphsForexcel={paragraphsForexcel}  paragraphsKey={paragraphsKey}/>
                        <AddPointfromDogovor showAddPoint={showAddPoint} closeAddPoint={this.closeAddPoint} openProtocolhideFormJoin={this.openProtocolhideFormJoin} />
                        <AddPointfromReglament showReglamentPopup={showReglamentPopup} closePopup={this.closePopup} openProtocolhideFormReglament={this.openProtocolhideFormReglament} closePopupAddPointfromReglament={this.closePopupAddPointfromReglament} showPrilozhenie2={showPrilozhenie2} indexForProtokolReglament={indexForProtokolReglament} checkIndexForProtokolReglament={this.checkIndexForProtokolReglament}/>
                       
                        <div className="doc_paragraphs" >
                            <div className="spisok_head" style={{ display: (hideFormJoin) ? 'none' : 'flex' }}>
                                <div className="num_prilozhenija_k_dogovoru">№ п/п</div>
                                <div className="prisoedinenie_k_uslovijam_head">Краткое наименование</div>
                                <div className="naimenovanie_dokumenta_head upper_head">Телефон</div>
                                <div className="cancel_join_head">Email</div>
                            </div>
                                    <ol className="doc_paragraphs_name" style={{ display: (hideFormJoin) ? 'none' : 'block' }}>
                                                {                                    
                                                    companies.map((paragraph, index) => {
                                                        return <DocParagraphs index={index} paragraph={paragraph} openPopup={this.openPopup} showPopupCompanyDetailed={showPopupCompanyDetailed} openPopupCompanyDetailed={this.openPopupCompanyDetailed} openAddPoint={this.openAddPoint} paragraphsKey={paragraphsKey} />
                                                    })                               
                                                }
                                    </ol>
                                    {/*<CSVDownload data={paragraphsForexcel} target="_blank" />*/}
                            <CSVLink
                                id="dogovorToExcel"
                                className="toExcel"
                                filename={"dogovor.csv"}
                                data={paragraphsForexcel}
                                
                                onClick={event => {
                                    console.log("You click the link");
                                     // 👍🏻 You are stopping the handling of component
                                }}
                                >Скачать договор в формате csv(файл Excel)</CSVLink>
                                
                                    <ol className="spisok_zagolovok"><li><h3>СПИСОК ПРИСОЕДИНЕНИЯ:</h3></li></ol>
                                    <div className="spisok_hea">
                                        <div className="num_prilozhenija_k_dogovoru">№</div>
                                        <div className="prisoedinenie_k_uslovijam_head">Присоединение   к условиям</div>
                                        <div className="naimenovanie_dokumenta_head upper_head">Наименование документа (приложения к действующему Договору)</div>
                                        <div className="cancel_join_head">Отменить</div>
                                    </div>                            
                                    <ol className="spisok_prisoedinenija">
                                        {
                                            joiningReglament.map((join, index) => {
                                                        if (index == 0) {
                                                            return (
                                                                <li key={"li_1_" + index}>
                                                                    <JoiningReglament key={"R_0_" + index} apiUrl={["/api/contactorsSpisok"]} join={join} showPrilozhenie2={showPrilozhenie2} showPrilozhenie2={showPrilozhenie2} openPrilozhenie={this.openPrilozhenie} prilozhenie1={prilozhenie1} prilozhenie2={prilozhenie2} prilozhenie3={prilozhenie3} prilozhenie4={prilozhenie4} prilozhenie5={prilozhenie5} prilozhenie6={prilozhenie6} prilozhenie7={prilozhenie7} prilozhenie8={prilozhenie8} prilozhenie9={prilozhenie9} naimenovanieDokumentaOpen={this.naimenovanieDokumentaOpen} naimenovanieDokumentaClose={this.naimenovanieDokumentaClose} contractClient={contractClient} arrReglamentHref={arrReglamentHref} />
                                                                    <Prilozhenie1 key={"PR_0_" + index} prilozhenie1={prilozhenie1} reglament1Unfold={reglament1Unfold} openPopupAddPointfromReglament={this.openPopupAddPointfromReglament} reglament1Paragraphs={reglament1Paragraphs} />
                                                                </li>
                                                            )
                                                        }
                                                        if (index == 1) {
                                                            return (
                                                                <li key={"li_2_" + index}>
                                                                    <JoiningReglament key={"R_1_" + index} apiUrl={["/api/contactorsSpisok"]} join={join} showPrilozhenie2={showPrilozhenie2} showPrilozhenie2={showPrilozhenie2} openPrilozhenie={this.openPrilozhenie} prilozhenie1={prilozhenie1} prilozhenie2={prilozhenie2} prilozhenie3={prilozhenie3} prilozhenie4={prilozhenie4} prilozhenie5={prilozhenie5} prilozhenie6={prilozhenie6} prilozhenie7={prilozhenie7} prilozhenie8={prilozhenie8} prilozhenie9={prilozhenie9} naimenovanieDokumentaOpen={this.naimenovanieDokumentaOpen} naimenovanieDokumentaClose={this.naimenovanieDokumentaClose} contractClient={contractClient} arrReglamentHref={arrReglamentHref}/>
                                                                    <Prilozhenie2 key={"PR_1_" + index} prilozhenie2={prilozhenie2} reglament2Unfold={reglament2Unfold} openPopupAddPointfromReglament={this.openPopupAddPointfromReglament} reglament2Paragraphs={reglament2Paragraphs} />
                                                                        </li>
                                                                    )
                                                        }
                                                        if (index == 2) {
                                                            return (
                                                                <li key={"li_3_" + index}>
                                                                    <JoiningReglament key={"R_2_" + index} apiUrl={["/api/contactorsSpisok"]} join={join} showPrilozhenie3={showPrilozhenie3} openPrilozhenie={this.openPrilozhenie} prilozhenie1={prilozhenie1} prilozhenie2={prilozhenie2} prilozhenie3={prilozhenie3} prilozhenie4={prilozhenie4} prilozhenie5={prilozhenie5} prilozhenie6={prilozhenie6} prilozhenie7={prilozhenie7} prilozhenie8={prilozhenie8} prilozhenie9={prilozhenie9} naimenovanieDokumentaOpen={this.naimenovanieDokumentaOpen} naimenovanieDokumentaClose={this.naimenovanieDokumentaClose} contractClient={contractClient} arrReglamentHref={arrReglamentHref}/>
                                                                    <Prilozhenie3 key={"PR_2_" + index} prilozhenie3={prilozhenie3} reglament3Unfold={reglament3Unfold} openPopupAddPointfromReglament={this.openPopupAddPointfromReglament} reglament3Paragraphs={reglament3Paragraphs} />
                                                                        </li>
                                                            )
                                                        }
                                                        if (index == 3) {
                                                            return (
                                                                <li key={"li_4_" + index}>
                                                                    <JoiningReglament key={"R_3_" + index} apiUrl={["/api/contactorsSpisok"]} join={join} showPrilozhenie3={showPrilozhenie3} openPrilozhenie={this.openPrilozhenie} prilozhenie1={prilozhenie1} prilozhenie2={prilozhenie2} prilozhenie3={prilozhenie3} prilozhenie4={prilozhenie4} prilozhenie5={prilozhenie5} prilozhenie6={prilozhenie6} prilozhenie7={prilozhenie7} prilozhenie8={prilozhenie8} prilozhenie9={prilozhenie9} naimenovanieDokumentaOpen={this.naimenovanieDokumentaOpen} naimenovanieDokumentaClose={this.naimenovanieDokumentaClose} contractClient={contractClient} arrReglamentHref={arrReglamentHref}/>
                                                                    <Prilozhenie4 key={"PR_3_" + index} prilozhenie4={prilozhenie4} reglament4Unfold={reglament4Unfold} openPopupAddPointfromReglament={this.openPopupAddPointfromReglament}  reglament4Paragraphs={reglament4Paragraphs} />
                                                                        </li>
                                                            )
                                                        }
                                                        if (index == 4) {
                                                            return (
                                                                <li key={"li_5_" + index}>
                                                                    <JoiningReglament key={"R_4_" + index} apiUrl={["/api/contactorsSpisok"]} join={join} showPrilozhenie3={showPrilozhenie3} openPrilozhenie={this.openPrilozhenie} prilozhenie1={prilozhenie1} prilozhenie2={prilozhenie2} prilozhenie3={prilozhenie3} prilozhenie4={prilozhenie4} prilozhenie5={prilozhenie5} prilozhenie6={prilozhenie6} prilozhenie7={prilozhenie7} prilozhenie8={prilozhenie8} prilozhenie9={prilozhenie9} naimenovanieDokumentaOpen={this.naimenovanieDokumentaOpen} naimenovanieDokumentaClose={this.naimenovanieDokumentaClose} contractClient={contractClient} arrReglamentHref={arrReglamentHref}/>
                                                                    <Prilozhenie5 key={"PR_4_" + index} prilozhenie5={prilozhenie5} reglament5Unfold={reglament5Unfold} openPopupAddPointfromReglament={this.openPopupAddPointfromReglament}  reglament5Paragraphs={reglament5Paragraphs} />
                                                                        </li>
                                                            )
                                                        }
                                                        if (index == 5) {
                                                            return (
                                                                <li key={"li_6_" + index}>
                                                                    <JoiningReglament key={"R_5_" + index} apiUrl={["/api/contactorsSpisok"]} join={join} showPrilozhenie3={showPrilozhenie3} openPrilozhenie={this.openPrilozhenie} prilozhenie1={prilozhenie1} prilozhenie2={prilozhenie2} prilozhenie3={prilozhenie3} prilozhenie4={prilozhenie4} prilozhenie5={prilozhenie5} prilozhenie6={prilozhenie6} prilozhenie7={prilozhenie7} prilozhenie8={prilozhenie8} prilozhenie9={prilozhenie9} naimenovanieDokumentaOpen={this.naimenovanieDokumentaOpen} naimenovanieDokumentaClose={this.naimenovanieDokumentaClose} contractClient={contractClient} arrReglamentHref={arrReglamentHref}/>
                                                                    <Prilozhenie6 key={"PR_5_" + index} prilozhenie6={prilozhenie6} reglament6Unfold={reglament6Unfold} openPopupAddPointfromReglament={this.openPopupAddPointfromReglament}  reglament6Paragraphs={reglament6Paragraphs} />
                                                                        </li>
                                                            )
                                                        }
                                                        if (index == 6) {
                                                            return (
                                                                <li key={"li_7_" + index}>
                                                                    <JoiningReglament key={"R_6_" + index} apiUrl={["/api/contactorsSpisok"]} join={join} showPrilozhenie3={showPrilozhenie3} openPrilozhenie={this.openPrilozhenie} prilozhenie1={prilozhenie1} prilozhenie2={prilozhenie2} prilozhenie3={prilozhenie3} prilozhenie4={prilozhenie4} prilozhenie5={prilozhenie5} prilozhenie6={prilozhenie6} prilozhenie7={prilozhenie7} prilozhenie8={prilozhenie8} prilozhenie9={prilozhenie9} naimenovanieDokumentaOpen={this.naimenovanieDokumentaOpen} naimenovanieDokumentaClose={this.naimenovanieDokumentaClose} contractClient={contractClient} arrReglamentHref={arrReglamentHref}/>
                                                                    <Prilozhenie7 key={"PR_6_" + index} prilozhenie7={prilozhenie7} reglament7Unfold={reglament7Unfold} openPopupAddPointfromReglament={this.openPopupAddPointfromReglament}  reglament7Paragraphs={reglament7Paragraphs} />
                                                                        </li>
                                                            )
                                                        }
                                                        if (index == 7) {
                                                            return (
                                                                <li key={"li_8_" + index}>
                                                                    <JoiningReglament key={"R_7_" + index} apiUrl={["/api/contactorsSpisok"]} join={join} showPrilozhenie3={showPrilozhenie3} openPrilozhenie={this.openPrilozhenie} prilozhenie1={prilozhenie1} prilozhenie2={prilozhenie2} prilozhenie3={prilozhenie3} prilozhenie4={prilozhenie4} prilozhenie5={prilozhenie5} prilozhenie6={prilozhenie6} prilozhenie7={prilozhenie7} prilozhenie8={prilozhenie8} prilozhenie9={prilozhenie9} naimenovanieDokumentaOpen={this.naimenovanieDokumentaOpen} naimenovanieDokumentaClose={this.naimenovanieDokumentaClose} contractClient={contractClient} arrReglamentHref={arrReglamentHref}/>
                                                                    <Prilozhenie8 key={"PR_7_" + index} prilozhenie8={prilozhenie8} reglament8Unfold={reglament8Unfold} openPopupAddPointfromReglament={this.openPopupAddPointfromReglament}  reglament8Paragraphs={reglament8Paragraphs} />
                                                                        </li>
                                                            )
                                                        }
                                                        if (index == 8) {
                                                            return (
                                                                <li key={"li_9_" + index}>
                                                                    <JoiningReglament key={"R_8_" + index} apiUrl={["/api/contactorsSpisok"]} join={join} showPrilozhenie3={showPrilozhenie3} openPrilozhenie={this.openPrilozhenie} prilozhenie1={prilozhenie1} prilozhenie2={prilozhenie2} prilozhenie3={prilozhenie3} prilozhenie4={prilozhenie4} prilozhenie5={prilozhenie5} prilozhenie6={prilozhenie6} prilozhenie7={prilozhenie7} prilozhenie8={prilozhenie8} prilozhenie9={prilozhenie9} naimenovanieDokumentaOpen={this.naimenovanieDokumentaOpen} naimenovanieDokumentaClose={this.naimenovanieDokumentaClose} contractClient={contractClient} arrReglamentHref={arrReglamentHref}/>
                                                                    <Prilozhenie9 key={"PR_8_" + index} prilozhenie9={prilozhenie9} reglament9Unfold={reglament9Unfold} openPopupAddPointfromReglament={this.openPopupAddPointfromReglament}  reglament9Paragraphs={reglament9Paragraphs} />
                                                                        </li>
                                                            )
                                                        }
                                            })
                                         }
                                         {
                                    joiningPrice.map((join, index) => {
                                        if (index == 0) {
                                            return (
                                                <li key={"li_10_" + index}>
                                                    <JoiningPrice key={index} join={join} openPrilozhenie={this.openPrilozhenie} naimenovanieDokumentaOpen={this.naimenovanieDokumentaOpen} naimenovanieDokumentaClose={this.naimenovanieDokumentaClose}/>
                                                    <Price1 key={"PR_10_" + index} price1Unfold={price1Unfold} price1={price1} openPopupAddPointfromReglament={this.openPopupAddPointfromReglament} price1Paragraphs={price1Paragraphs }/>
                                                </li>
                                            )
                                        }
                                        if (index == 1) {
                                            return (
                                                <li key={"li_11_" + index}>
                                                    <JoiningPrice key={index} join={join}  openPrilozhenie={this.openPrilozhenie} naimenovanieDokumentaOpen={this.naimenovanieDokumentaOpen} naimenovanieDokumentaClose={this.naimenovanieDokumentaClose}/>
                                                    <Price2 key={"PR_10_" + index} price2Unfold={price2Unfold} price2={price2} openPopupAddPointfromReglament={this.openPopupAddPointfromReglament} price2Paragraphs={price2Paragraphs}/>
                                                </li>
                                            )
                                        }
                                        if (index == 2) {
                                            return (
                                                <li key={"li_13_" + index}>
                                                    <JoiningPrice key={index} join={join} openPrilozhenie={this.openPrilozhenie} naimenovanieDokumentaOpen={this.naimenovanieDokumentaOpen} naimenovanieDokumentaClose={this.naimenovanieDokumentaClose} />
                                                    <Price3 key={"PR_10_" + index} price3Unfold={price3Unfold} price3={price3} openPopupAddPointfromReglament={this.openPopupAddPointfromReglament} price3Paragraphs={price3Paragraphs}/>
                                                </li>
                                            )
                                        }
                                        if (index == 4) {
                                            return (
                                                <li key={"li_14_" + index}>
                                                    <JoiningPrice key={index} join={join} openPrilozhenie={this.openPrilozhenie} naimenovanieDokumentaOpen={this.naimenovanieDokumentaOpen} naimenovanieDokumentaClose={this.naimenovanieDokumentaClose} />
                                                    <Price5 key={"PR_10_" + index} price5Unfold={price5Unfold} price5={price5} openPopupAddPointfromReglament={this.openPopupAddPointfromReglament} price5Paragraphs={price5Paragraphs}/>
                                                </li>
                                            )
                                        }
                                        if (index == 5) {
                                            return (
                                                <li key={"li_15_" + index}>
                                                    <JoiningPrice key={index} join={join} openPrilozhenie={this.openPrilozhenie} naimenovanieDokumentaOpen={this.naimenovanieDokumentaOpen} naimenovanieDokumentaClose={this.naimenovanieDokumentaClose} />
                                                    <Price6 key={"PR_10_" + index} price6Unfold={price6Unfold} price6={price6} openPopupAddPointfromReglament={this.openPopupAddPointfromReglament} price6Paragraphs={price6Paragraphs}/>
                                                </li>
                                            )
                                        }
                                        if (index == 6) {
                                            return (
                                                <li key={"li_16_" + index}>
                                                    <JoiningPrice key={index} join={join} openPrilozhenie={this.openPrilozhenie} naimenovanieDokumentaOpen={this.naimenovanieDokumentaOpen} naimenovanieDokumentaClose={this.naimenovanieDokumentaClose} />
                                                    <Price7 key={"PR_10_" + index} price7Unfold={price7Unfold} price7={price7} openPopupAddPointfromReglament={this.openPopupAddPointfromReglament} price7Paragraphs={price7Paragraphs}/>
                                                </li>
                                            )
                                        }
                                        if (index == 7) {
                                            return (
                                                <li key={"li_17_" + index}>
                                                    <JoiningPrice key={index} join={join} openPrilozhenie={this.openPrilozhenie} naimenovanieDokumentaOpen={this.naimenovanieDokumentaOpen} naimenovanieDokumentaClose={this.naimenovanieDokumentaClose} />
                                                    <Price8 key={"PR_10_" + index} price8Unfold={price8Unfold} price8={price8} openPopupAddPointfromReglament={this.openPopupAddPointfromReglament} price8Paragraphs={price8Paragraphs}/>
                                                </li>
                                            )
                                        }
                                        if (index == 9) {
                                            return (
                                                <li key={"li_19_" + index}>
                                                    <JoiningPrice key={index} join={join} openPrilozhenie={this.openPrilozhenie} naimenovanieDokumentaOpen={this.naimenovanieDokumentaOpen} naimenovanieDokumentaClose={this.naimenovanieDokumentaClose} />
                                                    <Price10 key={"PR_10_" + index} price10Unfold={price10Unfold} price10={price10} openPopupAddPointfromReglament={this.openPopupAddPointfromReglament} price10Paragraphs={price10Paragraphs}/>
                                                </li>
                                            )
                                        }
                                            })
                                        }
                                    </ol>
                            </div>
                        
                        <ol className="rekvizity_storon_zagolovok" style={{ display: (hideFormJoin) ? 'none' : 'block' }}><li><h5><strong>ДОБАВИТБ КОМПАНИЮ И РЕКВИЗИТЫ</strong></h5></li></ol>
                        <div className="rekvizity_storon">                           

                            <ol className="rekvizity_storon_zakazchik" style={{ display: (hideFormJoin) ? 'none' : 'block' }}>
                                
                                <li>Наименование: <span><strong>{this.state.downOrganizationName}</strong></span></li>
                                <li>Адрес: <input type="text" size="67" ref={function (el) { self._input = el; }} onChange={this.adressValue} name="adress" /></li>
                                <li><div className="inn-kpp-okpo-bik-rc-kc">
                                    ИНН/КПП:
                                                <span>
                                                    <input type="text" size="20" ref={function (el) { self._input = el; }} onChange={this.innValue} name="inn" />
                                                    <div className="isinn" id="isinn" style={{ display: (isinn) ? 'none' : 'block' }}></div>
                                                </span>
                                                <span>/</span>
                                                <span>
                                                    <input type="text" size="20" ref={function (el) { self._input = el; }} onChange={this.kppValue} name="kpp" />
                                                    <div className="iskpp" id="iskpp" style={{ display: (iskpp) ? 'none' : 'block' }}></div>
                                                </span>
                                     ОКПО:
                                                <span>
                                                    <input type="text" size="20" ref={function (el) { self._input = el; }} name="okpo" ref={function (el) { self._input = el; }} onChange={this.okpoValue} />
                                        <div className="isokpo" id="isokpo" style={{ display: (isokpo) ? 'none' : 'block' }}></div>
                                                </span>
                                     </div>
                                                
                                                 
                                </li>
                                <li><div className="inn-kpp-okpo-bik-rc-kc">
                                    р/с:
                                    <span>
                                        <input type="text" size="69" ref={function (el) { self._input = el; }} onChange={this.rekvizity_rcValue} name="rekvizity_rc" />
                                        <div className="isrc" id="isrc" style={{ display: (isrc) ? 'none' : 'block' }}></div>
                                    </span>
                                </div>
                                </li>
                                <li>Банк:<input type="text" size="67" ref={function (el) { self._input = el; }} onChange={this.rekvizity_bankValue} name="rekvizity_bank" /></li>
                                <li><div className="inn-kpp-okpo-bik-rc-kc">
                                    к/с:
                                    <span>
                                        <input type="text" size="69" ref={function (el) { self._input = el; }} onChange={this.rekvizity_kcValue} name="rekvizity_kc" />
                                        <div className="iskc" id="iskc" style={{ display: (iskc) ? 'none' : 'block' }}></div>
                                    </span>
                                    </div>
                                </li>
                                <li><div className="inn-kpp-okpo-bik-rc-kc">
                                    БИК:
                                        <span>
                                            <input type="text" name="rekvizity_bik" size="20" ref={function (el) { self._input = el; }} onChange={this.rekvizity_bikValue} />
                                            <div className="isbik" id="isbik" style={{ display: (isbik) ? 'none' : 'block' }}></div>
                                        </span>
                                    </div>
                                </li>
                                <li><div className="inn-kpp-okpo-bik-rc-kc">
                                    телефон:                                     
                                        <span>
                                            <input type="text" size="15" ref={function (el) { self._input = el; }} onChange={this.phoneValue} name="phone" /><span>Пример номера телефона: 8 (999) 99-99-99</span>
                                            <div className="istelephone" id="istelephone" style={{ display: (istelephone) ? 'none' : 'block' }}></div>
                                        </span>
                                    </div>
                                </li>
                                <li><div className="inn-kpp-okpo-bik-rc-kc">
                                    телефон для SMS уведомлений:                                     
                                    <span>
                                        <input type="text" size="15" ref={function (el) { self._input = el; }} onChange={this.phoneforsmsValue} name="phoneforsms" /><span>Пример номера телефона: 8 (999) 99-99-99</span>
                                        <div className="istelephoneSMS" id="istelephoneSMS" style={{ display: (istelephoneSMS) ? 'none' : 'block' }}></div>
                                    </span>
                                    </div>
                                </li>
                                <li><div className="inn-kpp-okpo-bik-rc-kc">
                                        электронная почта для уведомлений:
                                         <span>
                                            <input type="text" size="15" ref={function (el) { self._input = el; }} onChange={this.emailValue} name="email" />
                                            <div className="isemail" id="isemail" style={{ display: (isemail) ? 'none' : 'block' }}></div>
                                        </span>
                                    </div>
                                </li>
                            </ol>
                        </div>
                        <div className="ispolnitel_zakazchik">
                            <span><strong>Управляющий</strong></span>
                            <span><strong>Иванов В.А.</strong></span>
                            <span><strong>{doljnost}</strong></span>
                            <span><span><strong>{userLastName}</strong></span></span>
                        </div>
                        < ButtonJoinedToPublicDoc
                            /*address={address} emails={emails} fullname={fullname} id={id} inn={inn} kpp={kpp} name={name} okpo={okpo} phones={phones}
                          abbreviated={abbreviated} actualcontract={actualcontract} address={address} autodelivery={autodelivery} badpayer={badpayer} carriageservice1c={carriageservice1c}
                            checkttnmail={checkttnmail} color={color} containerservice1c={containerservice1c} country={country} debet={debet} diadoc={diadoc} emails={emails} fnsparticipantid={fnsparticipantid} foralterinter={foralterinter}
                            freightcharge={freightcharge} freightchargeservice={freightchargeservice} fullname={fullname} group={group} id={id} inn={inn} istrusted={istrusted} isunreliable={isunreliable} kod1c={kod1c} kpp={kpp}
                            ktpriceversion={ktpriceversion} loadedgateinconstraint={loadedgateinconstraint} maxdebt={maxdebt} name={name} nightgateout={nightgateout} nightgateoutreserve={nightgateoutreserve} nonds={nonds} notifycontainersemails={notifycontainersemails}
                            notifyemails={notifyemails} okpo={okpo} operationsdenial={operationsdenial} ownerdisposerplatform={ownerdisposerplatform} pastetogethernumber={pastetogethernumber} payfreightcharge={payfreightcharge} phones={phones} postpay={postpay}
                            priceversion={priceversion} repairchargetype={repairchargetype} repaircontract={repaircontract} sbemptyinbound={sbemptyinbound} sbemptyoutput={sbemptyoutput} sbfullinbound={sbfullinbound} sbfulloutput={sbfulloutput} separateatbids={separateatbids}
                            separateatbidsreceivers={separateatbidsreceivers} transporter={transporter} useaddressfom1c={useaddressfom1c}


                            legalStatus={legalStatus} downOrganizationName={downOrganizationName} organizationName={organizationName} doljnost={doljnost} dokumentname={dokumentname} adress={this.state.adress} inn={this.state.inn} kpp={this.state.kpp} okpo={this.state.okpo} rekvizity_rc={this.state.rekvizity_rc} rekvizity_bank={this.state.rekvizity_bank} rekvizity_kc={this.state.rekvizity_kc} rekvizity_bik={this.state.rekvizity_bik} phone={this.state.phone} phoneforsms={this.state.phoneforsms} email={this.state.email}


                           apiUrl={["/api/createNewContragent"]} contractClientID={this.state.contractClient.id} setInn={this.setInn} goloader={goloader} launchPreloader={this.launchPreloader} stopPreloader={this.stopPreloader} uzhe_zaregestrirovan={uzhe_zaregestrirovan} uzhe_zaregestrirovan_inn={uzhe_zaregestrirovan_inn}
                            popupUzhe_zaregestrirovanShow={this.popupUzhe_zaregestrirovanShow} getNameUzhe_zaregestrirovanogo={this.getNameUzhe_zaregestrirovanogo}*/
                        />
                        
                        </form>
                    </div>
                </div>
           
        );
            
    }
}
export default ContentContract;
