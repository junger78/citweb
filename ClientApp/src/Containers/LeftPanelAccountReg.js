import logo from './logo.png'
import React, { Component } from "react";
import { Redirect } from "react-router-dom"
import CurrentUser from '../Components/CurrentUser';
import Fesco  from '../Components/Fesco';
import Timeslot  from '../Components/TimeSlot';
import OnlineTablo  from '../Components/OnlineTablo';
import Applications  from '../Components/Applications';
import Treking  from '../Components/Treking';
import TermsOfTheContract  from '../Components/TermsOfTheContract';
import AuthorizationConditions  from '../Components/AuthorizationConditions';
import request from "superagent";


class LeftPanelAccountReg extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentuser: "",
            some:"",
            // tokenKey: "accessToken",
            auth: this.props.noauth
            
            
        };

    }
    UNSAFE_componentWillMount(){
      
        //this.getData();
       
    }
    componentDidMount() {
        //Promise.all(this.props.apiUrl.map(a => {
        //    return new Promise((resolve, reject) => {
        //        //using superagent here (w/o its promise api), "import request as 'superagent'. You'd import this at the top of your file.
        //        request.get(a)
        //            .end((error, response) => {
        //                if (error) {
        //                    return reject()
        //                } else {
        //                    resolve(response)
        //                }
        //            })
        //    })
        //}))
        //    .then(v => {

        //        this.setState({
        //            currentuser: v[0].body,
        //            some: v[1].body,
        //            auth: true
                     
        //        })

            
        //    })
        //    .catch(() => {
        //        console.error("Error in data retrieval")
        //    })
       //this.loaduserProfile();
        
      
    }
    loaduserProfile() {
        var xhr = new XMLHttpRequest();
        xhr.open("get", this.props.apiUrl, true);
        xhr.onload = function () {
            /* var data = JSON.parse(xhr.responseText);*/           
            if (xhr.status !== 200) {           
                console.log(xhr.status + ': ' + xhr.statusText);
                this.setState({ auth: false });
            } else {
                
              
                var data = xhr.responseText;
                data = data.replace(/[0-9]/g, '');
                //console.log(data);
                this.setState({ currentuser: data });
                this.setState({ auth: true });
        }
        }.bind(this);
        
        xhr.send();
    }

    
    getData = async () => {
        const token = localStorage.getItem("tokenKey");
        //console.log(this.props.gtlgn);
        console.log(token);
        const response = await fetch(this.props.gtlgn, {
        method: "GET",
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token  // �������� ������ � ���������
        }
    });
    if (response.ok === true) {
        const data = await response.json();
        console.log(data);
       
    }
    else
            console.log("Status: ", response.status);
        
};


    render() {
        if (this.state.auth === false) {
            return <Redirect to='/account-unreg' />;
        }
    return (
        <div className="leftpanel">
            <div className="left_panel_the_first_part"> 
                <a id="to_home_page" href="/">
                    <img src={logo} alt={"logo"}/>
                </a>
            </div>           
            <div className="left_panel_the_second_part">
                <CurrentUser user={this.state.currentuser}/>
                <Fesco/>
                <Timeslot/>
                <OnlineTablo/>
                <Applications/>
                <Treking/>
                <TermsOfTheContract/>
                <AuthorizationConditions/>                
            </div>
            
        </div>
        );
    }
}
export default LeftPanelAccountReg;