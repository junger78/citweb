﻿import React, { Component } from "react";
import TopMenu from './TopMenu';
import Footer from './Footer';
import ContentInfoOkompanii from './ContentInfoOkompanii';
class MainInfoOkompanii extends Component {
    render() {
        return (
            <div className="width73">
                <div className="content_wrapper">
                    <TopMenu />
                    <ContentInfoOkompanii />
                    <Footer />
                </div>
            </div>
        );
    }
}
export default MainInfoOkompanii;