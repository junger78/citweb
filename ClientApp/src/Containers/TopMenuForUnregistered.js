﻿import React from "react";
import { Link } from "react-router-dom";
class TopMenuForUnregistered extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <div >
                <div className="top_menu">
                    <Link to="/porjadok_zakljuchenija_dogovora">
                        <div className="top_menu_button_black">Порядок заключения договора</div>
                    </Link>
                    <Link to="/registration">
                        <div className="top_menu_button_black"> Регистрация</div>
                    </Link>
                    <Link to="/authorization">
                        <div className="top_menu_button_black"> Авторизация</div>
                    </Link>
                    <Link to="/on-line_zakljuchenie_dogovora">
                        <div className="top_menu_button_black">on-line заключение договора</div>
                    </Link>

                </div>
            </div>
        );
    }
}
export default TopMenuForUnregistered;