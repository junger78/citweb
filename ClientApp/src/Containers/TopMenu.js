import React  from "react";
import { Link} from "react-router-dom";
class TopMenu extends React.Component{		
        constructor(props) {
            super(props);           
            this.state = {              
            };
           }	
	render(){      
		return (		
			<div >			
                <div className="top_menu">
                    <Link to="/about_company">
                        <div className="top_menu_button_black">О компании</div>
                    </Link>
                    <Link to="/personalnye_menedzhery">
                        <div  className="top_menu_button_black">Персональные менеджеры</div>
                    </Link> 
                    <Link to="/history"> 
                        <div  className="top_menu_button_black">История</div>
                    </Link> 
                    <Link to="/photoalbum">
                        <div  className="top_menu_button_black">Фотоальбом</div>
                    </Link>
                    <Link to="/presentation">
                        <div  className="top_menu_button_black">Презентация</div>
                    </Link>
                    <Link to="/write">
                        <div className="top_menu_button_red">Пишите нам</div>
                    </Link>
                    <Link to="/call">
                        <div className="top_menu_button_red">Звоните нам</div>
                    </Link>
                </div>
			</div>		
		);
	}
}
export default TopMenu;