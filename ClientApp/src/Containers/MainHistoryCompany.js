﻿import React, { Component } from "react";
import TopMenu from './TopMenu';
import Footer from './Footer';
import ContentHistoryCompany from './ContentHistoryCompany';
class MainHistoryCompany extends Component {
    render() {
        return (
            <div className="width73">
                <div className="content_wrapper">
                    <TopMenu />
                    <ContentHistoryCompany />
                    <Footer />
                </div>
            </div>
        );
    }
}
export default MainHistoryCompany;