import React, {Component} from "react";
import TopMenuForUnregistered from './TopMenuForUnregistered';
import Content  from './Content';
class MainAccountReg extends Component {
 render() {
    return (
        <div className="width73">
            <div className="content_wrapper">                   
                <TopMenuForUnregistered/>
                <Content/>                              
            </div>        
        </div>
        );
    }
}
export default MainAccountReg;