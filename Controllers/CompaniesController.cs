﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AspNetCoreReact.Models;
using Microsoft.AspNetCore.Authorization;

namespace AspNetCoreReact.Controllers
{

    [Route("api/[controller]")]
    public class CompaniesController : Controller
    {

        static readonly List<Companies> data;
        static CompaniesController()
        {

            data = new List<Companies>
            {
                new Companies { CompaniesId = 1, CompaniesShortName = "Газпром", CompaniesTelephone = "+7(495)258765", CompaniesEmail = "gazprom@mail.ru", CompaniesINN = "7736050003", CompaniesKPP="781401001" , CompaniesAdress = "197229, город Санкт-Петербург, Лахтинский пр-кт, д. 2 к. 3 стр. 1"},
                new Companies { CompaniesId = 2, CompaniesShortName = "Лукойл", CompaniesTelephone = "+7(495)618906", CompaniesEmail = "lukoil@mail.ru", CompaniesINN = "7708004767", CompaniesKPP="770801001" , CompaniesAdress = "101000, город Москва, Сретенский б-р, д.11"} ,
                new Companies { CompaniesId = 3, CompaniesShortName = "Роснефть", CompaniesTelephone = "+7(495)739025", CompaniesEmail = "rosneft@mail.ru", CompaniesINN = "7706107510", CompaniesKPP="770601001 " , CompaniesAdress = "115035, город Москва, Софийская наб., д.26/1" },
                new Companies { CompaniesId = 4, CompaniesShortName = "Магнит", CompaniesTelephone = "+7(495)316903", CompaniesEmail = "magnit@mail.ru", CompaniesINN = "2309085638", CompaniesKPP="231101001" , CompaniesAdress = "350072, Краснодарский край, г. Краснодар, Солнечная ул., д.15 к.5" },
                new Companies { CompaniesId = 5, CompaniesShortName = "Сбербанк России", CompaniesTelephone = "+7(495)076287", CompaniesEmail = "sber@mail.ru", CompaniesINN = "7707083893", CompaniesKPP="773601001 ", CompaniesAdress = "117312, город Москва, ул. Вавилова, д.19" },
                new Companies { CompaniesId = 6, CompaniesShortName = "Российские железные дороги", CompaniesTelephone = "+7(495)803267", CompaniesEmail = "rjd@mail.ru", CompaniesINN = "7708503727", CompaniesKPP="770801001", CompaniesAdress = "107174, город Москва, Новая Басманная ул, д. 2/1 стр. 1" },
                new Companies { CompaniesId = 7, CompaniesShortName = "ВТБ", CompaniesTelephone = "+7(495)703682", CompaniesEmail = "vtb@mail.ru", CompaniesINN = "7702070139", CompaniesKPP="784201001", CompaniesAdress = "191144, город Санкт-Петербург, Дегтярный пер., д. 11 литер а" },
                new Companies { CompaniesId = 8, CompaniesShortName = "Ростех", CompaniesTelephone = "+7(495)582907", CompaniesEmail = "rostech@mail.ru", CompaniesINN = "7704274402", CompaniesKPP="770401001", CompaniesAdress = "119991, город Москва, Гоголевский б-р, д. 21 стр. 1"  },
            };
        }
        [HttpGet]
        public IEnumerable<Companies> Get()
        {
            return data;
        }

        //[HttpPost]
        //public IActionResult Post(Joining phone)
        //{
        //    phone.Id = Guid.NewGuid().ToString();
        //    data.Add(phone);
        //    return Ok(phone);
        //}

        //[HttpDelete("{id}")]
        //public IActionResult Delete(string id)
        //{
        //    Joining phone = data.FirstOrDefault(x => x.Id == id);
        //    if (phone == null)
        //    {
        //        return NotFound();
        //    }
        //    data.Remove(phone);
        //    return Ok(phone);
        //}
    }
}

