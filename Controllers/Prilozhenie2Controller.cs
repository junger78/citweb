﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AspNetCoreReact.Models;
using Microsoft.AspNetCore.Authorization;

namespace AspNetCoreReact.Controllers
{

    [Route("api/[controller]")]
    public class Prilozhenie2Controller : Controller
    {

        static readonly List<Prilozhenie2> data;
        static Prilozhenie2Controller()
        {

            data = new List<Prilozhenie2>();
            using (var context = new logistContext())
            {
                // string username = null;
                data.AddRange(context.Prilozhenie2s.ToList());


            }

        }
        [HttpGet]
        public IEnumerable<Prilozhenie2> Get()
        {
            return data;
        }

        //[HttpPost]
        //public IActionResult Post(Joining phone)
        //{
        //    phone.Id = Guid.NewGuid().ToString();
        //    data.Add(phone);
        //    return Ok(phone);
        //}

        //[HttpDelete("{id}")]
        //public IActionResult Delete(string id)
        //{
        //    Joining phone = data.FirstOrDefault(x => x.Id == id);
        //    if (phone == null)
        //    {
        //        return NotFound();
        //    }
        //    data.Remove(phone);
        //    return Ok(phone);
        //}
    }
}

