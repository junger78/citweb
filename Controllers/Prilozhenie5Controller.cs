﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AspNetCoreReact.Models;
using Microsoft.AspNetCore.Authorization;

namespace AspNetCoreReact.Controllers
{

    [Route("api/[controller]")]
    public class Prilozhenie5Controller : Controller
    {

        static readonly List<Prilozhenie5> data;
        static Prilozhenie5Controller()
        {

            data = new List<Prilozhenie5>();
            using (var context = new logistContext())
            {
                // string username = null;
                data.AddRange(context.Prilozhenie5s.ToList());


            }

        }
        [HttpGet]
        public IEnumerable<Prilozhenie5> Get()
        {
            return data;
        }

        //[HttpPost]
        //public IActionResult Post(Joining phone)
        //{
        //    phone.Id = Guid.NewGuid().ToString();
        //    data.Add(phone);
        //    return Ok(phone);
        //}

        //[HttpDelete("{id}")]
        //public IActionResult Delete(string id)
        //{
        //    Joining phone = data.FirstOrDefault(x => x.Id == id);
        //    if (phone == null)
        //    {
        //        return NotFound();
        //    }
        //    data.Remove(phone);
        //    return Ok(phone);
        //}
    }
}

