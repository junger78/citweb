﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

using AspNetCoreReact.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Ubiety.Dns.Core;




//using LayerTo1C._1C;

namespace AspNetCoreReact.Controllers
{
    [ApiController]
    
    [Produces("application/json")]
    //[Route("api/{controller}")]
    [Route("api/[controller]")]
    //[Authorize]
    public class contactorsSpisok : Controller
    {  
        static readonly List<ContactorsSpisokPprisoedinenija> data;
        public decimal res;

        static contactorsSpisok()
        {
            data = new List<ContactorsSpisokPprisoedinenija>();
           
            using (var context = new logistContext())
            {         
                data.AddRange(context.ContactorsSpisokPprisoedinenijas.ToList());                

            }            
        }

       
        [HttpGet]
        public IEnumerable<ContactorsSpisokPprisoedinenija> Get()
        {
            return data;
        }

        [HttpPut]
        public IActionResult Put(ContactorsSpisokPprisoedinenija Organiz)
        {
          //  LinqTo1C linqTo1C = new LinqTo1C();
          //linqTo1C.GetPriceServiceFrom1C("00-00039576");           
            using (var context = new logistContext())
            {
                int? thatId = 0;
                foreach( var item in data)
                {
                    if(item.OrganizId == Organiz.OrganizId)
                    {
                        thatId = item.OrganizId;
                    }                   
                    try
                    {
                        var bids = context.ContactorsSpisokPprisoedinenijas.Where(x => x.OrganizId == thatId).FirstOrDefault();
                        bids.Prilozhenie1 = Organiz.Prilozhenie1;
                        bids.Prilozhenie2 = Organiz.Prilozhenie2;
                        bids.Prilozhenie3 = Organiz.Prilozhenie3;
                        bids.Prilozhenie4 = Organiz.Prilozhenie4;
                        bids.Prilozhenie5 = Organiz.Prilozhenie5;
                        bids.Prilozhenie6 = Organiz.Prilozhenie6;
                        bids.Prilozhenie7 = Organiz.Prilozhenie7;
                        bids.Prilozhenie8 = Organiz.Prilozhenie8;
                        bids.Prilozhenie9 = Organiz.Prilozhenie9;


                        context.SaveChanges();
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine("Возникло исключение!" + ex.Message);
                    }

                
                }
                return Ok();
            }
        }

        [HttpPost]

        public IActionResult Post(ContactorsSpisokPprisoedinenija Organiz)       
        {
           
            using (var context = new logistContext())
                {                   
                            //var bd = new ContactorsSpisokPprisoedinenija();
                            //bd.OrganizId = Organiz.OrganizId;
                            //bd.Prilozhenie1 = Organiz.Prilozhenie1;
                            context.ContactorsSpisokPprisoedinenijas.Add(Organiz);
                            context.SaveChanges();                   
                }
                return Ok(data);
            }            
           
         }

    

}
