﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AspNetCoreReact.Models;
using Microsoft.AspNetCore.Authorization;

namespace AspNetCoreReact.Controllers
{

    [Route("api/[controller]")]
    public class DocParagraphsHeadsController : Controller
    {
        
        static readonly List<DocParagraphsHead> data;
        static DocParagraphsHeadsController()
        {

            data = new List<DocParagraphsHead>();
            using (var context = new logistContext())
            {
                
                data.AddRange(context.DocParagraphsHeads.ToList());
            }
        }
        [HttpGet]
        public IEnumerable<DocParagraphsHead> Get()
        {
            return data;
        }

        //[HttpPost]
        //public IActionResult Post(Joining phone)
        //{
        //    phone.Id = Guid.NewGuid().ToString();
        //    data.Add(phone);
        //    return Ok(phone);
        //}

        //[HttpDelete("{id}")]
        //public IActionResult Delete(string id)
        //{
        //    Joining phone = data.FirstOrDefault(x => x.Id == id);
        //    if (phone == null)
        //    {
        //        return NotFound();
        //    }
        //    data.Remove(phone);
        //    return Ok(phone);
        //}
    }
}

