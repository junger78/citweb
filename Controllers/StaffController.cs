﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AspNetCoreReact.Models;
using Microsoft.AspNetCore.Authorization;

namespace AspNetCoreReact.Controllers
{

    [Route("api/[controller]")]
    public class StaffController : Controller
    {

        static readonly List<Staff> data;
        static StaffController()
        {

            data = new List<Staff>
            {
        
                new Staff { CompaniesId = 1, EmployeeId = 1, EmployeeName="Миллер Алексей Борисович", EmployeeTelephone = "+7(495)258761", EmployeeEmail = "gazprom_alecs@mail.ru" },
                new Staff { CompaniesId = 1, EmployeeId = 2, EmployeeName="Миллер Жанна Алексеевна", EmployeeTelephone = "+7(495)258762", EmployeeEmail = "gazprom_janna@mail.ru" },
                new Staff { CompaniesId = 1, EmployeeId = 3, EmployeeName="Миллер Алексей Алексеевич", EmployeeTelephone = "+7(495)258763", EmployeeEmail = "gazpr_alecs_secom@mail.ru" },
                new Staff { CompaniesId = 1, EmployeeId = 4, EmployeeName="Миллер Дмитрий Алексеевич", EmployeeTelephone = "+7(495)258764", EmployeeEmail = "gazprom_dima@mail.ru" },
                new Staff { CompaniesId = 1, EmployeeId = 5, EmployeeName="Миллер Ольга Алексеевна", EmployeeTelephone = "+7(495)258765", EmployeeEmail = "gazprom_olga@mail.ru" },


                new Staff { CompaniesId = 2, EmployeeId = 1, EmployeeName="Алекперов Вагит Юсуфович", EmployeeTelephone = "+7(495)258761",  EmployeeEmail = "lukoil_vagit@mail.ru" },
                new Staff { CompaniesId = 2, EmployeeId = 2, EmployeeName="Алекперова Марина Вагитовна", EmployeeTelephone = "+7(495)258762",  EmployeeEmail = "lukoil_marin@mail.ru" },
                new Staff { CompaniesId = 2, EmployeeId = 3, EmployeeName="Алекперова Мария Вагитовна", EmployeeTelephone = "+7(495)258763",  EmployeeEmail = "lukoil_mar@mail.ru" },
                new Staff { CompaniesId = 2, EmployeeId = 4, EmployeeName="Алекперова Софья Вагитовна", EmployeeTelephone = "+7(495)258764",  EmployeeEmail = "lukoil_cof@mail.ru" },
                new Staff { CompaniesId = 2, EmployeeId = 5, EmployeeName="Алекперова Зухлия Вагитовна", EmployeeTelephone = "+7(495)258765",  EmployeeEmail = "lukoil_zuh@mail.ru" },


                new Staff { CompaniesId = 3, EmployeeId = 1, EmployeeName="Сечин Игорь Иванович", EmployeeTelephone = "+7(495)739025", EmployeeEmail = "rosneft_igor@mail.ru" },
                new Staff { CompaniesId = 3, EmployeeId = 2, EmployeeName="Сечин Дмитрий Игоревич", EmployeeTelephone = "+7(495)739024", EmployeeEmail = "rosneft_dmitr@mail.ru" },
                new Staff { CompaniesId = 3, EmployeeId = 3, EmployeeName="Сечин Роман Игоревич", EmployeeTelephone = "+7(495)739023", EmployeeEmail = "rosneft_rom@mail.ru" },
                new Staff { CompaniesId = 3, EmployeeId = 4, EmployeeName="Сечин Павел Игоревич", EmployeeTelephone = "+7(495)739022", EmployeeEmail = "rosneft_pavel@mail.ru" },
                new Staff { CompaniesId = 3, EmployeeId = 5, EmployeeName="Сечин Евгений Игоревич", EmployeeTelephone = "+7(495)739021", EmployeeEmail = "rosneft_evgen@mail.ru" },


                new Staff { CompaniesId = 4,  EmployeeId = 1, EmployeeName="Галицкий Сергей Николаевич", EmployeeTelephone = "+7(495)316903", EmployeeEmail = "magnit_serj@mail.ru" },
                new Staff { CompaniesId = 4,  EmployeeId = 2, EmployeeName="Галицкий Роман Николаевич", EmployeeTelephone = "+7(495)316902", EmployeeEmail = "magnit_roman@mail.ru" },
                new Staff { CompaniesId = 4,  EmployeeId = 3, EmployeeName="Галицкий Бигбулат Николаевич", EmployeeTelephone = "+7(495)316901", EmployeeEmail = "magnit_bulat@mail.ru" },
                new Staff { CompaniesId = 4,  EmployeeId = 4, EmployeeName="Галицкий Изекиль Николаевич", EmployeeTelephone = "+7(495)316904", EmployeeEmail = "magnit_izya@mail.ru" },
                new Staff { CompaniesId = 4,  EmployeeId = 5, EmployeeName="Галицкий Иван Николаевич", EmployeeTelephone = "+7(495)316905", EmployeeEmail = "magnit_ivan@mail.ru" },



                new Staff { CompaniesId = 5, EmployeeId = 1, EmployeeName="Греф Герман Оскарович", EmployeeTelephone = "+7(495)076287", EmployeeEmail = "sber_german@mail.ru" },
                new Staff { CompaniesId = 5, EmployeeId = 2, EmployeeName="Греф Сергей Германович", EmployeeTelephone = "+7(495)076286", EmployeeEmail = "sber_serg@mail.ru" },
                new Staff { CompaniesId = 5, EmployeeId = 3, EmployeeName="Греф Олег Германович", EmployeeTelephone = "+7(495)076285", EmployeeEmail = "sber_oleg@mail.ru" },
                new Staff { CompaniesId = 5, EmployeeId = 4, EmployeeName="Греф Роман Германович", EmployeeTelephone = "+7(495)076284", EmployeeEmail = "sber_roman@mail.ru" },
                new Staff { CompaniesId = 5, EmployeeId = 5, EmployeeName="Греф Евгений Германович", EmployeeTelephone = "+7(495)076283", EmployeeEmail = "sber_evgen@mail.ru" },



                new Staff { CompaniesId = 6, EmployeeId = 1, EmployeeName="Белозёров Олег Валентинович", EmployeeTelephone = "+7(495)803267", EmployeeEmail = "rjd_oleg@mail.ru" },
                new Staff { CompaniesId = 6, EmployeeId = 2, EmployeeName="Белозёров Евгений Олегович", EmployeeTelephone = "+7(495)803266", EmployeeEmail = "rjd_evgen@mail.ru" },
                new Staff { CompaniesId = 6, EmployeeId = 3, EmployeeName="Белозёров Александр Олегович", EmployeeTelephone = "+7(495)803265", EmployeeEmail = "rjd_al@mail.ru" },
                new Staff { CompaniesId = 6, EmployeeId = 4, EmployeeName="Белозёров Роман Олегович", EmployeeTelephone = "+7(495)803264", EmployeeEmail = "rjd_rom@mail.ru" },
                new Staff { CompaniesId = 6, EmployeeId = 5, EmployeeName="Белозёров Николай Олегович", EmployeeTelephone = "+7(495)803263", EmployeeEmail = "rjd_nik@mail.ru" },


                new Staff { CompaniesId = 7, EmployeeId = 1, EmployeeName="Костин Андрей Леонидович", EmployeeTelephone = "+7(495)703682", EmployeeEmail = "vtb_andre@mail.ru" },
                new Staff { CompaniesId = 7, EmployeeId = 2, EmployeeName="Костин Евгений Андреевич", EmployeeTelephone = "+7(495)703683", EmployeeEmail = "vtb_evg@mail.ru" },
                new Staff { CompaniesId = 7, EmployeeId = 3, EmployeeName="Костин Леонид Андреевич", EmployeeTelephone = "+7(495)703681", EmployeeEmail = "vtb_lenya@mail.ru" },
                new Staff { CompaniesId = 7, EmployeeId = 4, EmployeeName="Костин Олег Андреевич", EmployeeTelephone = "+7(495)703684", EmployeeEmail = "vtb_oleg@mail.ru" },
                new Staff { CompaniesId = 7, EmployeeId = 5, EmployeeName="Костин Сергей Андреевич", EmployeeTelephone = "+7(495)703685", EmployeeEmail = "vtb_sereg@mail.ru" },


                new Staff { CompaniesId = 8, EmployeeId = 1, EmployeeName="Чемезов Сергей Викторович", EmployeeTelephone = "+7(495)582907", EmployeeEmail = "rostech_sergei@mail.ru" },
                new Staff { CompaniesId = 8, EmployeeId = 2, EmployeeName="Чемезов Михаил Сергеевич", EmployeeTelephone = "+7(495)582904", EmployeeEmail = "rostech_misha@mail.ru" },
                new Staff { CompaniesId = 8, EmployeeId = 3, EmployeeName="Чемезов Олег Сергеевич", EmployeeTelephone = "+7(495)582905", EmployeeEmail = "rostech_oleg@mail.ru" },
                new Staff { CompaniesId = 8, EmployeeId = 4, EmployeeName="Чемезов Евгений Сергеевич", EmployeeTelephone = "+7(495)582902", EmployeeEmail = "rostech_evgenii@mail.ru" },
                new Staff { CompaniesId = 8, EmployeeId = 5, EmployeeName="Чемезов Иван Сергеевич", EmployeeTelephone = "+7(495)582901", EmployeeEmail = "rostech_ivan@mail.ru" },
            };
        }
        [HttpGet]
        public IEnumerable<Staff> Get()
        {
            return data;
        }

        //[HttpPost]
        //public IActionResult Post(Joining phone)
        //{
        //    phone.Id = Guid.NewGuid().ToString();
        //    data.Add(phone);
        //    return Ok(phone);
        //}

        //[HttpDelete("{id}")]
        //public IActionResult Delete(string id)
        //{
        //    Joining phone = data.FirstOrDefault(x => x.Id == id);
        //    if (phone == null)
        //    {
        //        return NotFound();
        //    }
        //    data.Remove(phone);
        //    return Ok(phone);
        //}
    }
}

