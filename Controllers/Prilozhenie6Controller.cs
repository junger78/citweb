﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AspNetCoreReact.Models;
using Microsoft.AspNetCore.Authorization;

namespace AspNetCoreReact.Controllers
{

    [Route("api/[controller]")]
    public class Prilozhenie6Controller : Controller
    {

        static readonly List<Prilozhenie6> data;
        static Prilozhenie6Controller()
        {

            data = new List<Prilozhenie6>();
            using (var context = new logistContext())
            {
                // string username = null;
                data.AddRange(context.Prilozhenie6s.ToList());


            }

        }
        [HttpGet]
        public IEnumerable<Prilozhenie6> Get()
        {
            return data;
        }

        //[HttpPost]
        //public IActionResult Post(Joining phone)
        //{
        //    phone.Id = Guid.NewGuid().ToString();
        //    data.Add(phone);
        //    return Ok(phone);
        //}

        //[HttpDelete("{id}")]
        //public IActionResult Delete(string id)
        //{
        //    Joining phone = data.FirstOrDefault(x => x.Id == id);
        //    if (phone == null)
        //    {
        //        return NotFound();
        //    }
        //    data.Remove(phone);
        //    return Ok(phone);
        //}
    }
}

