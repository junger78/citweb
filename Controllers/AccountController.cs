﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using AspNetCoreReact.Models; // класс Membership
using System.Text;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Authorization;

namespace AspNetCoreReact.Controllers
{
    //[Authorize]
    public class AccountController : Controller
    {
        public class AuthOptions
        {
            public const string ISSUER = "MyAuthServer"; // издатель токена
            public const string AUDIENCE = "MyAuthClient"; // потребитель токена
            const string KEY = "mysupersecret_secretkey!123";   // ключ для шифрации
            public const int LIFETIME = 1; // время жизни токена - 1 минута
            public static SymmetricSecurityKey GetSymmetricSecurityKey()
            {
                return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
            }
        }
        // тестовые данные вместо использования базы данных
        private List<Person> people = new List<Person>
        {
            new Person {Login="admin@gmail.com", Password="12345", Role = "admin" }
           
        };

        [HttpPost("/token")]
        public IActionResult Token(string username, string password)
        {
            try
            {
                var identity = GetIdentity(username, password);
                if (identity == null)
                {
                    return BadRequest(new { errorText = "Invalid username or password." });
                }

                var now = DateTime.UtcNow;
                // создаем JWT-токен
                var jwt = new JwtSecurityToken(
                        issuer: AuthOptions.ISSUER,
                        audience: AuthOptions.AUDIENCE,
                        notBefore: now,
                        claims: identity.Claims,
                        expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                        signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
                var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

                var response = new
                {
                    access_token = encodedJwt,
                    username = identity.Name
                };

                return Json(response);
            }
            catch(Exception ex) {
                return Json(ex.Message);
            }
           
        }

        private ClaimsIdentity GetIdentity(string username, string password)
        {




            //using (var obj = new logistContext())
            //{
            //    string hash = null;
            //    Membership person=null;
            //    object id = null;
            //    foreach (var member in obj.Memberships)
            //    {
            //        if (member.Email == username) {
            //            hash = EncodePassword(password, MembershipPasswordFormat.Hashed, member.PasswordSalt);
            //            if (member.Password == hash)
            //            {
            //                person = member;
            //                id=member.UserId;
            //            }
            //        }


            //    }
            //    //Membership person = (Membership)obj.Memberships.Where(x => x.Email == username && x.PasswordSalt == password);

            //    //string hash = EncodePassword(password, MembershipPasswordFormat.Hashed, person.PasswordSalt);

            //    if (person.Password == hash)
            //    {
            //        var claims = new List<Claim>
            //        {
            //            new Claim(ClaimsIdentity.DefaultNameClaimType, person.Email),
            //            new Claim("userid", person.UserId.ToString()),

            //            //new Claim(ClaimsIdentity.DefaultRoleClaimType, person.Role)
            //        };

            //        Repository.Claims = claims;



            //        ClaimsIdentity claimsIdentity =
            //        new ClaimsIdentity(claims, "Token");
            //        return claimsIdentity;
            //    }



            //    //if (person != null)
            //    //{
            //    //    var claims = new List<Claim>
            //    //    {
            //    //        new Claim(ClaimsIdentity.DefaultNameClaimType, person.Email),
            //    //        //new Claim(ClaimsIdentity.DefaultRoleClaimType, person.Role)
            //    //    };
            //    //    ClaimsIdentity claimsIdentity =
            //    //    new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
            //    //        ClaimsIdentity.DefaultRoleClaimType);
            //    //    return claimsIdentity;
            //    //}
            //}

            //return null;



            Person person = people.FirstOrDefault(x => x.Login == username && x.Password == password);
            if (person != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, person.Login),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, person.Role)
                };
                ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }

            // если пользователя не найдено
            return null;
        }

        //public enum MembershipPasswordFormat
        //{
        //    Clear = 0,
        //    Hashed = 1,
        //    Encrypted = 2
        //}

        //public static string EncodePassword(string pass, MembershipPasswordFormat passwordFormat, string salt)
        //{
        //    byte[] numArray;
        //    byte[] numArray1;
        //    string base64String;

        //    if (passwordFormat == MembershipPasswordFormat.Hashed)
        //    {
        //        byte[] bytes = Encoding.Unicode.GetBytes(pass);
        //        byte[] numArray2 = Convert.FromBase64String(salt);
        //        byte[] numArray3;
                
        //        // Hash password
        //        HashAlgorithm hashAlgorithm = HashAlgorithm.Create("HMACSHA256");

        //        if (hashAlgorithm as KeyedHashAlgorithm == null)
        //        {
        //            numArray1 = new byte[numArray2.Length + bytes.Length];
        //            Buffer.BlockCopy(numArray2, 0, numArray1, 0, numArray2.Length);
        //            Buffer.BlockCopy(bytes, 0, numArray1, numArray2.Length, bytes.Length);
        //            numArray3 = hashAlgorithm.ComputeHash(numArray1);
        //        }
        //        else
        //        {
        //            KeyedHashAlgorithm keyedHashAlgorithm = (KeyedHashAlgorithm)hashAlgorithm;

        //            if (keyedHashAlgorithm.Key.Length != numArray2.Length)
        //            {

        //                if (keyedHashAlgorithm.Key.Length >= numArray2.Length)
        //                {
        //                    numArray = new byte[keyedHashAlgorithm.Key.Length];
        //                    int num = 0;
        //                    while (true)
        //                    {
        //                        if (!(num < numArray.Length))
        //                        {
        //                            break;
        //                        }
        //                        int num1 = Math.Min(numArray2.Length, numArray.Length - num);
        //                        Buffer.BlockCopy(numArray2, 0, numArray, num, num1);
        //                        num = num + num1;
        //                    }
        //                    keyedHashAlgorithm.Key = numArray;
        //                }
        //                else
        //                {
        //                    numArray = new byte[keyedHashAlgorithm.Key.Length];
        //                    Buffer.BlockCopy(numArray2, 0, numArray, 0, numArray.Length);
        //                    keyedHashAlgorithm.Key = numArray;
        //                }
        //            }
        //            else
        //            {
        //                keyedHashAlgorithm.Key = numArray2;
        //            }
        //            numArray3 = keyedHashAlgorithm.ComputeHash(bytes);
        //        }

        //        base64String = Convert.ToBase64String(numArray3);
        //    }
        //    else if (passwordFormat == MembershipPasswordFormat.Encrypted)
        //    {
        //        throw new NotImplementedException("Encrypted password method is not supported.");
        //    }
        //    else
        //    {
        //        base64String = pass;
        //    }

        //    return base64String;
        //}


    }
}