﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AspNetCoreReact.Models;
using Microsoft.AspNetCore.Authorization;
using System.IO;

namespace AspNetCoreReact.Controllers
{

    [Route("api/[controller]")]
    public class JoiningController : Controller
    {
        //class Links
        //{
        //    public object link;
        //}
       
        static readonly List<DocUnit> data;
        static JoiningController()
        {

            data = new List<DocUnit>();
           
            using (var context = new logistContext())
            {
                //var path = new List<string>();                 
                // var newpath = @"C:\Windows\Temp\" + 1 + ".docx";
                // string username = null;
                string[] path = new string[19] {
                    @"c:\windows\temp\Prilozhenie-1-reglament-podachi-zajavki.docx",
                    @"c:\windows\temp\Prilozhenie-2-reglament-pribytija-avtotransporta-na-terminal.docx",
                    @"c:\windows\temp\Prilozhenie-3-reglament-obrabotki-gruza-pod-tamozhennymi-procedurami.docx",
                    @"c:\windows\temp\Prilozhenie-4-reglamenet-priema-otpravlenija-kontejnerov-kontrejlerov-vagonov.docx",
                    @"c:\windows\temp\Prilozhenie-5-reglament-priema-otpravki-opasnyh-gruzov.docx",
                    @"c:\windows\temp\Prilozhenie-6-reglament-priema-otpravki-hranenija-refrizheratornyh-kontejnerov.docx",
                    @"c:\windows\temp\Prilozhenie-7-reglament-predostavlenija-kontejnerov.docx",
                    @"c:\windows\temp\Prilozhenie-8-reglament-predostavlenija-fitingovyh-plptform.docx",
                    @"c:\windows\temp\Prilozhenie-9-reglament-okazanija-pogruzo-razgruzochnyh-uslug.docx",
                    @"c:\windows\temp\Prilozhenie-10-prajs-list-priema-otpravlenija-kontejnerov.docx",
                    @"c:\windows\temp\Prilozhenie-11-prajs-list-sklada-vremennogo-hranenija.docx",
                    @"c:\windows\temp\Prilozhenie-12-prajs-list-tamozhennogo-sklada.docx",
                    @"c:\windows\temp\Prilozhenie-13-prajs-list-remont-kontejnerov.docx",
                    @"c:\windows\temp\Prilozhenie-14-prajs-list-priema-otpravlenija-vagonov.docx",
                    @"c:\windows\temp\Prilozhenie-15-prajs-list-fitosanitarnogo-kontrolja-tovarov-eajes.docx",
                    @"c:\windows\temp\Prilozhenie-16-prajs-list-priema-otpravlenija-kontejnerov-s-gruzom-7-klassa-opasnosti.docx",
                    @"c:\windows\temp\Prilozhenie-17-prajs-list-pogruzo-razgruzochnyh-rabot.docx",
                    @"c:\windows\temp\Prilozhenie-18-prajs-list-organizacii-avtoperevozki.docx",
                    @"c:\windows\temp\Prilozhenie-19-prajs-list-priema-otpravlenija-kontrejlerov.docx"



                };
                data.AddRange(context.DocUnits.ToList());
                //foreach (var some in data)
                //{
                //    var binaryword = some.DocUnitFile;
                //    foreach (var newpath in path)
                //    {
                //        System.IO.File.WriteAllBytes(newpath, binaryword);
                //    }

                //}
                //Links prisoedinenie = new Links { link = data };
            }           
        }
       
        [HttpGet]
        public IEnumerable<DocUnit> Get()
        {
            return data;
        }

        //[HttpPost]
        //public IActionResult Post(Joining phone)
        //{
        //    phone.Id = Guid.NewGuid().ToString();
        //    data.Add(phone);
        //    return Ok(phone);
        //}

        //[HttpDelete("{id}")]
        //public IActionResult Delete(string id)
        //{
        //    Joining phone = data.FirstOrDefault(x => x.Id == id);
        //    if (phone == null)
        //    {
        //        return NotFound();
        //    }
        //    data.Remove(phone);
        //    return Ok(phone);
        //}
    }
}

