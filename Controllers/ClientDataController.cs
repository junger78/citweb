﻿using System;
using System.Collections.Generic;
using System.Linq;




using Microsoft.AspNetCore.Mvc;

using AspNetCoreReact.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;



namespace AspNetCoreReact.Controllers
{
    
        [ApiController]
    [Route("api/[controller]")]
    public class ClientDataController : Controller
    {
        static readonly List<User> dataUser;
        static readonly List<Contractor> dataContractor;

        public object get_who()
        {
            int? Owner = null;
            string organiz = null;
            
            Contractor contractClient = null;

            foreach (var user in dataUser)
            {
                foreach (var orgnz in dataContractor)
                {
                    foreach (var useridauth in Repository.Claims)
                    {
                        if (user.UserId.ToString() == useridauth.Value.ToString())
                        {
                            Owner = user.OrganizId;
                            if (orgnz.Id == Owner)
                            {
                                var UserAssignment = orgnz.Name;
                                if (UserAssignment != null)
                                {
                                    organiz = UserAssignment;
                                    contractClient = orgnz;
                                }

                            }

                        }
                    }
                }   
                
            }
            return contractClient;
        }



        static ClientDataController()
        {
            dataUser = new List<User>();
            using (var context = new logistContext())
            {                
                dataUser.AddRange(context.Users.ToList());
            }
            dataContractor = new List<Contractor>();
            using (var context = new containers3Context())
            {
                dataContractor.AddRange(context.Contractors.ToList());                
            }

        }
        [HttpGet]
        
        public object Get() 
        {
            var res = get_who();            
            return res;


        }



    }
}

