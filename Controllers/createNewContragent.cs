﻿using System;
using System.Collections.Generic;
using System.Linq;




using Microsoft.AspNetCore.Mvc;

using AspNetCoreReact.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using LayerTo1C._1C;
using System.Net;

namespace AspNetCoreReact.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class CreateNewContragent : Controller
    {
        static readonly List<NewContractor> data;

        

        static CreateNewContragent()
        {
            data = new List<NewContractor>();

            using (var context = new containers3Context())
            {
                data.AddRange(context.NewContractors.ToList());

            }
        }

        [HttpPost]

        public IActionResult Post(NewContractor NewOrganiz)
        {
            var inn = NewOrganiz.Inn;
            var kpp = NewOrganiz.Kpp;
            LinqTo1C linqTo1C = new LinqTo1C();           
            var result= LinqTo1C.ValidateContractors(inn, kpp);
            
            using (var context = new containers3Context())
            {
                
                context.NewContractors.Add(NewOrganiz);
                context.SaveChanges();
            }
            return Ok(result);
        }

        [HttpGet]
        public IEnumerable<NewContractor> Get()
        {
            return data;
        }

    }


}


//наименование: Тестовый клиент
//полное наименоване: ООО Тестовый клиент
//огрн: 1057748629960
//inn: 7707563762
//kpp: 770701001
//банк: 046577674 УРАЛЬСКИЙ БАНК ПАО СБЕРБАНК
//счет: 40817810116545045924
//телефон: (343) 226 - 85 - 82
//адрес :  Каланчевская 27
